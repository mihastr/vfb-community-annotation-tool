package com.vfb.socl.exception;

public class UnAuthorizedException extends RuntimeException {
	/**
	 * 
	 */
	private static final long serialVersionUID = 5700153960667265711L;
	public UnAuthorizedException(String msg) {
		super(msg);
	}
}
