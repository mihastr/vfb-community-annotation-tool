package com.vfb.socl.exception;

public class NoSuchChatException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7058528694199413573L;

	public NoSuchChatException() {
		super("no such chat");
	}
	
}
