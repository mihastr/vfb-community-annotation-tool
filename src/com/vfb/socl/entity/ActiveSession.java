package com.vfb.socl.entity;

import java.util.Date;

public class ActiveSession {
	private String sessionId;
	private String username;
	private Date sessionStarted;
	private Date sessionEnded;
	
	public ActiveSession() {
		
	}
	
	public ActiveSession(String id, String username) {
		this.username = username;
		this.sessionId = id;
	}
	
	public String getSessionId() {
		return sessionId;
	}
	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public Date getSessionStarted() {
		return sessionStarted;
	}
	public void setSessionStarted(Date sessionStarted) {
		this.sessionStarted = sessionStarted;
	}
	
	public Date getSessionEnded() {
		return sessionEnded;
	}

	public void setSessionEnded(Date sessionEnded) {
		this.sessionEnded = sessionEnded;
	}

	@Override
	public String toString() {
		return String.format("%s - id: %s (from: %s, to: %s)", username, sessionId, sessionStarted, sessionEnded);
	}
	
}
