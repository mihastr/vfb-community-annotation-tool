package com.vfb.socl.entity;

import java.util.Date;

public class Connection extends Person {
	public String status;
	public String relationship;
	public String direction;
	public Date connectionRequest;
}
