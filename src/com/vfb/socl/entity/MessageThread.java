package com.vfb.socl.entity;

import java.util.Date;
import java.util.Collection;

public class MessageThread extends Person {
	private long conversationId;
	private String title;
	private Person initiator;
	private Date sessionStart;
	private Date sessionEnd;
	private String visibility;
	private boolean hasNewMessages;	
	private String sender;
	private String senderName;
	private String body;
	private Date lastMessageTime;
	private boolean isLMSOnline;
	
	private String boardLocation;
	private String entityId;


	public String getBoardLocation() {
		return boardLocation;
	}

	public void setBoardLocation(String boardLocation) {
		this.boardLocation = boardLocation;
	}

	public String getEntityId() {
		return entityId;
	}

	public void setEntityId(String entityId) {
		this.entityId = entityId;
	}

	private Collection<Person> participants;
	private Collection<Message> messages;

	
	public String getVisibility() {
		return visibility;
	}

	public void setVisibility(String visibility) {
		this.visibility = visibility;
	}

	public String getSender() {
		return sender;
	}

	public void setSender(String sender) {
		this.sender = sender;
	}

	public String getSenderName() {
		return senderName;
	}

	public void setSenderName(String senderName) {
		this.senderName = senderName;
	}

	public Date getLastMessageTime() {
		return lastMessageTime;
	}

	public void setLastMessageTime(Date lastMessageTime) {
		this.lastMessageTime = lastMessageTime;
	}

	public String getBody() {
		return body;
	}

	public void setBody(String body) {
		this.body = body;
	}


	public long getConversationId() {
		return conversationId;
	}

	public void setConversationId(long conversationId) {
		this.conversationId = conversationId;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Person getInitiator() {
		return initiator;
	}

	public void setInitiator(Person initiator) {
		this.initiator = initiator;
	}

	public Date getSessionStart() {
		return sessionStart;
	}

	public void setSessionStart(Date sessionStart) {
		this.sessionStart = sessionStart;
	}

	public Date getSessionEnd() {
		return sessionEnd;
	}

	public void setSessionEnd(Date sessionEnd) {
		this.sessionEnd = sessionEnd;
	}

	public Collection<Person> getParticipants() {
		return participants;
	}

	public void setParticipants(Collection<Person> participants) {
		this.participants = participants;
	}

	public Collection<Message> getMessages() {
		return messages;
	}

	public void setMessages(Collection<Message> messages) {
		this.messages = messages;
	}

	public boolean isHasNewMessages() {
		return hasNewMessages;
	}

	public void setHasNewMessages(boolean hasNewMessages) {
		this.hasNewMessages = hasNewMessages;
	}

	public boolean isLMSOnline() {
		return isLMSOnline;
	}

	public void setLMSOnline(boolean isLMSOnline) {
		this.isLMSOnline = isLMSOnline;
	}

}
