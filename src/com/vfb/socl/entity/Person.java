package com.vfb.socl.entity;

import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpSessionBindingEvent;
import javax.servlet.http.HttpSessionBindingListener;

import com.vfb.socl.mappers.PersonMapper;
import com.vfb.socl.util.DB;
import com.vfb.socl.util.XMPPUtil;

public class Person implements HttpSessionBindingListener {
	public String firstName;
	public String lastName;
	public String username;
	private String password;
	public String institute;
	public String position;
	public String email;
	public String phone;
	public Location from;
	public Location lives;
	public Boolean isOnline;
	public Boolean isOnXMPP;
	private Connections connections;
	private Date lastUpdate;
	private String notes;
	private Boolean email4newsletter;
	
	private XMPPUtil xmpp;
	
	private Boolean isSubscribed;
	
	private Boolean isAFriend;
	
	@Override
	public String toString() {
		return String.format("%s %s", this.getFirstName(), this.getLastName());
	}
	

	public String toStringLimit(int n) {
		return String.format("%s %s", this.getFirstName(), this.getLastName()).substring(0, n);
	}	

	@Override
	public void valueBound(HttpSessionBindingEvent arg0) {
		System.err.println("session bound: " + this.toString()); 
	}

	@Override
	public void valueUnbound(HttpSessionBindingEvent arg0) {
		if (this.xmpp!=null)
			this.xmpp.close();
		System.err.println("session unbound: " + this.toString());		
	}

	public String getFirstName() {
		if (firstName==null) return username; 
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		if (lastName==null) return ""; 
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getInstitute() {
		if (institute==null) return "";
		return institute;
	}

	public void setInstitute(String institute) {
		this.institute = institute;
	}

	public String getPosition() {
		if (position==null) return ""; 
		return position;
	}

	public void setPosition(String position) {
		this.position = position;
	}

	public Location getFrom() {
		if (from==null) return new Location(); 
		return from;
	}

	public void setFrom(Location from) {
		this.from = from;
	}

	public Location getLives() {
		if (lives==null) return new Location(); 
		return lives;
	}

	public void setLives(Location lives) {
		this.lives = lives;
	}

	public Boolean isOnline() {
		return isOnline;
	}

	public void setOnline(Boolean isOnline) {
		this.isOnline = isOnline;
	}

	public Boolean isOnXMPP() {
		if (xmpp!=null)
			return xmpp.isOnline(String.format("%s@%s", username, xmpp.SERVER_URL));
		return isOnXMPP;
	}
	
	
	public Connections getConncections() {
		try {
			DB db = new DB();
			PersonMapper pm = db.getPersonMapper();
			Date updated = pm.getLastUpdate(username);

			//System.err.println("updating friends list");
			List<Connection> l = pm.getPersonConnections(username);
			//System.err.println(Arrays.toString(l.toArray()));
			this.connections = new Connections();
			for (Connection c: l) {
				if ("A".equals(c.status)) {
					this.connections.accepted.add(c);
				}
				if ("R".equals(c.status)) {
					if ("outlink".equals(c.direction))
						this.connections.requests.add(c);
					else if ("inlink".equals(c.direction))
						this.connections.pending.add(c);
				}
				if ("H".equals(c.status)) {
					this.connections.hidden.add(c);
				}
				if ("D".equals(c.status)) {
					this.connections.denied.add(c);
				} 
			}
		} catch (Exception e) {
			throw new RuntimeException(e);
		}		
		return connections;
	}

	public String getEmail() {
		if (email==null) return ""; 
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhone() {
		if (phone==null) return ""; 
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public Date getLastUpdate() {
		return lastUpdate;
	}

	public void setLastUpdate(Date lastUpdate) {
		this.lastUpdate = lastUpdate;
	}

	public Boolean isAFriend() {
		return isAFriend;
	}

	public void setAFriend(Boolean isAFriend) {
		this.isAFriend = isAFriend;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	public Boolean isEmail4newsletter() {
		return email4newsletter;
	}

	public void setEmail4newsletter(Boolean email4newsletter) {
		this.email4newsletter = email4newsletter;
	}

	public XMPPUtil getXmpp() {
		return xmpp;
	}

	public void setXmpp(XMPPUtil xmpp) {
		this.xmpp = xmpp;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj==this) return true;
		if (obj instanceof Person) {
			if (((Person) obj).username.equals(this.username)) 
				return true;
			else 
				return false;
		}
		return false;
	}


	public Boolean isSubscribed() {
		return isSubscribed;
	}


	public void setSubscribed(Boolean isSubscribed) {
		this.isSubscribed = isSubscribed;
	}
}
