package com.vfb.socl.entity;

public class Location {
	public String city;
	public String country;
	public Location() {
		this.city = "";
		this.country = "";
	}
	public Location(String city, String country) {
		this.city = city;
		this.country = country;
	}
	
	@Override
	public String toString() {
		return String.format("%s, %s", city, country);
	}
}
