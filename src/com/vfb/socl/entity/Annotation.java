package com.vfb.socl.entity;

import java.util.Date;

public class Annotation {
	
	private Integer nodeId;
	private Long annotationId;
	private String geneId;
	private String structureId;
	private String location;
	private String strength;
	private String pattern;
	private String comments;
	private String stackId;
	private Date dateSubmited;
	
	private String username;
	
	private Boolean isSubmission;
	
	private Integer sumOfVotes;
	
	private Integer nUp;
	private Integer nDown;
	private Integer nSubmissions;
	
	private Integer nPossible;
	private Integer nWeak;
	private Integer nModerate;
	private Integer nStrong;
	
	private Integer nFlags;
	
	
	
	private Integer vote;
	private Boolean isFlagged;
	
	private String pathToNode;
	
	public Long getAnnotationId() {
		return annotationId;
	}
	public void setAnnotationId(long annotationId) {
		this.annotationId = annotationId;
	}
	public String getGeneId() {
		return geneId;
	}
	public void setGeneId(String geneId) {
		this.geneId = geneId;
	}
	public String getStructureId() {
		return structureId;
	}
	public void setStructureId(String structureId) {
		this.structureId = structureId;
	}
	public String getPosition() {
		return location;
	}
	public void setPosition(String position) {
		this.location = position;
	}
	public String getStrength() {
		return strength;
	}
	public void setStrength(String strength) {
		this.strength = strength;
	}
	public String getPattern() {
		return pattern;
	}
	public void setPattern(String pattern) {
		this.pattern = pattern;
	}
	public String getComments() {
		if (comments==null) return "";
		return comments;
	}
	public void setComments(String comments) {
		this.comments = comments;
	}
	public String getStackId() {
		return stackId;
	}
	public void setStackId(String stackId) {
		this.stackId = stackId;
	}
	public Date getDateSubmited() {
		return dateSubmited;
	}
	public void setDateSubmited(Date dateSubmited) {
		this.dateSubmited = dateSubmited;
	}
	public Integer getNodeId() {
		return nodeId;
	}
	public void setNodeId(int nodeId) {
		this.nodeId = nodeId;
	}
	public Boolean isFlagged() {
		return isFlagged;
	}
	public void setFlagged(boolean isFlagged) {
		this.isFlagged = isFlagged;
	}
	public String getPathToNode() {
		return pathToNode;
	}
	public void setPathToNode(String pathToNode) {
		this.pathToNode = pathToNode;
	}
	public Integer getVote() {
		return vote;
	}
	public void setVote(int vote) {
		this.vote = vote;
	}
	
	public Boolean hasThumbsUp() {
		return vote==1;
	}
	public boolean hasThumbsDown() {
		return vote==-1;
	}
	public Integer getSumOfVotes() {
		return sumOfVotes;
	}
	public void setSumOfVotes(int sumOfVotes) {
		this.sumOfVotes = sumOfVotes;
	}
	
	public Integer getNumberOfVotes() {
		return nUp+nDown;
	}
	public Integer getnDown() {
		return nDown;
	}
	public void setnDown(int nDown) {
		this.nDown = nDown;
	}
	public Integer getnUp() {
		return nUp;
	}
	public void setnUp(int nUp) {
		this.nUp = nUp;
	}
	
	public boolean similarTo(Annotation other) {
		if (this.structureId.equals(other.structureId) 
				&& this.geneId.equals(other.geneId) 
				&& this.strength.equals(other.strength)
				&& this.pattern.equals(other.pattern)) {
			return true;
		} else
			return false;
	}
	public Boolean isSubmission() {
		return isSubmission;
	}
	public void setSubmission(boolean isSubmission) {
		this.isSubmission = isSubmission;
	}
	public Integer getnSubmissions() {
		return nSubmissions;
	}
	public void setnSubmissions(int nSubmissions) {
		this.nSubmissions = nSubmissions;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public Integer getnPossible() {
		return nPossible;
	}
	public void setnPossible(Integer nPossible) {
		this.nPossible = nPossible;
	}
	public Integer getnWeak() {
		return nWeak;
	}
	public void setnWeak(Integer nWeak) {
		this.nWeak = nWeak;
	}
	public Integer getnModerate() {
		return nModerate;
	}
	public void setnModerate(Integer nModerate) {
		this.nModerate = nModerate;
	}
	public Integer getnStrong() {
		return nStrong;
	}
	public void setnStrong(Integer nStrong) {
		this.nStrong = nStrong;
	}
	public Integer getnFlags() {
		return nFlags;
	}
	public void setnFlags(Integer nFlags) {
		this.nFlags = nFlags;
	}
}
