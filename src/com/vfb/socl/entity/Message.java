package com.vfb.socl.entity;

import java.util.Date;

public class Message extends Person {
	private long messageId;
	private long conversationId;
	private String body;
	private Date messageSent;
	
	public long getMessageId() {
		return messageId;
	}
	public void setMessageId(long messageId) {
		this.messageId = messageId;
	}
	public long getConversationId() {
		return conversationId;
	}
	public void setConversationId(long conversationId) {
		this.conversationId = conversationId;
	}
	public String getSender() {
		return this.username;
	}
	public void setSender(String sender) {
		this.username = sender;
	}
	public String getBody() {
		return body;
	}
	public void setBody(String body) {
		this.body = body;
	}
	public Date getMessageSent() {
		return messageSent;
	}
	public void setMessageSent(Date messageSent) {
		this.messageSent = messageSent;
	}
}
