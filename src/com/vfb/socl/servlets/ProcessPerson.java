package com.vfb.socl.servlets;

import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.jivesoftware.smack.RosterEntry;

import com.vfb.socl.entity.ActiveSession;
import com.vfb.socl.entity.Connection;
import com.vfb.socl.entity.Person;
import com.vfb.socl.mappers.ActiveSessionsMapper;
import com.vfb.socl.mappers.MessageMapper;
import com.vfb.socl.mappers.PersonMapper;
import com.vfb.socl.util.DB;
import com.vfb.socl.util.ServletUtil;
import com.vfb.socl.util.XMPPUtil;

public class ProcessPerson {
	ServletUtil su;
	DB db;
	PersonMapper pm;
	Person user;
	Object savepoint;
	
	private boolean isLoggedIn = false;
	private boolean isLoginError = false;

	public ProcessPerson(String sessionId, String remoteAddr, String username, String password) throws Exception {

		if (username==null || username.length()==0 || password==null || password.length()==0) return;
		isLoggedIn = false;
		isLoginError = true;
		
		try {
			db = new DB();
			pm = db.getPersonMapper();
			
			user = pm.getPersonPwd(username, password);

			if (user!=null) {
				setLoggedIn(true);
				ActiveSessionsMapper asm = db.getSessionMapper();
				if (asm.setUsernameToSession(user.username, remoteAddr, sessionId)==0) {
					//System.err.println("had to create new session");
					ActiveSession as = new ActiveSession(sessionId, user.getUsername());
					asm.closeOldSessions(user.username);
					asm.createSession(as);
					asm.setUsernameToSession(user.username, remoteAddr, sessionId);
					isLoginError = false;
				}
	
				try {
					user.setXmpp(new XMPPUtil(username, password));
					Collection<RosterEntry> rosterList = user.getXmpp().getRosterList();
					System.out.println("roster list: " + rosterList);
				} catch (Exception e) {
					throw new RuntimeException(e);
				}
			}
			db.commit();
		} catch (Exception e) {
			db.rollback(savepoint);
			System.err.println(e.getMessage());
			throw e;
		}
			
		this.su = new ServletUtil();

	}

	
	
	public ProcessPerson(Person user) throws Exception {
		this.db = new DB();
		this.pm = db.getPersonMapper();
		this.su = new ServletUtil();
		this.user = user;
	}
	
	public ResponseObject seekPerson(HttpServletRequest request) {
		ResponseObject ro = new ResponseObject("seekPerson"); 
		String query = request.getParameter("q");
		//System.out.println("query: " + query);
		query = query.replaceAll("[^\\p{L}\\p{N}|&\\!\\?()]", "_");
		query = query.replaceAll("[_]&[_]", "&").replaceAll("[_]![_]", "!").replaceAll("[_]\\?[_]", "?");

		if (query==null || "".equals(query)) {
			ro.message = new LinkedList<Person>();
		} else {
			ro.message = pm.seekPerson(query);	
		}
		
		return ro;
	}

	public ResponseObject acceptConnectionRequest(HttpServletRequest request) throws Exception {
		ResponseObject ro = new ResponseObject("acceptConnectionRequest");
		try {
			String username = request.getParameter("username");
			savepoint = db.createSavePoint();
			Connection conn = pm.getConnection(user.username, username);
			if (conn!=null) {
				if ("inlink".equals(conn.direction)) {
					pm.changeConnectionRequestStatus(user.username, username, "A");
					user.getXmpp().acceptRequest(username + "@" + user.getXmpp().SERVER_URL);
				} else {
					// you can not accept your own request
					throw new Exception("you can not accept your own request");
				}
			}
			ro.message = "OK";
			db.commit();
		} catch (Exception e) {
			db.rollback(savepoint);
			throw new RuntimeException(e);
		}
		return ro;
	}
	
	public ResponseObject connectTo(HttpServletRequest request) {
		ResponseObject ro = new ResponseObject("connectTo");
		try {
			String username = request.getParameter("username");
			Connection conn = pm.getConnection(user.username, username);
			if (conn==null) {
				pm.connectTo(user.username, username, request.getParameter("relationship"), "R");
				pm.setProfileUpdated(user.username);
				MessageMapper mm = db.getMessageMapper();
				Long conversationId = mm.getConversationIDParticipants(user.username, username);
				if (conversationId==null) {
					conversationId = mm.createConversation("Friend request", user.username, "private", "");
					mm.addParticipant(conversationId, user.username, user.username);
					mm.addParticipant(conversationId, username, user.username);
				}
				String body = "I have added you as friend. Yaay!";
				mm.createMessage(conversationId, user.username, body);
				user.getXmpp().connectTo(username + "@" + user.getXmpp().SERVER_URL, username, new String[]{"friends"});
				user.getXmpp().acceptRequest(username + "@" + user.getXmpp().SERVER_URL);
				user.getXmpp().sendMessage(String.format("%s@%s", username, user.getXmpp().SERVER_URL), body);
			} else {
				if ("inlink".equals(conn.direction)) {
					// he/she requested this connection before - we might as well accept it ;)
					pm.changeConnectionRequestStatus(user.username, username, "A");
					user.getXmpp().connectTo(username + "@" + user.getXmpp().SERVER_URL, username, new String[]{"friends"});
				} else {
					// error - connection request already exists
				}
			}
			db.commit();
		} catch (Exception e) {
			db.rollback(savepoint);
			throw new RuntimeException(e);
		}
		return ro; 
	}

	public ResponseObject createPerson(HttpServletRequest request) {
		ResponseObject ro = new ResponseObject("createPerson");
		Person newUser = new Person();
		try {
			su.save(request, newUser);
			pm.createPerson(newUser);
			pm.createPersonDetail(newUser);
			ro.message = "ok";
			// create user in XMPP server
			//XMPPUtil u = user.getXmpp();
			XMPPUtil u = new XMPPUtil();
			u.createUser(newUser.getUsername(), newUser.getPassword());
			db.commit();
		} catch (Exception e) {
			db.rollback(savepoint);
			throw new RuntimeException(e);
		}
		return ro;
	}

	public ResponseObject updatePerson(HttpServletRequest request) {
		ResponseObject ro = new ResponseObject("updatePerson");
		pm.setProfileUpdated(user.username);
		return ro;
	}

	public ResponseObject changePassword(HttpServletRequest request) {
		ResponseObject ro = new ResponseObject("changePassword");
		try {
			String newPassword = request.getParameter("newPassword");
			String oldPassword = request.getParameter("oldPassword");
			if (oldPassword==null || oldPassword.length()<4) {
				throw new Exception("password was never shorter than 4 symbols");
			}
			if (newPassword==null || newPassword.length()<4) {
				throw new Exception("password must never ever be shorter than 4 symbols");
			}
			if (pm.changePassword(user.username, oldPassword, newPassword)==1) {
				XMPPUtil u = user.getXmpp();
				u.setPassword(user.username, oldPassword, newPassword);
				ro.message = "ok";
				db.commit();
			} else {
				throw new Exception("troubles updating password, please try again or later. If it still does not work, conntact support.");
			}
		} catch (Exception e) {
			db.rollback(savepoint);
			throw new RuntimeException(e);
		}		
		return ro;
	}



	public boolean isLoggedIn() {
		return isLoggedIn;
	}



	public void setLoggedIn(boolean isLoggedIn) {
		this.isLoggedIn = isLoggedIn;
	}



	public boolean isLoginError() {
		return isLoginError;
	}

	public Person getUser() {
		return this.user;
	}

	public void setLoginError(boolean isLoginError) {
		this.isLoginError = isLoginError;
	}
}
