package com.vfb.socl.servlets;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.ibatis.exceptions.PersistenceException;
import org.postgresql.util.PSQLException;

import com.google.gson.Gson;
import com.vfb.socl.entity.Annotation;
import com.vfb.socl.entity.Person;
import com.vfb.socl.exception.UnAuthorizedException;
import com.vfb.socl.mappers.AnnotationMapper;
import com.vfb.socl.util.DB;
import com.vfb.socl.util.ServletUtil;

/**
 * Servlet implementation class LoadAnnotations
 */
// url load_annotations
public class DoAnnotations extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public DoAnnotations() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("application/json");
		String action = request.getParameter("action");
		ResponseObject responseObject = null;
		responseObject = new ResponseObject(action);
		Person user = (Person) request.getSession().getAttribute("user");
		
		Gson g = new Gson();
		try {
			if (user==null) {
				throw new UnAuthorizedException("secure API - you must be logged in");
			}
			ProcessAnnotation pa = new ProcessAnnotation(user);
			if ("store".equals(action)) {
				responseObject = pa.store(request);
			} else
			if ("vote".equals(action)) {
				responseObject = pa.vote(request);
			} else
			if ("flag".equals(action)) {
				responseObject = pa.flag(request);
			} else
			if ("getStats".equals(action)) {
				responseObject = pa.getStats(request);
			} else
			if ("getMostFavorableAnnotations".equals(action)) {
				responseObject = pa.getMostFavorableAnnotations(request);
			} else 
			if ("getNewAnnotations".equals(action)) {
				responseObject = pa.getNewAnnotations(request);
			}
		} catch (Exception e) {
			responseObject.error = "yes";
			responseObject.message = e.getMessage();
			System.err.println(e.getMessage());
			if (!(e instanceof UnAuthorizedException))
				e.printStackTrace();
		}
		response.getWriter().write(g.toJson(responseObject));
		g=null;
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
