package com.vfb.socl.servlets;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.vfb.socl.entity.Person;
import com.vfb.socl.exception.UnAuthorizedException;

/**
 * Servlet implementation class DoPerson
 */
public class DoPerson extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public DoPerson() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("application/json");
		String action = request.getParameter("action");
		ResponseObject responseObject = null;
		responseObject = new ResponseObject(action);
		Person user = (Person) request.getSession().getAttribute("user");
		
		Gson g = new Gson();
		try {
			if (user==null && !("createPerson".equals(action))) {
				throw new RuntimeException("secure API - you must be logged in");
			}
			ProcessPerson pa = new ProcessPerson(user);
			if ("seekPerson".equals(action)) {
				responseObject = pa.seekPerson(request);
				response.getWriter().write(g.toJson(responseObject.message));
			} else
			if ("createPerson".equals(action)) {
				responseObject = pa.createPerson(request);
				response.getWriter().write(g.toJson(responseObject));
			} else
			if ("updatePerson".equals(action)) {
				responseObject = pa.updatePerson(request);
				response.getWriter().write(g.toJson(responseObject));
			} else
			if ("changePassword".equals(action)) {
				responseObject = pa.changePassword(request);
				response.getWriter().write(g.toJson(responseObject));
			} else				
			if ("connectTo".equals(action)) {
				responseObject = pa.connectTo(request);
				response.getWriter().write(g.toJson(responseObject));
			} else
			if ("acceptConnectionRequest".equals(action)) {
					responseObject = pa.acceptConnectionRequest(request);
					response.getWriter().write(g.toJson(responseObject));
			} else{
				response.getWriter().write(g.toJson(responseObject));
			}
		} catch (Exception e) {
			responseObject.error = "yes";
			responseObject.message = e.getMessage();
			response.getWriter().write(g.toJson(responseObject));
			if (!(e instanceof UnAuthorizedException))
				e.printStackTrace();
		}
		g=null;
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
