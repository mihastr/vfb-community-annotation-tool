package com.vfb.socl.servlets;

import java.io.IOException;

import javax.management.RuntimeErrorException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.vfb.socl.entity.Person;
import com.vfb.socl.exception.UnAuthorizedException;

/**
 * Servlet implementation class DoMessages
 */
public class DoMessages extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public DoMessages() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("application/json");
		String action = request.getParameter("action");
		ResponseObject responseObject = null;
		responseObject = new ResponseObject(action);
		Person user = (Person) request.getSession().getAttribute("user");
		
		Gson g = new Gson();
		try {
			if (user==null) {
				throw new UnAuthorizedException("secure API - you must be logged in");
			}
			ProcessMessage pa = new ProcessMessage(user);
			if ("getThreads".equals(action)) {
				responseObject = pa.getThreads(request);
			} else
			if ("getMessages".equals(action)) {
				responseObject = pa.getMessages(request);
			} else			
			if ("addParticipant".equals(action)) {
				responseObject = pa.addParticipant(request);
			} else
			if ("getParticipants".equals(action)) {
					responseObject = pa.getParticipants(request);
			} else
			if ("sendMessage".equals(action)) {
				responseObject = pa.addMessage(request);
			} else
			if ("getNewMessages".equals(action)) {
				responseObject = pa.getNewMessages(request);
			}
		} catch (Exception e) {
			responseObject.error = "yes";
			responseObject.message = e.getMessage();
			
			if (!(e instanceof UnAuthorizedException))
				e.printStackTrace();
		}
		response.getWriter().write(g.toJson(responseObject));
		g=null;
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
