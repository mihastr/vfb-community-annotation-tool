package com.vfb.socl.servlets;

public class ResponseObject {

	public String error = "no";
	public String action = "";
	public Object message = null;
	
	public ResponseObject(String action) {
		this.action = action;
	}
}
