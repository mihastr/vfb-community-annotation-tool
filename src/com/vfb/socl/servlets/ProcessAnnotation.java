package com.vfb.socl.servlets;

import java.util.HashMap;
import java.util.List;
import java.sql.SQLException;

import javax.servlet.http.HttpServletRequest;

import com.vfb.socl.entity.Annotation;
import com.vfb.socl.entity.Person;
import com.vfb.socl.mappers.AnnotationMapper;
import com.vfb.socl.mappers.MessageMapper;
import com.vfb.socl.util.DB;
import com.vfb.socl.util.ServletUtil;

public class ProcessAnnotation {
	ServletUtil su;
	DB db;
	AnnotationMapper am;
	Person user;
	Object savepoint;

	public ProcessAnnotation(Person user) throws Exception {
		this.db = new DB();
		this.am = db.getAnnotationMapper();
		this.su = new ServletUtil();
		this.user = user;
	}

	public ResponseObject store(HttpServletRequest request) throws Exception {
		ResponseObject responseObject = new ResponseObject("store");
		Annotation newAnnotation = new Annotation();
		su.save(request, newAnnotation);
		responseObject.message = newAnnotation;
		try {
			savepoint = db.createSavePoint();
			Annotation existingAnnotation = am.getAnnotationUser(user.username, newAnnotation.getStructureId(), newAnnotation.getGeneId());
			if (existingAnnotation==null) {
				// I have not voted or submitted such annotation, someone else might have
				existingAnnotation = am.getAnnotation(newAnnotation.getStructureId(), newAnnotation.getGeneId(), newAnnotation.getPattern(), newAnnotation.getStrength());
				if (existingAnnotation==null) {
					// nobody has submitted such annotation for this structure/gene
					// explicit submission
					newAnnotation.setAnnotationId(am.insertAnnotation(newAnnotation));
					// store vote
					am.insertVote(user.username, newAnnotation.getAnnotationId(), 1, true);
				} else {
					// somebody else has submitted this kind of annotation
					// store it as a vote (and implicit submission) - two lines down
					am.insertVote(user.username, existingAnnotation.getAnnotationId(), 1, true);
				}
			} else {
				// I have submitted annotation on this structure/gene before 
				// but I have changed my mind or I am just stupid an I have submitted it again
				if (existingAnnotation.similarTo(newAnnotation)) {
					// don't actually have to do anything?? submitted same shit twice :)
				} else {
					// remove the entry from annotation_vote
					am.deleteAnnotationVote(existingAnnotation.getAnnotationId(), user.username);
					
					//TODO: check logic with Nestor
					//am.updateUsernameForAnnotation(existingAnnotation.getAnnotationId());
					
					// find annotation_id with same structure/gene/pattern/strength
					existingAnnotation = am.getAnnotation(newAnnotation.getStructureId(), newAnnotation.getGeneId(), newAnnotation.getPattern(), newAnnotation.getStrength());
					if (existingAnnotation!=null) {
						// + if found insert annotation_vote with annotation_id
						am.deleteAnnotationVote(existingAnnotation.getAnnotationId(), user.username);
						am.insertVote(user.username, existingAnnotation.getAnnotationId(), 1, true);
					} else {
						// + if not found insert new annotation
						// explicit submission
						newAnnotation.setAnnotationId(am.insertAnnotation(newAnnotation));
						// store vote
						am.insertVote(user.username, newAnnotation.getAnnotationId(), 1, true);				
					}

				}
			}
			responseObject.message = newAnnotation;
			db.commit();
		} catch (Exception pe) {
			db.rollback(savepoint);
			throw pe;
			/*
			if (pe.getCause() instanceof SQLException) {
				SQLException cause = (SQLException) pe.getCause();
				if ("23505".equals(cause.getSQLState())) {
					// dupplicate key? No worries - just have to update row with
					// new valeus
					try {
						savepoint = db.createSavePoint();
						am.updateAnnotation(a);
						db.commit();
					} catch (Exception e) {
						db.rollback(savepoint);
						throw e;
					}
				} else {
					throw pe;
				}
			}
			*/
		}
		return responseObject;
	}

	public ResponseObject vote(HttpServletRequest request) throws Exception {
		ResponseObject responseObject = new ResponseObject("vote");
		try {
			long annotationId = Integer.parseInt(request.getParameter("annotationId"));
			int vote = 0;
			if ("1".equals(request.getParameter("vote"))) {
				vote = 1;
			} else if ("0".equals(request.getParameter("vote"))) {
				vote = 0;
			} else {
				vote = -1;
			}
			try {
				savepoint = db.createSavePoint();
				// check if is allowed to vote on this node
				Boolean isVoteSubmission = am.checkIfVoteIsSubmission(user.username, annotationId);
				if (isVoteSubmission==null) {
					// user did not vote yet and has not submitted the vote
					am.insertVote(user.username, annotationId, vote, false);
				} else {
					if (isVoteSubmission==false) {
						am.deleteAnnotationVote(annotationId, user.username);
						am.insertVote(user.username, annotationId, vote, false);
					} else {
						throw new RuntimeException("You published this annotation, can not vote for it");
					}
				}
				responseObject.message = request.getParameter("nodeId");
				db.commit();
			} catch (Exception pe) {
				db.rollback(savepoint);
				throw pe;
			}
		} catch (Exception e) {
			responseObject.error = "yes";
			throw e;
		}
		return responseObject;
	}
	

	public ResponseObject flag(HttpServletRequest request) {
		ResponseObject responseObject = new ResponseObject("flag");
		try {
			boolean isFlagged = false;
			//System.err.println(request.getParameter("isFlagged"));
			if ("true".equals(request.getParameter("isFlagged"))) {
				isFlagged = true;
			} 
			long annotationId = Integer.parseInt(request.getParameter("annotationId"));
	
			try {
				savepoint = db.createSavePoint();
				
				// 1. save flag
				am.setFlag(annotationId, isFlagged, user.username);
				
				// 2. publish on discussion board
				Long conversationId = am.getAnnotationDiscussionBoardByAnnotationID(annotationId);
				MessageMapper mm = db.getMessageMapper();
				Annotation annotation = am.getAnnotationByAnnotationId(annotationId);
				if (conversationId==null) {
					conversationId = mm.createConversation(annotation.getStructureId() + " - " + annotation.getGeneId() + " flagged", user.username, "public", "");
					am.setAnnotationDiscussionBoard(annotation.getGeneId(), annotation.getPathToNode(), conversationId);
				}
				if (!mm.checkParticipant(user.username, conversationId)) {
					mm.addParticipant(conversationId, user.username, user.username);	
				}
				String messageBody=null;
				if (isFlagged) {
					messageBody = "I have flagged annotation (%s / %s) - (%s - %s)";
				} else {
					messageBody = "I have removed flag from annotation:  (%s / %s) - (%s - %s)";
				}
				messageBody = String.format(messageBody, annotation.getStructureId(), annotation.getGeneId(), annotation.getPattern(), annotation.getStrength());
				mm.createMessage(conversationId, user.username, messageBody);
				
				// 3. send XMPP message to subscribers
				ProcessMessage pm = new ProcessMessage(user);
				pm.sendMessageToSubscibers(mm.getParticipants(user.username, conversationId), messageBody);
				db.commit();
			} catch (Exception pe) {
				db.rollback(savepoint);
				throw pe;
			}
		} catch (Exception e) {
			responseObject.error = "yes";
			System.err.println(e.getMessage());
			responseObject.message = e.getMessage();
		}
		return responseObject;
	}
	
	public ResponseObject getStats(HttpServletRequest request) {
		ResponseObject responseObject = new ResponseObject("getStats");
		try {
			String annotationId = request.getParameter("annotationId");
			responseObject.message = am.getAnnotationEntrisWithVotes(Long.parseLong(annotationId));
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
		return responseObject;
	}

	public ResponseObject getMostFavorableAnnotations(HttpServletRequest request) {
		ResponseObject responseObject = new ResponseObject("getMostFavorableAnnotations");
		try {
			HashMap<String, Object> rv = new HashMap<String, Object>();
			rv.put("annotations", am.getMostFavorableAnnotations());
			rv.put("entry_count", am.getAnnotationStats());
			responseObject.message = rv;
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
		return responseObject;
	}

	public ResponseObject getNewAnnotations(HttpServletRequest request) {

		ResponseObject responseObject = new ResponseObject("getNewAnnotations");
		try {
			HashMap<String, Object> rv = new HashMap<String, Object>();
			rv.put("annotations", am.getNewAnnotations(user.username));
			rv.put("flags", am.getFlags());
			responseObject.message = rv;
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
		return responseObject;
	}

	public ResponseObject getAnnotationsForUser(String username) {

		ResponseObject responseObject = new ResponseObject("getAnnotationsForUser");
		try {
			responseObject.message = am.getAnnotationsForUser(username);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
		return responseObject;
	}

	public ResponseObject getAnnotationsVotesForUser(String username) {

		ResponseObject responseObject = new ResponseObject("getAnnotationVotesForUser");
		try {
			responseObject.message = am.getAnnotationsVotesForUser(username);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
		return responseObject;
	}
	
	public ResponseObject getAnnotationsFlagsForUser(String username) {

		ResponseObject responseObject = new ResponseObject("getAnnotationsFlagsForUser");
		try {
			responseObject.message = am.getFlagsForUser(username);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
		return responseObject;
	}
	
	
	
}
