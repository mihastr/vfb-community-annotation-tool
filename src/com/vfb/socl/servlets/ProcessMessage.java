package com.vfb.socl.servlets;

import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

import javax.management.RuntimeErrorException;
import javax.servlet.http.HttpServletRequest;

import org.jivesoftware.smack.Chat;
import org.jivesoftware.smack.XMPPException;

import com.vfb.socl.entity.Message;
import com.vfb.socl.entity.Person;
import com.vfb.socl.exception.NoSuchChatException;
import com.vfb.socl.exception.UnAuthorizedException;
import com.vfb.socl.mappers.MessageMapper;
import com.vfb.socl.util.DB;
import com.vfb.socl.util.ServletUtil;

public class ProcessMessage {
	ServletUtil su;
	Person user;
	Object savepoint;
	
	private String XMPPSERVER;
	
	public ProcessMessage(Person user) throws Exception {
		this.su = new ServletUtil();
		this.user = user;
		if (null!=user.getXmpp())
			this.XMPPSERVER = user.getXmpp().SERVER_URL;
	}
	
	public ResponseObject getThreads(HttpServletRequest request) {
		ResponseObject ro = new ResponseObject("getThreads");
		try {
			DB db = new DB();
			MessageMapper am = db.getMessageMapper();
			ro.message = am.getThreads(user.username);
			db.commit();
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
		return ro;
	}
	
	public ResponseObject getMessages(HttpServletRequest request) {
		ResponseObject ro = new ResponseObject("getMessages");
		try {
			Long conversationId; 
			long lastMsgId = 0;
			try {
				String cidParam = request.getParameter("conversationId");
				if (cidParam==null) {
					throw new Exception("conversation id shall not be null");
				}
				//System.err.println("cidParam:  " + cidParam);
				conversationId = (long)Integer.parseInt(cidParam);
				String lmid = request.getParameter("lastMsgId");
				if (lmid!=null)
					lastMsgId  = Long.parseLong(request.getParameter("lastMsgId"));
				else
					lastMsgId = 0;
			} catch (Exception e) {
				conversationId = null;
				lastMsgId = 0;
				e.printStackTrace();
			}
			if (conversationId==null) throw new Exception("coversation id must be specified and have numeric value");

			DB db = new DB();
			MessageMapper am = db.getMessageMapper();
			
			try {
				savepoint = db.createSavePoint();
				ro.message = am.getMessages(conversationId, user.username, lastMsgId);
				if (ro.message!=null && ((Collection<Message>)ro.message).size()>0) {
					am.updateUserLastReadMessage(conversationId, user.username, (Long)((List<Message>)ro.message).get(0).getMessageId());
					
				}
				db.commit();
			} catch (Exception e) {
				if (savepoint!=null)
					db.rollback(savepoint);
			}
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
		return ro;
	}
	
	public ResponseObject addParticipant(HttpServletRequest request) {
		ResponseObject ro = new ResponseObject("addParticipant");
		long conversationId = 0;
		try {
			conversationId = Long.parseLong(request.getParameter("conversationId"));
		} catch (Exception e) {
			conversationId = 0;
		}
		try {
			String recipient = request.getParameter("recipient");
			DB db = new DB();
			MessageMapper am = db.getMessageMapper();
			savepoint = db.createSavePoint();
			if (conversationId==0) {
				// new thread
				String threadId = user.getXmpp().createChat(String.format("%s@%s", user.username, XMPPSERVER));
				conversationId = am.createConversation("Chat", user.username, "private", threadId);
				am.addParticipant(conversationId, user.username, user.username);
				am.addParticipant(conversationId, recipient, user.username);
			} else {
				// existing thread
				List<Person> participants = (List<Person>) am.getParticipants(user.username, conversationId);
				if (participants.size()==1) {
					// will be a private chat between me and some one else
					am.addParticipant(conversationId, recipient, user.username);
				} else 
				if (participants.size()==0) {
					// probably do not have access to the conversation
					throw new UnAuthorizedException("no access to this thread");
				} else if (participants.size()>1) {
					if (am.isDiscussionBoard(conversationId) || am.isThreadPublic(conversationId)) {
						am.addParticipant(conversationId, recipient, user.username);
						Person newParticipant = new Person();
						newParticipant.setUsername(recipient);
						participants.add(newParticipant);
						sendMessageToSubscibers(participants, "Hi " + recipient + "! " + user.username + " added you to conversation. Check messages online. Unsubscribe options avalibale");
					} else {
						// proper group chat
						Person newParticipant = new Person();
						newParticipant.setUsername(recipient);
						invitePersonToGroupChat(conversationId, user, newParticipant, participants);
					}
				} else {
					throw new RuntimeException("strange - negative number is not allowed");
				}
			}
			db.commit();
			ro.message = conversationId;
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
		return ro;
	}
	
	public ResponseObject addMessage(HttpServletRequest request) {
		System.err.println("addMessage");
		ResponseObject ro = new ResponseObject("addMessage");
		DB db=null;
		try {
			db = new DB();
			MessageMapper am = db.getMessageMapper();
			savepoint = db.createSavePoint();
			long conversationId = 0;
			try {
				conversationId = Long.parseLong(request.getParameter("conversationId"));
			} catch (Exception e) {
				conversationId = 0;
			}
			String body = (String)request.getParameter("body");
			if (conversationId==0) {
				// create new thread
				try {
					String visibility = (String)request.getParameter("visibility");
					String ownerString  = (String)request.getParameter("owner");
					String entityIdString  = (String)request.getParameter("entityId");
					
					conversationId = am.createConversation(request.getParameter("title"), user.username, visibility, "");
					ro.message = conversationId;
					
					am.addParticipant(conversationId, user.username, user.username);
					if (ownerString!=null && !"null".equals(ownerString) && entityIdString!=null && !"null".equals(entityIdString)) {
						am.createDiscussionBoard(ownerString, entityIdString, conversationId);
					}
					am.createMessage(conversationId, user.username, body);
				} catch (Exception e) {
					throw new RuntimeException(e);
				}
			} else {
				// check if user is member of conversation
				if (am.checkParticipant(user.username, conversationId)) {
					// if he is add the message
					//System.err.println(String.format("storing message in the DB (%s, %s, %s)", conversationId, user.username, body));
					am.createMessage(conversationId, user.username, body);
					//System.err.println("storing message in the DB - DONE ");
					
					// XMPP magic
					ResponseObject participants = getParticipants(request);
					System.out.println(participants.message);
					if (participants.message instanceof List) {
						List<Person> col = (List<Person>) participants.message;
						
						// 1. private chat
						if (col.size()==2) {
							Person p1 = col.get(0);
							Person p2 = col.get(1);
							String to = String.format("%s@%s", (p1.equals(user) ? p2.username : p1.username), XMPPSERVER);
							//System.out.println("private chat with: " + to );
							String threadId = user.getXmpp().sendMessage(to, body);
							am.setThreadIdToConversation(conversationId, threadId);
						} else {
						// this is a multi-user chat
							if (am.isDiscussionBoard(conversationId)) {
								// send XMMP message to subscribers
								sendMessageToSubscibers(col, body);
							} else {
								sendMessageToGroupChat(conversationId, body, col);
							}
						}
					} else {
						System.err.println("participants.message is " + participants.message.getClass());
					}
				} else {
					// if he is not, check if thread is public		
					if (am.isThreadPublic(conversationId)) {
						// user is not a participant, add him
						am.addParticipant(conversationId, user.username, user.username);
						// send message
						am.createMessage(conversationId, user.username, body);
						
						// xmpp magic
						ResponseObject participants = getParticipants(request);
						List<Person> people = (List<Person>)participants.message;
						if (people.size()==2) {
							// private chat	
							Person p1 = people.get(0);
							Person p2 = people.get(1);
							String to = String.format("%s@%s", (p1.equals(user) ? p2.username : p1.username), XMPPSERVER);
							System.out.println("private chat with: " + to );
							String threadId = user.getXmpp().sendMessage(to, body);
							am.setThreadIdToConversation(conversationId, threadId);
						} else {
							if (am.isDiscussionBoard(conversationId)) {
								// send XMMP message to subscribers
								sendMessageToSubscibers(people, body);
							} else {
								sendMessageToGroupChat(conversationId, body, people);
							}
							//System.out.println(participants.message);
						}
						
						
					} else {
						throw new RuntimeException("thread is not public, ask moderator to join");
					}
				}
			}
			if (db!=null) {
				db.commit();
			}
			return ro;
		} catch (Exception e) {
			if (db!=null)
				db.rollback(savepoint);
			throw new RuntimeException(e);
		}
	}
	
	public void sendMessageToGroupChat(long conversationId, String body, List<Person> participants) throws Exception {
		try {
			user.getXmpp().sendGroupMessage(conversationId, body);
		} catch (NoSuchChatException e) {
			user.getXmpp().createGroupChat(conversationId, user, participants);
			user.getXmpp().sendGroupMessage(conversationId, body);
		} catch (XMPPException e) {
			throw e;
		}
	}

	public void sendMessageToSubscibers(Collection<Person> participants, String body) throws XMPPException {
		for (Person p: participants) {
			if (!p.equals(user))
			if (p.isSubscribed()) {
				System.out.println("sent subscription message to: " + p.getUsername());
				String to = String.format("%s@%s", p.getUsername(), XMPPSERVER);
				String threadId = user.getXmpp().sendMessage(to, body);
			}
		}
	}

	private void invitePersonToGroupChat(long conversationId, Person creator, Person participant, List<Person> participants) throws Exception {
		user.getXmpp().invitePerson(conversationId, creator, participant, participants);
	}
	
	/**
	 * Get participants, gets the participants of the conversation or a discussion
	 * */
	public ResponseObject getParticipants(HttpServletRequest request) {
		//System.out.println("*** checking presence information ***");
		ResponseObject ro = new ResponseObject("getParticipants");
		long conversationId = 0;
		try {
			conversationId = Long.parseLong(request.getParameter("conversationId"));
		} catch (Exception e) {
			ro.message = "conversationId must existing conversation id and you have to be participant in that conversation";
		}
		try {
			DB db = new DB();
			MessageMapper am = db.getMessageMapper();
			ro.message = am.getParticipants(user.username, conversationId);
			if (ro.message!=null) {
				if (ro.message instanceof Collection)
				for (Person p: (Collection<Person>)ro.message) {
					p.isOnXMPP = user.getXmpp().isOnline(String.format("%s@%s", p.username, XMPPSERVER));
				}
			}
			db.commit();
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
		//System.out.println("*** checking presence information - DONE ***");
		return ro;
	}

	public ResponseObject getNewMessages(HttpServletRequest request) throws Exception {
		ResponseObject ro = new ResponseObject("getNewMessages");
		DB db=null;
		try {
			db = new DB();
			MessageMapper am = db.getMessageMapper();
			savepoint = db.createSavePoint();
			ro.message = am.getNewMessages(user.username);
			db.commit();
		} catch (Exception e) {
			db.rollback(savepoint);
			throw e;
		}
		return ro;
	}
	
	public ResponseObject getDiscussionsForUser(String username) throws Exception {
		ResponseObject ro = new ResponseObject("getDiscussionsForUser");
		DB db=null;
		try {
			db = new DB();
			MessageMapper am = db.getMessageMapper();
			savepoint = db.createSavePoint();
			ro.message = am.getDiscussionBoardsForUser(username);
			db.commit();
		} catch (Exception e) {
			db.rollback(savepoint);
			throw e;
		}
		return ro;
		
	}
}
