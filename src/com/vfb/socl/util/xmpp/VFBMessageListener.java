package com.vfb.socl.util.xmpp;


import java.util.Arrays;
import java.util.List;

import org.jivesoftware.smack.Chat;
import org.jivesoftware.smack.MessageListener;
import org.jivesoftware.smack.PacketListener;
import org.jivesoftware.smack.packet.Message;
import org.jivesoftware.smack.packet.Message.Type;
import org.jivesoftware.smack.packet.Packet;
import org.jivesoftware.smack.packet.Presence;

import com.vfb.socl.mappers.MessageMapper;
import com.vfb.socl.mappers.PersonMapper;
import com.vfb.socl.util.DB;
import com.vfb.socl.entity.Connection;
import com.vfb.socl.entity.Person;


public class VFBMessageListener implements MessageListener, PacketListener {

	/**
	 * processes messages from private chats
	 * */
	@Override
	public void processMessage(Chat chat, Message msg) {
		DB db=null;
		Object sp=null;

		Boolean isStoredInDB = (Boolean) msg.getProperty("isStoredInDB");
		if (isStoredInDB!=null && isStoredInDB) {
			// message already stored in the database :)
			return;
		}
		try { 
			if (msg.getType().equals(Type.chat) && msg.getBody()!=null) {
				System.out.println("*****");
				if (chat!=null)
					System.out.println("Thread id: " + chat.getThreadID());
				else {
					System.out.println("processing group chat message: \n" + msg.toXML());
				}
				String[] from = msg.getFrom().split("@");
				String[] to = msg.getTo().split("@");
				System.out.println(String.format("%s\t%s\t%s", Arrays.toString(from), Arrays.toString(to),  msg.getBody()));
				//chat.sendMessage(msg);
				db = new DB();
				sp = db.createSavePoint();
				MessageMapper mm = db.getMessageMapper();
				Long conversationId = null;
				if (chat!=null)
					conversationId = mm.getConversationByXMMPThreadId(chat.getThreadID());
				else
					conversationId = null;
				
				if (conversationId==null) conversationId = Long.valueOf(0);
				boolean createNewThread = false;
				if (conversationId==0) {
					// create new thread, add participants
					conversationId = mm.getConversationIDParticipants(from[0], to[0]);
					if (conversationId==null) {
						createNewThread = true;
						conversationId = Long.valueOf(0);
					} else {
						// found some matching conversation
						createNewThread = false;
					}
				}
					
				if (!createNewThread) {
					boolean isSenderParticipant = mm.checkParticipant(from[0], conversationId);
					boolean isReceiverParticipant = mm.checkParticipant(to[0], conversationId);
					boolean isThreadPublic = mm.isThreadPublic(conversationId);
					if (isSenderParticipant && isReceiverParticipant) {
						mm.createMessage(conversationId, from[0], msg.getBody());	
					} else {
						if (isThreadPublic) {
							// probably a discussion board :) treat it as it is
							if (isSenderParticipant) {
								mm.addParticipant(conversationId, from[0], "xmpp");
							} else {
								mm.addParticipant(conversationId, from[0], "xmpp");
							}
							mm.createMessage(conversationId, from[0], msg.getBody());
						} else {
							// create new thread
							createNewThread = true;
						}
					}
				} else {
					conversationId = mm.createConversation("chat", from[0], "private", chat.getThreadID());
					mm.addParticipant(conversationId, from[0], from[0]);
					mm.addParticipant(conversationId, to[0], from[0]);
					mm.createMessage(conversationId, from[0], msg.getBody());
				}
			}
			if (db!=null)
				db.commit();
		} catch (Exception e) {
			if (db!=null && sp!=null)
				db.rollback(sp);
			System.err.println(e.getMessage());
			throw new RuntimeException(e);
		}
	}

	@Override
	public void processPacket(Packet packet) {
		if (packet instanceof Message) {
			Message msg = (Message)packet;
			processMessage(null, msg);
		} else if (packet instanceof Presence) {
			Presence presence = (Presence)packet;
			if (presence.getType().equals(Presence.Type.subscribe)) {
				System.out.println(presence.getFrom() + " wants to be friend with: " + presence.getTo());
				DB db=null;
				Object sp=null;
				try {
					db = new DB();
					sp = db.createSavePoint();
					PersonMapper pm = db.getPersonMapper();
					Connection conn = pm.getConnection(presence.getFrom().split("@")[0], presence.getTo().split("@")[0]);
					if (conn!=null) {
						if ("inlink".equals(conn.direction)) {
							// he/she requested this connection before - we might as well accept it ;)
							pm.changeConnectionRequestStatus(presence.getFrom().split("@")[0], presence.getTo().split("@")[0], "A");
						} else {
							// error - connection request already exists
						}
					} else {
						pm.connectTo(presence.getFrom().split("@")[0], presence.getTo().split("@")[0], "friend", "R");
					}
					db.commit();
				} catch (Exception e) {
					if (db!=null && sp!=null)
						db.rollback(sp);
					System.err.println(e.getMessage());
					throw new RuntimeException(e);
				}
			} else if (presence.getType().equals(Presence.Type.subscribed)) {
				// connection request accepted from an external client
				System.out.println(presence.getFrom() + " wants to be friend with: " + presence.getTo());
				DB db=null;
				Object sp=null;
				try {
					db = new DB();
					sp = db.createSavePoint();
					PersonMapper pm = db.getPersonMapper();
					Connection conn = pm.getConnection(presence.getFrom().split("@")[0], presence.getTo().split("@")[0]);
					if (conn!=null) {
						if ("inlink".equals(conn.direction)) {
							// he/she requested this connection before - we might as well accept it ;)
							pm.changeConnectionRequestStatus(presence.getFrom().split("@")[0], presence.getTo().split("@")[0], "A");
						} else {
							// error - connection request already exists
						}
					} else {
						pm.connectTo(presence.getFrom().split("@")[0], presence.getTo().split("@")[0], "friend", "A");
					}
					db.commit();
				} catch (Exception e) {
					if (db!=null && sp!=null)
						db.rollback(sp);
					System.err.println(e.getMessage());
					throw new RuntimeException(e);
				}
				
			}
			System.out.println(presence.toXML());
		}	
	}
}
