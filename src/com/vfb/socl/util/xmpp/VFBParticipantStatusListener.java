package com.vfb.socl.util.xmpp;

import org.jivesoftware.smackx.muc.ParticipantStatusListener;

public class VFBParticipantStatusListener implements ParticipantStatusListener {

	@Override
	public void voiceRevoked(String arg0) {
		System.out.println("revoke ");
		System.out.println(arg0);
	}
	
	@Override
	public void voiceGranted(String arg0) {
		System.out.println("grant ");
		System.out.println(arg0);
		
	}
	
	@Override
	public void ownershipRevoked(String arg0) {
		System.out.println("ownership revoked ");
		System.out.println(arg0);
	}
	
	@Override
	public void ownershipGranted(String arg0) {
		System.out.println("ownership granted ");
		System.out.println(arg0);
		
	}
	
	@Override
	public void nicknameChanged(String arg0, String arg1) {
		System.out.println("nickname chnaged ");
		System.out.println(arg0);
		
	}
	
	@Override
	public void moderatorRevoked(String arg0) {
		System.out.println("moderator revoked ");
		System.out.println(arg0);
		
	}
	
	@Override
	public void moderatorGranted(String arg0) {
		System.out.println("moderator granted ");
		System.out.println(arg0);
		
	}
	
	@Override
	public void membershipRevoked(String arg0) {
		System.out.println("membership revoked ");
		System.out.println(arg0);
		
	}
	
	@Override
	public void membershipGranted(String arg0) {
		System.out.println("membership granted ");
		System.out.println(arg0);
		
	}
	
	@Override
	public void left(String arg0) {
		System.out.println("left ");
		System.out.println(arg0);
		
	}
	
	@Override
	public void kicked(String arg0, String arg1, String arg2) {
		System.out.println("kicked ");
		System.out.println(arg0);
		
	}
	
	@Override
	public void joined(String arg0) {
		System.out.println("joined ");
		System.out.println("who: " + arg0);
		
	}
	
	@Override
	public void banned(String arg0, String arg1, String arg2) {
		System.out.println("banner ");
		System.out.println(arg0);
		System.out.println(arg1);
		System.out.println(arg2);
		
	}
	
	@Override
	public void adminRevoked(String arg0) {
		System.out.println("admin revoked");
		System.out.println(arg0);
		
	}
	
	@Override
	public void adminGranted(String arg0) {
		System.out.println("admin granted ");
		System.out.println(arg0);
		
	}
}