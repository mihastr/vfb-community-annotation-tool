package com.vfb.socl.util.xmpp;

import java.util.Collection;

import org.jivesoftware.smack.RosterListener;
import org.jivesoftware.smack.packet.Presence;

public class VFBRosterListener implements RosterListener {

	@Override
	public void entriesAdded(Collection<String> entries) {
		System.out.println("entries added");
		System.out.println(entries);
	}

	@Override
	public void entriesDeleted(Collection<String> entries) {
		System.out.println("entries deleted");
		System.out.println(entries);
	}

	@Override
	public void entriesUpdated(Collection<String> entries) {
		System.out.println("entries updated");
		System.out.println(entries);
	}

	@Override
	public void presenceChanged(Presence presence) {
		System.out.println("entry changed from: " + presence.getFrom() + " " + presence);
		//System.out.println(presence);
	}

}
