package com.vfb.socl.util.xmpp;

import org.jivesoftware.smack.Chat;
import org.jivesoftware.smack.ChatManagerListener;
import org.jivesoftware.smack.MessageListener;
import org.jivesoftware.smack.XMPPException;
import org.jivesoftware.smack.packet.Message;

public class VFBChatManagerListener implements ChatManagerListener {

	@Override
	public void chatCreated(Chat chat, boolean createdLocally) {
		try {
			if (!createdLocally) {
				System.out.println("chat created *****");
				System.out.println("threadId: " + chat.getThreadID());
				System.out.println("participant: " + chat.getParticipant().split("@")[0]);
				chat.addMessageListener(new VFBMessageListener());
				//chat.sendMessage("lol hey there, wellcome " + chat.getParticipant());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
