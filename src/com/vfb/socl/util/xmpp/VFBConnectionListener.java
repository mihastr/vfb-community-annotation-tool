package com.vfb.socl.util.xmpp;

import org.jivesoftware.smack.ConnectionListener;

public class VFBConnectionListener implements ConnectionListener {

	@Override
	public void connectionClosed() {
		System.out.println("connection closed");
	}

	@Override
	public void connectionClosedOnError(Exception arg0) {
		System.out.println("connection closed on error");
		
	}

	@Override
	public void reconnectingIn(int sec) {
		System.out.println("reconnecting in " + sec);
		
	}

	@Override
	public void reconnectionFailed(Exception arg0) {
		System.out.println("reconnect failed");
		
	}

	@Override
	public void reconnectionSuccessful() {
		System.out.println("reconnect successfull");
		
	}

}
