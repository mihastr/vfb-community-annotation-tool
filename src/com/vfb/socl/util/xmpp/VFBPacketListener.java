package com.vfb.socl.util.xmpp;

import org.jivesoftware.smack.PacketListener;
import org.jivesoftware.smack.packet.Message;
import org.jivesoftware.smack.packet.Packet;

public class VFBPacketListener implements PacketListener {

	@Override
	public void processPacket(Packet packet) {
		System.out.println("Group ************ packet ");
		System.out.print("from: " + packet.getFrom());
		System.out.println("   to: " + packet.getTo());
		if (packet instanceof Message) {
			Message msg = (Message)packet;
			System.out.println("Thread - " + msg.getThread() + ": " + msg.getBody());
		} else {
			System.out.println(packet.getClass().getName());
		}

	}

}
