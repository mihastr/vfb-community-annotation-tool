package com.vfb.socl.util.xmpp;

import org.jivesoftware.smack.Connection;
import org.jivesoftware.smack.packet.Message;
import org.jivesoftware.smackx.muc.InvitationListener;
import org.jivesoftware.smackx.muc.InvitationRejectionListener;

public class VFBInvitationListenet implements InvitationListener,
		InvitationRejectionListener {

	@Override
	public void invitationDeclined(String arg0, String arg1) {
		System.out.println("invitationDeclined");
		System.out.println(arg0);
		System.out.println(arg1);
	}

	@Override

	public void invitationReceived(Connection conn, String room, String inviter, String reason, String password, Message arg5) {
		System.err.println("invitation to a group chat recieved");
		System.out.println(room);
	}

}
