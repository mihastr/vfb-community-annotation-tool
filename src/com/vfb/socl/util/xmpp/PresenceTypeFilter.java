package com.vfb.socl.util.xmpp;

import org.jivesoftware.smack.filter.PacketFilter;
import org.jivesoftware.smack.packet.Packet;
import org.jivesoftware.smack.packet.Presence;

public class PresenceTypeFilter implements PacketFilter {

	Presence.Type type;
	
	public PresenceTypeFilter(Presence.Type type) {
		this.type = type;
	}
	 
	public boolean accept(Packet packet) {
		if (!(packet instanceof Presence)) {
			return false;
		} else {
			return ((Presence) packet).getType().equals(this.type);
		}
	}

}
