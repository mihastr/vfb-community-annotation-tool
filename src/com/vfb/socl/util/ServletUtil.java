package com.vfb.socl.util;

import java.lang.reflect.Array;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

public class ServletUtil {
	
	public void save(HttpServletRequest req, Object obj) throws Exception {
	    Set<String> names = new HashSet<String>();
	    @SuppressWarnings("unchecked")
	    Enumeration<String> enm = req.getParameterNames();
	    while (enm.hasMoreElements()) {
	        names.add(enm.nextElement());
	    }
	    Class clazz = obj.getClass();
	    while (clazz != Object.class && !names.isEmpty()) {
	        for (Field f: clazz.getDeclaredFields()) {
	            if (!Modifier.isTransient(f.getModifiers())) {
	                String name = f.getName();
	                if (names.contains(name)) {
	                    try {
	                        names.remove(name);
	                        f.setAccessible(true);
	                        Object val = convertValue(req, f.getType(),
	                                name);
	                        f.set(obj, val);
	                    } catch (ParseException ex) {
	                        //LOG.error("Error assigning field", ex);
	                    	throw ex;
	                    } catch (IllegalAccessException ex) {
	                        //LOG.error("Error assigning field", ex);
	                    	throw ex;
	                    }
	                }
	            }
	        }
	        clazz = clazz.getSuperclass();
	    }
	}
	
	private Object convertValue(HttpServletRequest req, Class<?> type,
	        String name) throws ParseException {
	    if (type.isArray()) {
	        Class<?> elemType = type.getComponentType();
	        String strings[] = req.getParameterValues(name);
	        if (strings == null || strings.length == 0) {
	            return new Object[0];
	        }
	        Object array = Array.newInstance(elemType, strings.length);
	        for (int i = 0; i < strings.length; ++i) {
	            Object val = parse(elemType, strings[i]);
	            Array.set(array, i, val);
	        }
	        return array;
	    } else {
	        String s = req.getParameter(name);
	        if (s == null) {
	            return null;
	        }
	        return parse(type, s);
	    }
	}

	public static Object parse(Class<?> type, String value)
	        throws ParseException {
	    if (type == String.class) {
	        return value;
	    } else if (value == null || value.length() == 0) {
	        return null;
	    } else if (Enum.class.isAssignableFrom(type)) {
	        @SuppressWarnings("unchecked")
	        Object result = Enum.valueOf((Class<? extends Enum>)type, value);
	        return result;
	    } else if (type == boolean.class || type == Boolean.class) {
	        return "true".equals(value);
	    } else if (type == byte.class || type == Byte.class) {
	        return Byte.valueOf(value);
	    } else if (type == short.class || type == Short.class) {
	        return Short.valueOf(value);
	    } else if (type == int.class || type == Integer.class) {
	        return Integer.valueOf(value);
	    } else if (type == long.class || type == Long.class) {
	        return Long.valueOf(value);
	    } else if (type == float.class || type == Float.class) {
	        return Float.valueOf(value);
	    } else if (type == double.class || type == Double.class) {
	        return Double.valueOf(value);
	    } else if (type == Date.class) {
	            return new SimpleDateFormat("dd/MM/yyyy").parse(value);
	    } else if (type == BigDecimal.class) {
	        DecimalFormat format = getDecimalFormat("0.00");
	        return format.parse(value);
	    } else {
	        throw new RuntimeException("Cannot convert value of type " + type);
	    }
	}

	private static DecimalFormat getDecimalFormat(String pattern) {
	    DecimalFormatSymbols symbols = new DecimalFormatSymbols();
	    symbols.setDecimalSeparator('.');
	    DecimalFormat format = new DecimalFormat(pattern);
	    format.setParseBigDecimal(true);
	    format.setDecimalFormatSymbols(symbols);
	    return format;
	}

}
