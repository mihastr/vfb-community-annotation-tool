package com.vfb.socl.util;

import java.util.Arrays;
import java.util.List;

import javax.servlet.ServletRequest;
import javax.servlet.ServletRequestEvent;
import javax.servlet.ServletRequestListener;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

import com.vfb.socl.entity.ActiveSession;
import com.vfb.socl.mappers.ActiveSessionsMapper;


public class VFBSessionListener implements HttpSessionListener {
	int activeSessions=0;
	
	DB db;
	ActiveSessionsMapper asm;
	
	private void debug(String s) {
		System.err.println(s);
	}
	
	public VFBSessionListener() {
		debug("constructor called");
		try {
			db = new DB();
			asm = db.getSessionMapper();
		} catch (Exception e) {
			debug(e.getMessage());
			throw new RuntimeException(e);
		}
	}

	public void sessionCreated(HttpSessionEvent se) {

		activeSessions++;
		/*
		ActiveSession as = new ActiveSession(se.getSession().getId(), null);
		try {
			System.err.println("asm.createSession(as) = " + asm.createSession(as));
			db.commit();
		} catch (Exception e) {
			db.rollback();
			debug(e.getMessage());
			throw new RuntimeException(e);
		}
		
		debug("session created " + se.getSession().getId());
		debug("active sessions: " + activeSessions);
		logActiveSessions();
		*/
	}

	public void sessionDestroyed(HttpSessionEvent se) {
		debug("session destroyed " + se.getSession().getId());
		if (activeSessions > 0)
			activeSessions--;
		try {
			asm.removeSession(se.getSession().getId());
			//db.commit();
		} catch (Exception e) {
			debug(e.getMessage());
			//db.rollback();
			throw new RuntimeException(e);
		}
		//debug("active sessions: " + activeSessions);
		//logActiveSessions();
		
	}

	
}
