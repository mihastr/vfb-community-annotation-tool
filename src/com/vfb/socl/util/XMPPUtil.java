package com.vfb.socl.util;

import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.net.ConnectException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;

import javax.servlet.http.HttpSession;

import org.apache.tomcat.jni.FileInfo;
import org.jivesoftware.smack.Chat;
import org.jivesoftware.smack.ChatManager;
import org.jivesoftware.smack.Connection;
import org.jivesoftware.smack.ConnectionConfiguration;
import org.jivesoftware.smack.PacketCollector;
import org.jivesoftware.smack.Roster;
import org.jivesoftware.smack.RosterEntry;
import org.jivesoftware.smack.XMPPConnection;
import org.jivesoftware.smack.XMPPException;
import org.jivesoftware.smack.filter.AndFilter;
import org.jivesoftware.smack.filter.PacketFilter;
import org.jivesoftware.smack.filter.PacketIDFilter;
import org.jivesoftware.smack.filter.PacketTypeFilter;
import org.jivesoftware.smack.packet.IQ;
import org.jivesoftware.smack.packet.Message;
import org.jivesoftware.smack.packet.Presence;
import org.jivesoftware.smack.packet.Registration;
import org.jivesoftware.smack.packet.XMPPError;
import org.jivesoftware.smackx.Form;
import org.jivesoftware.smackx.FormField;
import org.jivesoftware.smackx.muc.HostedRoom;
import org.jivesoftware.smackx.muc.InvitationListener;
import org.jivesoftware.smackx.muc.MultiUserChat;
import org.jivesoftware.smackx.muc.ParticipantStatusListener;

import com.vfb.socl.entity.Person;
import com.vfb.socl.exception.NoSuchChatException;
import com.vfb.socl.util.xmpp.PresenceTypeFilter;
import com.vfb.socl.util.xmpp.VFBChatManagerListener;
import com.vfb.socl.util.xmpp.VFBConnectionListener;
import com.vfb.socl.util.xmpp.VFBInvitationListenet;
import com.vfb.socl.util.xmpp.VFBMessageListener;
//import com.vfb.socl.util.xmpp.VFBPacketListener;
import com.vfb.socl.util.xmpp.VFBParticipantStatusListener;
import com.vfb.socl.util.xmpp.VFBRosterListener;

public class XMPPUtil {

	
	public String SERVER_URL = null;
	
	public static final String PROPERTIES_LOCATION = "/properties/vfb.properties";
	
	Connection conn=null;
	Roster roster;
	ChatManager chatmanager;
	
	boolean isEnabled = true;
	
	private HashMap<String, Chat> chatList;
	private HashMap<Long, MultiUserChat> groupChatList;
	
	public XMPPUtil() throws Exception {
		Properties p = new Properties();
		
		p.load(new FileInputStream(PROPERTIES_LOCATION));

		SERVER_URL = (String)p.getProperty("xmpp.server", "localhost");
		System.out.println(SERVER_URL);
		
		System.err.println("create XMPP utils");
		ConnectionConfiguration connConf = new ConnectionConfiguration(SERVER_URL);
		conn = new XMPPConnection(connConf);
		VFBConnectionListener connectionListener = new VFBConnectionListener();
		System.out.println("conn created");
		try {
			conn.connect();
			isEnabled = true;
		} catch (Exception e) {
			if (e instanceof ConnectException) {
				isEnabled = false;
				System.err.println("XMPP stopped: " + e.getMessage());
			}
		}
		conn.addConnectionListener(connectionListener);

	}
	
	public XMPPUtil(String username, String password) throws Exception {
		
		Properties p = new Properties();
		p.load(new FileInputStream(PROPERTIES_LOCATION));

		SERVER_URL = (String)p.getProperty("xmpp.server", "localhost");
		System.out.println(SERVER_URL);
		
		System.err.println("create XMPP utils");
		ConnectionConfiguration connConf = new ConnectionConfiguration(SERVER_URL);
		conn = new XMPPConnection(connConf);
		VFBConnectionListener connectionListener = new VFBConnectionListener();
		System.out.println("conn created");


		roster = conn.getRoster();
		roster.setSubscriptionMode(Roster.SubscriptionMode.manual);		
		roster.addRosterListener(new VFBRosterListener());
		roster.setSubscriptionMode(Roster.SubscriptionMode.manual);
		
		VFBMessageListener messageListener = new VFBMessageListener();
		conn.addPacketListener(messageListener, new PresenceTypeFilter(Presence.Type.subscribe));
		conn.addPacketListener(messageListener, new PresenceTypeFilter(Presence.Type.subscribed));
		
		// synchronize entries with user
		System.out.println("get entries: " + roster.getEntries());
		for (RosterEntry re: roster.getEntries()) {
			System.out.println(re.getUser() + " is " + re.getStatus());
		}
		
		
		chatmanager = conn.getChatManager();
		chatmanager.addChatListener(new VFBChatManagerListener());
		
		chatList = new HashMap<String, Chat>();
		groupChatList = new HashMap<Long, MultiUserChat>();

		try {
			conn.connect();
			isEnabled = true;
		} catch (Exception e) {
			if (e instanceof XMPPException) {
				isEnabled = false;
				System.err.println("XMPP stopped: " + e.getMessage());
			} else {
				throw e;
			}
		}
		if (isEnabled) {
		
			conn.addConnectionListener(connectionListener);
			
			conn.login(username, password, "VFB");
	
			System.out.println(username + " logged into XMPP server");
			
			MultiUserChat.addInvitationListener(conn, new VFBInvitationListenet());
		}
		
	}
	
	/**
	 * Send message to chat
	 * @throws XMPPException
	 * @return chat thread id 
	 * */
	public String sendMessage(String to, String body) throws XMPPException {
		
		if (!isEnabled) return null;
		
		Chat chat = null;
		if (chatList.containsKey(to)) {
			chat = chatList.get(to);
		}
		if (chat==null) {
			chat = chatmanager.createChat(to, new VFBMessageListener());
			chatList.put(to, chat);
		}
		Message m = new Message(to);
		m.setBody(body);
		m.setProperty("isStoredInDB", true);
		chat.sendMessage(m);
		return chat.getThreadID();
	}
	
	public void sendGroupMessage(long conversationId, String body) throws NoSuchChatException, XMPPException  {
		
		if (!isEnabled) return;
		
		if (groupChatList.containsKey(conversationId)) {
			MultiUserChat muc = groupChatList.get(conversationId);
			if (muc==null) throw new NoSuchChatException();
			Message m = new Message();
			m.setBody(body);
			m.setProperty("isStoredInDB", true);
			muc.sendMessage(m);			
		} else {
			throw new NoSuchChatException();
		}
	}
	public MultiUserChat createGroupChat(long conversationId, Person creator, List<Person> participants) throws Exception {
		
		if (!isEnabled) return null;
		
		if (!isEnabled) return null;
		// multiuser chat exists
		if (participants==null) throw new Exception("participants shall not be null");

		String jid = String.format("room%d@conference.%s", conversationId, SERVER_URL);

		if (groupChatList.containsKey(conversationId) && groupChatList.get(conversationId)!=null) 
			return groupChatList.get(conversationId);
				
		MultiUserChat muc = null;
		
		Collection<HostedRoom> rooms = MultiUserChat.getHostedRooms(conn, "conference." + SERVER_URL);
		boolean roomExists = false;
		for (HostedRoom r: rooms) {
			if (r.getJid().equals(jid)) {
				roomExists = true;
				break;
			}
		}
		
		if (roomExists) {
			muc = new MultiUserChat(conn, jid);
			//muc.addMessageListener(new VFBPacketListener());				
			muc.addMessageListener(new VFBMessageListener());
			muc.addParticipantStatusListener(new VFBParticipantStatusListener());
			
			if (muc.isJoined()) {
				System.out.println(muc.getParticipants());
			} else {
				muc.join(creator.toString());
			}
			for (Person u: participants) {
				if (!u.equals(creator))
					muc.invite(String.format("%s@%s", u.username, SERVER_URL), "join us");
			}
		} else {
			muc = new MultiUserChat(conn, jid);
			
			System.out.println("service: " + jid);

			//muc.addMessageListener(new VFBPacketListener());
			muc.addMessageListener(new VFBMessageListener());
			
			muc.addParticipantStatusListener(new VFBParticipantStatusListener());
			
			muc.create(creator.toString());

			// Get the the room's configuration form
            Form form = muc.getConfigurationForm();
			//Form form = new Form(Form.TYPE_SUBMIT);
			
			
            // Create a new form to submit based on the original form
            Form submitForm = form.createAnswerForm();
            // Add default answers to the form to submit
            for (Iterator<FormField> fields = form.getFields(); fields.hasNext();) {
                FormField field = fields.next();
                if (!FormField.TYPE_HIDDEN.equals(field.getType())
                    && field.getVariable() != null) {
                    // Sets the default value as the answer
                    submitForm.setDefaultAnswer(field.getVariable());
                }
            }
            submitForm.setAnswer("muc#roomconfig_publicroom", true);
            submitForm.setAnswer("muc#roomconfig_roomname", "room" + conversationId);
            List<String> owners = new ArrayList<String>();
            owners.add(String.format("%s@%s", creator.username, SERVER_URL));
            submitForm.setAnswer("muc#roomconfig_roomowners", owners);

            // Update the new room's configuration
            muc.sendConfigurationForm(submitForm);

			//muc.join(creator.toString());


			for (Person u: participants) {
				if (!u.equals(creator))
					muc.invite(String.format("%s@%s", u.username, SERVER_URL), "join us");
			}
		}
		groupChatList.put(conversationId, muc);
		return muc;
	}
	
	public String createChat(String userJID) {
		
		if (!isEnabled) return null;
		
		Chat chat = chatmanager.createChat(userJID, new VFBMessageListener());
		return chat.getThreadID();
	}
	
	/**
	 * gets user chat manager
	 * */
	public ChatManager getChatManager() {
		if (!isEnabled) return null;
		return chatmanager;
	}
	

	/**
	 * Creates user on XMPP server
	 * */
	public void createUser(String username, String password)
			throws XMPPException {
		
		if (!isEnabled) return;
		
		System.out.println("Registering account " + username + " at server");
		Registration register = new Registration();
		ArrayList<String> fieldListFields = new ArrayList<String>();
		PacketCollector iqCollector;

		iqCollector = conn.createPacketCollector(new PacketTypeFilter(IQ.class));

		ArrayList<String> fieldListNames = new ArrayList<String>();

		register.setType(IQ.Type.SET);
		register.setTo(SERVER_URL);

		Hashtable<String, String> map = new Hashtable<String, String>();

		// set up the various attributes to be sent to the server
		map.put("username", username);
		map.put("password", password);
		map.put("email", username + "@" + SERVER_URL);
		map.put("name", username);

		System.out.println(map);
		
		// send the packet
		register.setAttributes(map);
		PacketFilter filter = new AndFilter(new PacketIDFilter(
				register.getPacketID()), new PacketTypeFilter(IQ.class));

		PacketCollector collector = conn.createPacketCollector(filter);
		conn.sendPacket(register);

		// Packet response = iqCollector.nextResult();
		IQ response = (IQ) collector.nextResult(1000);

		if (response instanceof Registration) {
			if (response.getType() == IQ.Type.ERROR) {
				if (response.getError().toString().contains("409")) {
					throw new RuntimeException("Cannot register, user information already exists (error confict 409)");
				} else {
					throw new RuntimeException("Registration error! unknown error "
							+ response.getError().getMessage());
				}
			} else if (response.getType() == IQ.Type.RESULT) {
				System.out.println("Registration result RESULT");
			} else if (response.getType() == IQ.Type.SET) {
				System.out.println("Registration result SET");
			}
		}
	}
	
	/**
	 * Friend connecions
	 * **/
	public void connectTo(String username, String displayName, String[] groups) throws XMPPException {
		
		if (!isEnabled) return;
		
		roster.createEntry(username, displayName, groups);
		//Presence subscribe = new Presence(Presence.Type.subscribe);
		//subscribe.setTo(username);
		//this.conn.sendPacket(subscribe);
	}

	public void acceptRequest(String jid) {
		
		if (!isEnabled) return;
		
		Presence subscribed = new Presence(Presence.Type.subscribed);
		subscribed.setTo(jid);
		this.conn.sendPacket(subscribed);
	}
	
	/**
	 * Change password on XMPP server
	 * */
	public void setPassword(String username, String oldPassword, String newPassword) throws XMPPException {
		
		if (!isEnabled) return;
		
		conn.getAccountManager().changePassword(newPassword);	
	}
	
	public static void main(String[] args) throws Exception {
		XMPPUtil u = new XMPPUtil("miha", "miha");
		Thread.sleep(1000);
	
		//System.out.println(u.createGroupChat(16, null));
	}

	
	/**
	 * Logout of the XMPP server
	 * */
	public void close() {
		
		if (!isEnabled) return;
		
		this.conn.disconnect();		
	}

	/**
	 * get user presence on XMPP server
	 * */
	public boolean isOnline(String user) {
		if (!isEnabled) return false;
		Presence p = roster.getPresence(user);
		return p.isAvailable() || p.isAway();
	}

	public void invitePerson(long conversationId, Person creator, Person participant, List<Person> participants) throws Exception {
		if (!isEnabled) return;
		MultiUserChat muc;
		if (groupChatList.containsKey(conversationId)) {
			muc = groupChatList.get(conversationId);
			if (muc==null) {
				muc = createGroupChat(conversationId, creator, participants);
				muc.join(creator.toString());
			}
		} else {
			muc = createGroupChat(conversationId, creator, participants);
			muc.join(creator.toString());
		}
		muc.invite(String.format("%s@%s", participant.username, SERVER_URL), "join us");
	}

	public Collection<RosterEntry> getRosterList() {
		if (!isEnabled) return null;
		return this.conn.getRoster().getEntries();
	}

	
}
