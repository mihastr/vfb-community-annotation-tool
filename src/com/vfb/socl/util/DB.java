package com.vfb.socl.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.DefaultTransactionDefinition;

import com.vfb.socl.entity.Person;
import com.vfb.socl.mappers.ActiveSessionsMapper;
import com.vfb.socl.mappers.AnnotationMapper;
import com.vfb.socl.mappers.MessageMapper;
import com.vfb.socl.mappers.PersonMapper;

public class DB {
	final static Logger logger = LoggerFactory.getLogger(DB.class);

	private ApplicationContext ac;

	DataSourceTransactionManager man;
	TransactionStatus trStat;

	public DB() throws Exception {
		ac = new ClassPathXmlApplicationContext("spring.xml");
	}

	public PersonMapper getPersonMapper() {
		return (PersonMapper) ac.getBean("userMapper");
	}

	public ActiveSessionsMapper getSessionMapper() {
		return (ActiveSessionsMapper) ac.getBean("activeSessionsMapper");
	}
	
	public MessageMapper getMessageMapper() {
		return (MessageMapper) ac.getBean("messageMapper");
	}
	
	public AnnotationMapper getAnnotationMapper() {
		return (AnnotationMapper) ac.getBean("annotationMapper");
		
	}
	
	public Object createSavePoint() {
		DefaultTransactionDefinition def = new DefaultTransactionDefinition();
		def.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRES_NEW);
		man = (DataSourceTransactionManager) ac.getBean("transactionManager");
		trStat = man.getTransaction(def);
		return trStat.createSavepoint();

	}

	public void commit() {
		if (trStat==null || man==null) return;
		man.commit(trStat);
	}
	
	public void rollback(Object sp) {
		if (trStat==null || man==null) return;
		if (!trStat.isNewTransaction() && !trStat.isCompleted())
			trStat.rollbackToSavepoint(sp);
	}	
	
	


	public static void main(String[] args) throws Exception {
		
		DB db = new DB();
		Object sp = db.createSavePoint();
		PersonMapper pm = db.getPersonMapper();
		Person p = pm.getPerson("miha", "nestor");
		System.out.println(p);
		
		MessageMapper mm = db.getMessageMapper();
		long conId = mm.createConversation("java test", "miha", "private", "test");
		mm.addParticipant(conId, "miha", "miha");
		mm.addParticipant(conId, "nestor", "miha");
		long msgId = mm.createMessage(conId, "miha", "test java message");
		db.commit();
		db.rollback(sp);
		
		/*
		AnnotationMapper am = db.getAnnotationMapper();
		Annotation a = new Annotation();
		a.setUsername("miha");
		a.setComments("test annotation");
		a.setGeneId("CaMKII");
		a.setStructureId("FBbt:00040055");
		a.setStrength("moderate");
		a.setPattern("localized");
		am.storeAnnotation(a);
		db.commit();
		*/
	}
}
