package com.vfb.socl.util;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

public class JSOParser {

	/**
	 * @param args
	 * @throws Exception 
	 * @throws JsonSyntaxException 
	 */
	static List<Map<String, Object>> dataList=null;
	
	public static void main(String[] args) throws JsonSyntaxException, Exception {
		String structure = "C:\\Users\\Miha\\workspace\\VFBAnnotation\\WebContent\\js\\treeStructure.jso";
		String data = "C:\\Users\\Miha\\workspace\\VFBAnnotation\\WebContent\\js\\treeContent.jso";
		String dataOut = "C:\\Users\\Miha\\workspace\\VFBAnnotation\\WebContent\\js\\treeContentParsed.jso";
		String paths = "C:\\Users\\Miha\\workspace\\VFBAnnotation\\WebContent\\js\\treeContentPaths.jso";
		
		Gson g = new Gson();
		dataList = new LinkedList<Map<String,Object>>();
		//List<Map<String, Object>> structureList = new LinkedList<Map<String,Object>>();
		Map<String, Object> structureList = new HashMap<String, Object>();

		dataList = g.fromJson(readFile(data), dataList.getClass());
		structureList = g.fromJson(readFile(structure), structureList.getClass());
		
		traverseTree((Map<String, Object>) structureList.get("node"));
		String r = g.toJson(dataList);
		writeToFile(r, dataOut);
		
		PrintWriter pw = new PrintWriter(new File(paths));
		pw.append("[");
		for (Map<String, Object> o: dataList) {
			pw.println("{'nodeId':" + o.get("nodeId") + ",'path':" + o.get("path") + "}");
		}
		pw.append("]");
		pw.close();
	}
	
	private static void writeToFile(String json, String path) throws FileNotFoundException {
		PrintWriter pw = new PrintWriter(new File(path));
		pw.write(json);
		pw.close();
	}

	static LinkedList<String> currentPath = new LinkedList<String>();
	private static void traverseTree(Map<String, Object> node) {
		//System.out.print("\nparent: " + node.get("nodeId"));
		currentPath.addLast((String)node.get("nodeId"));
		Map<String, Object> cdl = dataList.get(Integer.parseInt((String) node.get("nodeId")));
		cdl.put("path", new LinkedList<String>(currentPath));
		if (node.containsKey("children")) {
			for (Map<String, Object> child: (List<Map<String, Object>>)node.get("children")) {
				Map<String, Object> o = (Map<String, Object>) child.get("node");
				//System.out.println(currentPath);
				traverseTree(o);
			}
		}
		currentPath.removeLast();
	}
	
	private static String readFile(String fileName) throws Exception {
		BufferedReader reader = new BufferedReader(new FileReader(new File(fileName)));
		StringBuffer sb = new StringBuffer();
		char[] cbuff = new char[2048];
		int n=0;
		while ((n=reader.read(cbuff))>0) {
			sb.append(cbuff, 0, n);
		}
		return sb.toString();
	}

}
