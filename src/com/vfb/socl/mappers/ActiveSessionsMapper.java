package com.vfb.socl.mappers;

import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import com.vfb.socl.entity.ActiveSession;

public interface ActiveSessionsMapper {
	@Delete("delete from active_sessions")
	public void clearSessions();
	
	@Insert("insert into active_sessions (session_id, username, user_ip, session_started, session_ended) values (#{sessionId}, #{username}, 'lol', CURRENT_TIMESTAMP, NULL)")
	public int createSession(ActiveSession s);
	
	@Update("update active_sessions set username=#{username}, user_ip=#{userIp} where session_id=#{sessionId}")
	public int setUsernameToSession(@Param("username")String username, @Param("userIp")String userIp, @Param("sessionId")String sessionId);
	
	@Update("update active_sessions set session_ended=CURRENT_TIMESTAMP where session_id=#{value}")
	public int removeSession(String sessionId);
	
	@Select("select session_id sessionId, username, session_started sessionStarted from active_sessions where session_ended is null")
	public List<ActiveSession> getActiveSessions();
	
	@Update("update active_sessions set session_ended=current_timestamp where username=#{username}")
	public int closeOldSessions(String username);
	
	@Update("update active_sessions set session_ended=current_timestamp where session_id=#{SSID}")
	public int closeOldSessionsBySSID(String SSID);
	
	
	
	@Select("select case when count(username)=1 then true else false end from active_sessions where username=#{username} and session_ended is null")
	public boolean checkSession(String username);
}
