package com.vfb.socl.mappers;

import java.util.Date;
import java.util.List;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import com.vfb.socl.entity.Connection;
import com.vfb.socl.entity.Person;

public interface PersonMapper {
	
	static final String seekPersonSQL = "select us.username, ud.firstname firstName, ud.surname lastName, rank from (" +
			" select us.username, ts_rank_cd(textsearch, query) rank" +
			" from user_textsearch us, to_tsquery('english', #{query}) query" +
			" where query @@ textsearch or username like '${query}%'" +
			" ) us left outer join user_detail ud on ud.username=us.username" +
			" order by rank desc";
	
	static final String getPersonPwdSQL = "select u.username, ud.firstname as firstName, ud.surname as lastName," +
			" institution as institute, position, email, phone, last_update lastUpdate" +
			" from users u left outer join user_detail ud on u.username=ud.username" +
			" where u.username=#{username} and password=md5(#{password}) and enabled = TRUE";

	static final String getPersonSQL = "select u.username, ud.firstname as firstName, ud.surname as lastName," +
			"   institution as institute, position, email, phone, last_update lastUpdate," +
			"   case when c.status = 'A' then true else false end isAFriend," +
			"   case when session_started is not null then true else false end isOnline" +
			" from users u left outer join user_detail ud on u.username=ud.username" +
			" left outer join connection c on ((u.username = person1 and person2=#{viewer}) or" +
			"				  (u.username=person2 and person1=#{viewer}))" +
			" left outer join active_sessions ass on (ass.username=u.username and ass.session_ended is null)" +
			" where u.username=#{username} and enabled = TRUE limit 1";
	
	
	static final String getPersonConnectionsSQL = "select c.person2 username, relationship, 'outlink' direction, status, " +
			"  connection_request_time connectionRequest, ud.firstname firstName, ud.surname lastName," +
			"     case when ass.session_started is not null then true else false end isOnline" +
			"   from users u join connection c on u.username=c.person2" +
			"	left outer join user_detail ud on ud.username=c.person2" +
			"	left outer join active_sessions ass on (ass.username=u.username and ass.session_ended is null)" +
			"  where c.person1 = #{value}" +
			" union " +
			" select c.person1 username, relationship, 'inlink' direction, status," +
			"     connection_request_time connectionRequest, ud.firstname firstName, ud.surname lastName," +
			"     case when ass.session_started is not null then true else false end isOnline" +
			"  from users u join connection c on u.username=c.person1" +
			"	left outer join user_detail ud on ud.username=c.person1" +
			"	left outer join active_sessions ass on (ass.username=u.username and ass.session_ended is null)" +
			" where c.person2 = #{value}" +
			" order by username";
	
	static final String getLastUpdateSQL = "select last_update from users where username = #{value}";
	
	@Select(getPersonSQL)
	public Person getPerson(@Param("username")String username, @Param("viewer")String viewer);
	
	@Select(getPersonPwdSQL)
	public Person getPersonPwd(@Param("username")String username, @Param("password")String password);
	
	@Select(getLastUpdateSQL)
	public Date getLastUpdate(String username);
	
	@Select(getPersonConnectionsSQL)
	public List<Connection> getPersonConnections(String username);
	
	@Select(seekPersonSQL)
	public List<Person> seekPerson(@Param("query")String query);

	@Insert("insert into connection (person1, person2, connection_request_time, relationship, status) values " +
			"(#{username}, #{friend}, current_timestamp, #{relationship}, #{status})")
	public int connectTo(@Param("username")String username, @Param("friend")String friend, @Param("relationship")String relationship, @Param("status")String status);
	
	@Update("update users set last_update=current_timestamp where username=#{username}")
	public int setProfileUpdated(String username);

	@Insert("insert into users (username, password, enabled, joined, last_update) values " +
			" (#{username}, md5(#{password}), true, current_timestamp, current_timestamp)")
	public int createPerson(Person newUser);

	@Insert("insert into user_detail (username, firstname, surname, institution,  position, email, phone, notes, email4newsletter) values " +
			" (#{username}, #{firstName}, #{lastName}, #{institute}, #{position}, #{email}, #{phone}, #{notes}, #{email4newsletter}) ")
	public int createPersonDetail(Person newUser);

	@Update("update users set password=md5(#{newPassword}), last_update=current_timestamp where username=#{username} and password=md5(#{oldPassword})")
	public int changePassword(@Param("username")String username, @Param("oldPassword")String oldPassword, @Param("newPassword")String newPassword);

	@Select("select status, relationship, case when person1= #{user} then 'outlink' else 'inlink' end direction," +
			" connection_request_time connectionRequest from connection" +
			" where (person1= #{user} and person2= #{peer} ) or (person2= #{user} and person1= #{peer} )")
	public Connection getConnection(@Param("user")String user, @Param("peer")String peer);

	@Update("update connection set status='A' where person1 = #{peer} and person2 = #{user} ")
	public int changeConnectionRequestStatus(@Param("user")String user, @Param("peer")String peer, String c);
}
