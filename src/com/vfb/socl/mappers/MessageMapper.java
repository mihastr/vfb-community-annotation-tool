package com.vfb.socl.mappers;

import java.util.Collection;
import java.util.List;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import com.vfb.socl.entity.Message;
import com.vfb.socl.entity.MessageThread;
import com.vfb.socl.entity.Person;

public interface MessageMapper {
	@Select("select t1.conversation_id conversationId, title, ud.username, ud.firstname firstName, ud.surname lastName, session_start sessionStart, session_end sessionEnd," +
			" visibility, case when last_read_message_id < last_msg_id then true else false end hasNewMessages, message_sent lastMessageTime,  sender, concat(coalesce(udm.firstname,''), ' ', coalesce(udm.surname, '')) senderName, body," +
			" case when as1.session_started is null then false else true end isOnline," +
			" case when as2.session_started is null then false else true end isLMSOnline," +
			" board_location boardLocation, entity_id entityId " +
			" FROM ( SELECT c.conversation_id, c.title, c.initiator, c.session_start, c.session_end, c.visibility, cp.last_read_message_id" +
			"    FROM conversation c join conversation_participant cp on cp.conversation_id=c.conversation_id and cp.username=#{username} and (cp.time_to<current_timestamp or cp.time_to is null))" +
			" t1 join (select max(message_id) last_msg_id, conversation_id from message group by conversation_id) t2 on t1.conversation_id= t2.conversation_id" +
			" join message m2 on m2.message_id = last_msg_id " +
			" left outer join user_detail udm on udm.username=sender" +
			" left outer join user_detail ud on ud.username=t1.initiator" +
			" left outer join active_sessions as1 on as1.username=ud.username and as1.session_ended is null" +
			" left outer join active_sessions as2 on as2.username=udm.username and as2.session_ended is null" +
			" left outer join discussion_board db on db.conversation_id = t1.conversation_id" +
			//" where t1.conversation_id > #{conversationId}" +
			" order by message_sent desc")
	public Collection<MessageThread> getThreads(@Param("username")String username);
	
	
	@Select("SELECT m.message_id messageId, m.sender username, m.conversation_id conversationId, m.body, m.message_sent messageSent, ud.firstname firstName, ud.surname lastName" +
			" FROM message m JOIN user_detail ud ON m.sender = ud.username" +
			" WHERE conversation_id = #{conversationId} and message_id > #{lastMsgId} " +
			" and exists (select c.conversation_id from conversation c left outer join conversation_participant cp on (c.conversation_id=cp.conversation_id) " +
			"                 where c.visibility='public' or (cp.conversation_id = #{conversationId} and username= #{username}))" +
			" ORDER BY m.message_id desc")
	public Collection<Message> getMessages(@Param("conversationId")long conversationId, @Param("username")String username, @Param("lastMsgId")long lastMsgId);
	
	@Update("update conversation_participant set last_read_message_id = #{lastMsgId} where conversation_id = #{conversationId} and username =  #{username}")
	public int updateUserLastReadMessage(@Param("conversationId")long conversationId, @Param("username")String username, @Param("lastMsgId")long lastMsgId);
	
	//	@Options(useGeneratedKeys=true, keyProperty="idName")
	
	@Insert("insert into discussion_board (board_location, entity_id, conversation_id) values (#{owner}, #{entitiyId}, #{conversationId})")
	public int createDiscussionBoard(@Param("owner")String owner, @Param("entitiyId")String entityId, @Param("conversationId")long conversationId);
	
	@Select("insert into conversation (conversation_id, title, initiator, session_start, visibility, xmmp_thread) values (nextval('conversation_conversation_id_seq'), #{title}, #{username}, current_timestamp, #{visibility}, #{threadId}) returning conversation_id")
	public long createConversation(@Param("title")String title, @Param("username")String username, @Param("visibility")String visibility, @Param("threadId")String threadId);
	
	@Select("insert into message (sender, conversation_id, body, message_sent) values (#{username}, #{conversationId}, #{body}, current_timestamp) returning message_id")
	public long createMessage(@Param("conversationId")long conversationId, @Param("username")String username, @Param("body")String body);
	
	@Insert("insert into conversation_participant (  conversation_id, username, time_from, last_read_message_id, added_by)" +
			" values (#{conversationId}, #{recipient}, current_timestamp, 0, #{username})")
	public int addParticipant(@Param("conversationId")long conversationId, @Param("recipient")String recipient, @Param("username")String username);
	
	@Select("select case when count(*)=1 then true else false end from conversation_participant where username=#{username} and conversation_id=#{conversationId}")
	public boolean checkParticipant(@Param("username")String username, @Param("conversationId")long conversationId);
	
	@Select("select case when count(*)=1 then true else false end from conversation where conversation_id=#{conversationId} and visibility='public'")
	public boolean isThreadPublic(@Param("conversationId")long conversationId);

	@Select("select u.username, firstname firstName, surname lastName, case when ass.session_started is not null then true else false end isOnline, " +
			" is_subscribed isSubscribed from (" +
			" select cp.username, firstname, surname, is_subscribed from conversation_participant cp left outer join user_detail ud on ud.username=cp.username" +
			" where conversation_id=#{conversationId}" +
			" and conversation_id in (select conversation_id from conversation_participant where conversation_id=#{conversationId} and username=#{username})" +
			") u left outer join active_sessions ass on u.username=ass.username and ass.session_ended is null")
	public Collection<Person> getParticipants(@Param("username")String username, @Param("conversationId")long conversationId);

	@Select("select conversation_id from conversation where xmmp_thread=#{threadId} order by conversation_id desc limit 1")
	public Long getConversationByXMMPThreadId(String threadID);

	@Update("update conversation set xmmp_thread=#{threadId} where conversation_id = #{conversationId}")
	public int setThreadIdToConversation(@Param("conversationId")long conversationId, @Param("threadId")String threadId);


	@Select("select conversation_id, count(*) from conversation_participant where conversation_id in (" +
			" select cp1.conversation_id" +
			" from conversation_participant cp1 " +
			"  join conversation_participant cp2 on cp1.conversation_id=cp2.conversation_id" +
			" where cp1.username=#{user1} and cp2.username=#{user2})" +
			" group by conversation_id " +
			" having count(*)=2 limit 1")
	public Long getConversationIDParticipants(@Param("user1")String user1, @Param("user2")String user2);

	@Select("select case when count(*)=1 then true else false end isDiscussionBoard from discussion_board" +
			" where conversation_id = #{conversationId} ")
	public boolean isDiscussionBoard(long conversationId);

	@Select("select m.sender username, m.message_id messageId, m.conversation_id conversationId, m.body, m.message_sent messageSent from message m " +
			" join conversation_participant cp on m.conversation_id=cp.conversation_id" +
			" where m.message_id>cp.last_read_message_id and cp.username=#{username}" +
			" order by message_sent desc")
	public List<Message> getNewMessages(String username);

	@Select("select t1.conversation_id conversationId, title, ud.username, ud.firstname firstName, ud.surname lastName, session_start sessionStart, session_end sessionEnd," +
			" visibility, case when last_read_message_id < last_msg_id then true else false end hasNewMessages, message_sent lastMessageTime,  sender, concat(coalesce(udm.firstname,''), ' ', coalesce(udm.surname, '')) senderName, body," +
			" case when as1.session_started is null then false else true end isOnline," +
			" case when as2.session_started is null then false else true end isLMSOnline," +
			" board_location boardLocation, entity_id entityId" +
			" FROM ( SELECT c.conversation_id, c.title, c.initiator, c.session_start, c.session_end, c.visibility, cp.last_read_message_id" +
			"    FROM conversation c join conversation_participant cp on cp.conversation_id=c.conversation_id and cp.username=#{username} and (cp.time_to<current_timestamp or cp.time_to is null))" +
			" t1 join (select max(message_id) last_msg_id, conversation_id from message group by conversation_id) t2 on t1.conversation_id= t2.conversation_id" +
			" join message m2 on m2.message_id = last_msg_id" +
			" left outer join user_detail udm on udm.username=sender" +
			" left outer join user_detail ud on ud.username=t1.initiator" +
			" left outer join active_sessions as1 on as1.username=ud.username and as1.session_ended is null" +
			" left outer join active_sessions as2 on as2.username=udm.username and as2.session_ended is null" +
			" left join discussion_board db on db.conversation_id = t1.conversation_id" +
			" where entity_id is not null" +
			" order by message_sent desc")
	public List<MessageThread> getDiscussionBoardsForUser(String username);
}
