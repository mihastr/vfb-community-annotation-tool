package com.vfb.socl.mappers;

import java.util.List;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import com.vfb.socl.entity.Annotation;

public interface AnnotationMapper {
	
	@Select("select conversation_id from discussion_board where board_location='annotation' and entity_id = concat(#{geneId},'#',#{pathToNode})") 
	public Long getAnnotationDiscussionBoard(@Param("geneId")String geneId, @Param("pathToNode")String pathToNode);
	
	@Select("select conversation_id from discussion_board" +
			" where board_location='annotation' and" +
			" entity_id = (select concat(gene_id, '#', path_to_node)" +
			"  from annotation where annotation_id = #{annotationId} )") 
	public Long getAnnotationDiscussionBoardByAnnotationID(@Param("annotationId")Long annotationId);


	
	@Insert("insert into discussion_board (board_location, entity_id, conversation_id) values ('annotation', concat(#{geneId},'#',#{pathToNode}), #{conversationId})")
	public int setAnnotationDiscussionBoard(@Param("geneId")String geneId, @Param("pathToNode")String pathToNode, @Param("conversationId")long conversationId);

	/*
	@Select("select tab.annotation_id annotationId, tab.username, gene_id geneId, structure_id structureId, submitted dateSubmited, tab.position, strength, pattern, comments, stack_id stackId, " + 
	" firstname firstName, surname lastName, is_flagged isFlagged, path_to_node pathToNode, coalesce(tab.sum_votes, 0) sumOfVotes, coalesce(tab.vote,0) vote, n_up nUp, n_down nDown from (( " +
	" select a.annotation_id, sum(av.vote) sum_votes, SUM(CASE WHEN av.vote > 0 THEN 1 ELSE 0 END) n_up, SUM(CASE WHEN av.vote < 0 THEN 1 ELSE 0 END) n_down, a.username, gene_id, structure_id, submitted, a.position, strength, pattern, comments, stack_id,  " +
	"  is_flagged, path_to_node " +
	" from annotation a left outer join annotation_vote av on a.annotation_id=av.annotation_id " +
	" group by a.annotation_id " +
	" ) agr " +
	" left outer join ( " +
	" select av.vote, a.annotation_id vaid " +
	" from annotation a left outer join annotation_vote av on a.annotation_id=av.annotation_id " +
	" where av.username= #{username} " +
	" ) ind " +
	" on agr.annotation_id = ind.vaid) tab " +
	" left outer join user_detail ud on tab.username=ud.username " +
	" where (path_to_node like '${pathToNode}/%' or path_to_node = #{pathToNode}) and gene_id=#{geneId}" +
	" order by path_to_node,  sumOfVotes desc")	
	*/
	@Select("select a.annotation_id annotationId, /*a.username,*/ gene_id geneId, structure_id structureId," +
			"  submitted dateSubmited, a.position, strength, pattern, comments, stack_id stackId, coalesce(av.is_submission, false) isSubmission, " +
			"  is_flagged isFlagged, path_to_node pathToNode, nUp, nDown, (nUp-nDown) sumOfVotes, coalesce(av.vote, 0) vote, nSubmissions " +
			"  from annotation a left outer join" +
			"    annotation_vote av on (a.annotation_id=av.annotation_id and av.username= #{username} and av.vote<>0 )" +
			" join " +
			" (select a.annotation_id, sum(case when av.vote>0 then 1 else 0 end) nUp," +
			"  sum(case when av.vote<0 then 1 else 0 end) nDown," +
			"  sum(case when av.is_submission then 1 else 0 end) nSubmissions" +
			" from annotation a" +
			"  join annotation_vote av on a.annotation_id = av.annotation_id " +
			" where (path_to_node like '${pathToNode}/%' or path_to_node = #{pathToNode}) and gene_id=#{geneId} " +
			" group by a.annotation_id) aggr" +
			" on a.annotation_id = aggr.annotation_id" +
			" order by structure_id, sumOfVotes desc")
	public List<Annotation> getAnnotationByPath(@Param("geneId")String geneId, @Param("pathToNode")String pathToNode, @Param("username")String username);

	
	/*
	@Select("select annotation_id annotationId, a.username, gene_id geneId, structure_id structureId, submitted dateSubmited, a.position, strength, pattern, comments, stack_id stackId, " +
			" firstname firstName, surname lastName, is_flagged isFlagged, path_to_node pathToNode" +
			" from annotation a left outer join user_detail ud on a.username=ud.username" +
			" where gene_id=#{geneId} and structure_id=#{structureId}")
	public List<Annotation> getAnnotationByStructure(@Param("geneId")String geneId, @Param("structureId")String structureId);
	
	/*
	@Select("select annotation_id annotationId, a.username, gene_id geneId, structure_id structureId, submitted dateSubmited, a.position, strength, pattern, comments, stack_id stackId, " +
			" firstname firstName, surname lastName, is_flagged isFlagged, path_to_node pathToNode" +
			" from annotation a left outer join user_detail ud on a.username=ud.username" +
			" where gene_id=#{geneId} and path_to_node like #{pathToNode}")

	@Insert("insert into annotation (username, gene_id, structure_id, strength, pattern, comments, stack_id, submitted, path_to_node)" +
			" values (#{username}, #{geneId}, #{structureId}, #{strength}, #{pattern}, #{comments}, #{stackId}, current_timestamp, #{pathToNode}) returns annotation_id")
	public Long storeAnnotation(Annotation annotation);
	
	// where username, gene_id, structure_id
	// #{username}, #{geneId}, #{structureId}
	@Update("update annotation set strength=#{strength}, pattern=#{pattern}, comments=#{comments}" +
			" where username = #{username} and gene_id = #{geneId} and structure_id = #{structureId}")
	public int updateAnnotation(Annotation annotation);
		
	// VOTES
	@Insert("insert into annotation_vote (username, annotation_id, vote, ts, is_submission) values (#{username}, #{annotationId}, #{vote}, current_timestamp, #{isSubmission})")
	public int storeVote(@Param("username")String username, @Param("annotationId")long annotationId, @Param("vote")int vote, @Param("isSubmission")boolean isSubmission);
	
	@Update("update annotation_vote set vote = #{vote}, ts=current_timestamp where username = #{username} and annotation_id = #{annotationId}")
	public int updateVote(@Param("username")String username, @Param("annotationId")long annotationId, @Param("vote")int vote);
	
	@Select("select annotation_id annotationId, sum(vote) sumOfVotes, SUM(CASE WHEN vote > 0 THEN 1 ELSE 0 END) nUp, SUM(CASE WHEN vote < 0 THEN 1 ELSE 0 END) nDown " +
			" from annotation_vote where annotation_id=#{value}" +
			" group by annotation_id")
	public Annotation getVoteSum(long annotationId);

	@Select("select annotation_id from annotation where" +
			" structure_id=#{structureId} and gene_id=#{geneId} and strength=#{strength} and pattern=#{pattern}")
	public Long getAnnotationId(Annotation a);
	*/
	
	@Select("select annotation_id annotationId, /*username,*/ structure_id structureId, gene_id geneId, strength, pattern, path_to_node pathToNode " +
			" from annotation where structure_id = #{structureId} and gene_id = #{geneId} and" +
			" annotation_id in ( select annotation_id from annotation_vote where username = #{username} and is_submission )")
	public Annotation getAnnotationUser(@Param("username")String username, @Param("structureId")String structureId, @Param("geneId")String geneId);
	
	
	@Select("select annotation_id annotationId, /*username,*/ structure_id structureId, gene_id geneId, strength, pattern, path_to_node pathToNode" +
			" from annotation where structure_id=#{structureId} and gene_id=#{geneId} and pattern=#{pattern} and strength=#{strength}")
	public Annotation getAnnotation(@Param("structureId")String structureId, @Param("geneId")String geneId, @Param("pattern")String pattern, @Param("strength")String strength);
	
	@Select("select annotation_id annotationId, username, vote, ts, is_submission isSubmission " +
			" from annotation_vote where annotation_id = #{annotationId} and vote<>0 order by is_submission, username")
	public List<Annotation> getAnnotationEntrisWithVotes(long annotationId);
	
	@Select("insert into annotation (gene_id, structure_id, strength, pattern, comments, stack_id, submitted, path_to_node)" +
			" values (#{geneId}, #{structureId}, #{strength}, #{pattern}, #{comments}, #{stackId}, current_timestamp, #{pathToNode}) returning annotation_id")
	public Long insertAnnotation(Annotation annotation);
	
	
	
	// votes
	@Insert("insert into annotation_vote (username, annotation_id, vote, ts, is_submission) values (#{username}, #{annotationId}, #{vote}, current_timestamp, #{isSubmission})")
	public int insertVote(@Param("username")String username, @Param("annotationId")long annotationId, @Param("vote")int vote, @Param("isSubmission")boolean isSubmission);

	@Delete("delete from annotation_vote where annotation_id=#{annotationId} and username=#{username}")
	public void deleteAnnotationVote(@Param("annotationId")long annotationId, @Param("username")String username);

	//@Update("update annotation set username=(select username from annotation_vote where annotation_id= #{annotationId} order by ts asc limit 1) where annotation_id = #{annotationId} ")
	//public int updateUsernameForAnnotation(long annotationId);
	
	@Select("select is_submission from annotation_vote where annotation_id= #{annotationId} and username= #{username} ")
	public Boolean checkIfVoteIsSubmission(@Param("username")String username, @Param("annotationId")long annotationId);
	
	@Update("update annotation set is_flagged = #{isFlagged}, flagged_by = #{flaggedBy} where annotation_id = #{annotationId}")
	public int setFlag(@Param("annotationId")long annotationId, @Param("isFlagged")boolean isFlagged, @Param("flaggedBy")String flaggedBy);
	
	@Select("SELECT annotation_id annotationId, structure_id structureId, gene_id geneId, strength, pattern, submitted dateSubmitted, is_flagged isFlagged, path_to_node pathToNode, stack_id stackId, comments" +
			" FROM annotation where flagged_by = #{username} ")
	public List<Annotation> getFlagsForUser(String username);
	
	@Select("select structure_id structureId, path_to_node pathToNode, gene_id geneId, strength, pattern, diff sumOfVotes from (" +
			" select structure_id, path_to_node, gene_id, strength, pattern, sum(av_in.vote) diff" +
			"		 from annotation a_in" +
			"		 join annotation_vote av_in on a_in.annotation_id=av_in.annotation_id" +
			"		 group by path_to_node, structure_id, gene_id, strength, pattern" +
			") t1 where (t1.structure_id, t1.gene_id, t1.diff) in" +
			"   (select structure_id, gene_id, max(diff) from (" +
			"		select structure_id, gene_id, sum(av_in.vote) diff" +
			"		 from annotation a_in" +
			"		 join annotation_vote av_in on a_in.annotation_id=av_in.annotation_id" +
			"		 group by structure_id, gene_id, strength, pattern " +
			"	) t3 group by structure_id, gene_id)")
	public List<Annotation> getMostFavorableAnnotations();

	@Select("select path_to_node pathToNode, submission_count nSubmissions, n_flags nFlags, possible nPossible, weak nWeak, moderate nModerate, strong nStrong " +
			" from annotation_aggregate ")
	public List<Annotation> getAnnotationStats();
	
	@Select("select a.annotation_id annotationId, username, structure_id structureId, gene_id geneId, av.ts dateSubmited, pattern, strength, vote, a.is_flagged isFlagged, is_submission isSubmission " +
			" from annotation a join annotation_vote av on a.annotation_id=av.annotation_id " +
			" where ts > (select ts from user_preferences " +
			"             where pref_id = 'l_v_ann' and username = #{username} ) order by av.ts desc")
	public List<Annotation> getNewAnnotations(String username);

	@Select("select a.annotation_id annotationId, pattern, strength, a.is_flagged isFlagged " +
			" from annotation a  " +
			" where a.is_flagged")
	public List<Annotation> getFlags();
	
	@Update("update user_preferences set ts = current_timestamp where username = #{username} ")
	public int updateViewedAnnotations(String username);
	
	@Insert("insert into user_preferences values (#{username}, 'l_v_ann', '0', current_timestamp)")
	public int createViewedAnnotationsField(String username);

	@Select("SELECT annotation_id annotationId, structure_id structureId, gene_id geneId, strength, pattern, submitted dateSubmitted, is_flagged isFlagged, path_to_node pathToNode, stack_id stackId, comments" +
			" FROM public.annotation where annotation_id = #{annotationId} ")
	public Annotation getAnnotationByAnnotationId(long annotationId);

	@Select("SELECT a.annotation_id annotationId, structure_id structureId, gene_id geneId, strength, pattern, submitted dateSubmitted, is_flagged isFlagged, path_to_node pathToNode, stack_id stackId, comments" +
			" FROM annotation a join annotation_vote av on a.annotation_id=av.annotation_id" +
			" where av.username = #{username} and av.is_submission order by path_to_node")
	public List<Annotation> getAnnotationsForUser(String username);
	
	
	@Select("SELECT a.annotation_id annotationId, av.vote, structure_id structureId, gene_id geneId, strength, pattern, submitted dateSubmitted, is_flagged isFlagged, path_to_node pathToNode, stack_id stackId, comments" +
			" FROM annotation a join annotation_vote av on a.annotation_id=av.annotation_id" +
			" where av.username = #{username} and not av.is_submission order by path_to_node")
	public List<Annotation> getAnnotationsVotesForUser(String username);
}
