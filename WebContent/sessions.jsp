<%@page import="java.util.Collection"%>
<%@page import="com.vfb.socl.mappers.ActiveSessionsMapper"%>
<%@page import="com.vfb.socl.entity.ActiveSession"%>
<%@page import="com.vfb.socl.util.DB"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%
DB db = new DB();
ActiveSessionsMapper asm = db.getSessionMapper();

String sessionId = request.getParameter("sessionId");
if ("invalidate".equals(request.getParameter("action")) && sessionId!=null) {
	asm.closeOldSessionsBySSID(sessionId);
}
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" href="css/style.css" />

<title>Active sessions</title>
</head>
<body>
<body>
	<div id="mainDiv">
		<h1>VFB Community Annotation Tool</h1>
		<div id="contentDiv">
		<h2>List of active sessions</h2>
		
		<ul>
		<%
		Collection<ActiveSession> list = asm.getActiveSessions();
		for (ActiveSession s: list) {
			%>
			<li><%=s.toString() %> - <a href='sessions.jsp?action=invalidate&sessionId=<%=s.getSessionId() %>'>invalidate</a></li>
			<%
		}
		%>
		</ul>
		</div>
	</div>
</body>
</html>