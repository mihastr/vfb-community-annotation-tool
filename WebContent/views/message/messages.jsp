<%@page import="com.vfb.socl.entity.Person"%>
<%@page import="com.vfb.socl.mappers.MessageMapper"%>
<%@page import="com.vfb.socl.util.DB"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%
	String threadId = (String) request.getParameter("tid");
	String to =  (String) request.getParameter("to");
	Person user = (Person)session.getAttribute("user");
	if ((threadId==null || "0".equals(threadId)) && user!=null) {
		if (to!=null && to.length()>0) {
			DB db = new DB();
			Object sp = db.createSavePoint();
			try {
				MessageMapper mm = db.getMessageMapper();
				Long threadIdPrep = mm.getConversationIDParticipants(to, user.username);
				if (threadIdPrep!=null) {
					threadId = threadIdPrep.toString();
				} else {
					threadIdPrep = mm.createConversation("chat", user.username, "private", "");
					mm.addParticipant(threadIdPrep, user.username, user.username);
					mm.addParticipant(threadIdPrep, to, user.username);
					threadId = threadIdPrep.toString();
				}
				db.commit();
			} catch (Exception e) {
				db.rollback(sp);
				threadId = "0";
			}
		}
	}
%>
<script type="text/javascript">
	var loadMessagesInterval = 0;
	var conversationId = <%=threadId %>;
	var owner = '<%=request.getParameter("owner") %>';
	var myUsername = '<%=user.getUsername() %>';
	var threadVisibility =  '<%=request.getParameter("visibility") %>';
	if (threadVisibility==null || threadVisibility==undefined) threadVisibility = "private";
	var entityId = '<%=request.getParameter("entityId") %>';
	var isFirstLoad = true;
	var lastLoadedMsgID = 0;
	window.addEvent('domready',	function() {
		var req = new Request.HTML({
			method : 'get',
			update : $('contentDiv'),
			onRequest : function() {
				$('loading').show();
			},
			onSuccess : function() {
				$('loading').hide();
			}
		});
		function loadURL(url) {
			req.cancel();
			req.options.url = url;
			req.send();
		}
		$('toThreadList').addEvent('click', function(event) {
			clearInterval(loadMessagesInterval);
			event.stop();
			loadURL("views/message/threads.jsp");
		});
		
		
		function printMessage(msg) {
			/*
			msg =  {"error":"no","action":"getMessages","message":[{"messageId":2,"conversationId":2,"body":"Lorem ipsum dolor sit amet",
				"messageSent":"Jul 13, 2012 2:27:06 PM","firstName":"Franci","lastName":"Strumbelj","username":"franci","isOnline":true};
				*/
			console.log("users: " + msg.username  + " - " + myUsername);
			var isMe = false;
			if (msg.username==myUsername) {
				isMe = true;
			}
			
			var isOnlineClass = (msg.isOnline ? " class='is_online' ": "");
			return '<div class="' + (isMe ? 'dMessageMe' : 'dMessage') + '" oid="' + msg.messageId +'">' +
			'<div>' +
			'	<div class="dMessageFrom">' +
			'		<a href="#user/profile/'+ msg.username +'"'+ isOnlineClass +'>'+ msg.firstName + ' ' + msg.lastName +'</a>' + 
			'	</div>' +
			'	<div class="dMessageDate">'+ msg.messageSent +'</div>' +
			'	<div class="clear"></div>' +
			'</div>' +
			'<div class="dMessageBody">' + msg.body +'</div></div>';
		}
		
		var reqDoMessages = new Request.JSON({
			url: 'do_messages?action=getMessages',
			/*update : $('contentDiv'),*/
			onRequest : function() {
				//$('loading').show();
			},
			onSuccess : function(requestJSON, requestTxt) {
				//console.log(requestJSON.message);
				if (requestJSON.error!="no") return;
				if (requestJSON.message.length>0) {
					lastLoadedMsgID = requestJSON.message[0].messageId;
				}
				console.log(requestJSON.message);
				for (var i=requestJSON.message.length-1; i>=0; i--) {
					var el = new Element('div', {'html': printMessage(requestJSON.message[i])});
					$('dMessageList').grab(el, 'top');
					if (!isFirstLoad && requestJSON.message[i].username!=myUsername) {
						el.setStyle('opacity', 0.2);
						el.tween('opacity', 1);
					    el.morph({
					        'background-color': '#F9F9EE',
					        'color': '#78BA91',
					        'duration': 'long'
					      });
					    el.clear('style');
					}
				}
				isFirstLoad = false;
				//$('loading').hide();
			}
		});
		function loadMessages() {
			if (conversationId==0) return;
			reqDoMessages.post({'conversationId': conversationId, 'lastMsgId': lastLoadedMsgID});			
		}
		// first load
		$('dMessageList').set('html', '');
		
		loadMessages();
		loadMessagesInterval = setInterval(loadMessages,2000);
		intervals.push(loadMessagesInterval);
		
		var reqDoMessagesSend = new Request.JSON({
			url: 'do_messages?action=sendMessage',
			onRequest : function() {
				$('loading').show();
			},
			onSuccess : function(requestJSON, requestTxt) {
				if (requestJSON.error!="no") return;
				var nid=parseInt(requestJSON.message);
				if (nid!=NaN && nid>0) {
					conversationId = nid;
				}
				loadMessages();
				$('new_post_ta').set("value","");
				$('loading').hide();
			}
		});
		
		$('new_message_post').addEvent('click', function(e) {
			e.stop();
			var sObj = {
				'conversationId': conversationId,
				'body': $('new_post_ta').get("value")
			};
			console.log(conversationId);
			if (conversationId==0) {
				sObj.visibility = threadVisibility;
				sObj.owner = owner;
				sObj.entityId = entityId;
				sObj.title = ""; 
			}
			reqDoMessagesSend.post(sObj);
		});

		var reqDoMessagesAddParticipant = new Request.JSON({
			url: 'do_messages?action=addParticipant',
			onRequest : function() {
				//$('loading').show();
			},
			onSuccess : function(requestJSON, requestTxt) {
				if (requestJSON.error!="no") return;
				conversationId = requestJSON.message;
				reqDoMessagesGetParticipants.post({'conversationId': conversationId});
				$('msg_add_participant').set('value', '');
			}
		});		
		
		
		var reqDoMessagesGetParticipants = new Request.JSON({
			url: 'do_messages?action=getParticipants',
			onRequest : function() {
				//$('loading').show();
			},
			onSuccess : function(requestJSON, requestTxt) {
				if (requestJSON.error!="no") return;
				$('participant_list').set('html', '');
				requestJSON.message.each(function(person, index) {
					var name = (person.firstName==undefined ? person.username : person.firstName) + ' ' + (person.lastName==undefined ? '' : person.lastName);
					var onlineClass = null;
					if (person.isOnline || person.isOnXMPP) {
						onlineClass='user_online';
					} else {
						onlineClass='user_offline';
					}
					var element = new Element('li', {'html': '<a href="#user/profile/'+person.username+'">'+ name +'</a>' + (person.isOnXMPP ? '<img src="images/mobile_cue.gif" />' : ''), 'class': onlineClass});
					element.getElement('a').addEvent('click',	function(event) {
								event.stop();
								// GET http://localhost:8080/VFBAnnotation/views/user/profile.jsp?id=bd
								//click in #user/profile/bd
								var link = $(this).getAttribute("href").replace("#", "").split("/");
								console.log(link);
								var path = "views/" + link[0] + "/" + link[1] + ".jsp?id="+ link[2];
								console.log(path);
								loadURL(path);
								console.log("click out");
							});
					
					$('participant_list').adopt(element);
				});
			}
		}).post({'conversationId': conversationId});
		
		
		function initAutoComplete(data) {
			var ac = new Meio.Autocomplete($('msg_add_participant'), data, {
				selectOnTab : true,
				onNoItemToList : function(elements) {
					elements.field.node.highlight('#ff0000');
				},
				valueFilter: function(data){
					console.log(data);
			        return data.identifier;
			    },
				onSelect : function(elements, value) {
					/*
					console.log("selected: " + JSON.stringify(value));
					var path = "views/user/profile.jsp?id";
					req.options.url = path;
					req.get({'pid': value.username});
					console.log(path);
					*/
					reqDoMessagesAddParticipant.post({'recipient': value.username, 'conversationId': conversationId });
				},
				filter : {
					filter: function(text, data){
						console.log("filter: " + text);
						var txt = (data.firstName!=undefined ? data.firstName : data.username) + " " + (data.lastName!=undefined ? data.lastName : "");
						//console.log(txt);
						return text ? txt.trim().standardize().test(new RegExp(text.standardize().escapeRegExp(), 'i')) : true;
					},
					formatMatch: function(text, data, i){
						var txt = (data.firstName!=undefined ? data.firstName : data.username) + " " + (data.lastName!=undefined ? data.lastName : "");
						return txt.trim();
					},
					formatItem: function(text, data){
						var txt = (data.firstName!=undefined ? data.firstName : data.username) + " " + (data.lastName!=undefined ? data.lastName : "");
						return txt.trim();
					}					
					/*
					type : 'contains',
					path : 'firstName'
					*/
				}
			});
			//console.log("ac done");			
		}
		initAutoComplete('do_person?action=seekPerson');
		
		
	});
</script>
<p id="testDiv"></p>
<h2>
	View Messages (thred:
	<%=threadId%>)
</h2>
<p>
	<a href="#" id="toThreadList">To inbox</a>
</p>

<div>
	<div style="float: left">
		<jsp:include page="newMessage.jsp">
			<jsp:param value="630" name="taWidth" />
		</jsp:include>
	</div>
	<div class="float_left">
		<h3>Participants</h3>
		<p>
			<input id="msg_add_participant" class="msg_add_input" type="text" />
		</p>
		<ul class="msgs" id='participant_list' style="min-height: 100px;">

		</ul>
	</div>
	<!-- 
	<div class="float_right">
		<h3>Attachments</h3>
		<p>
			<input id="msg_add_atachment" class="msg_add_input" type="text" /><input type="button"
				value="add" />
		</p>
		<ul class="msgs">
			<li><a href="#">doc2.docx</a></li>
			<li><a href="#">image1.png</a></li>
		</ul>
	</div>	
	
	<div class="float_right">
		<h3>Links</h3>
		<p>
			<input id="msg_add_link" class="msg_add_input" type="text" /><input type="button"
				value="add" />
		</p>
		<ul class="msgs">
			<li><a href="#">doc2.docx</a></li>
			<li><a href="#">image1.png</a></li>
		</ul>
	</div>
	-->		
	<div class="clear"></div>
</div>
