<script type="text/javascript">

window.addEvent('domready', function() {
	var loadMessagesInterval = 0;
	var lastLoadedThreadID = 0;
	
	function printMessage(msg) {
		/* {"conversationId":2,"title":"test","sessionStart":"Jul 13, 2012 12:52:41 AM",
			"visibility":"private","hasNewMessages":true,"sender":"miha","senderName":"Miha Strumbelj",
			"body":"test body","lastMessageTime":"Jul 13, 2012 1:26:14 AM","firstName":"Miha","lastName":"Strumbelj","username":"miha",
			"isOnline":true}
		*/
		var isOnlineClass = (msg.isOnline ? " class='is_online' ": "");
		var isLMSOnlineClass = (msg.isLMSOnline ? " class='is_online' ": "");
		var discussionBoardInfo = (msg.boardLocation!=undefined ? "DB: " + msg.boardLocation: "");
		
		var hasNewMessages = (msg.hasNewMessages ? " has_new": " no_new");
		return '<div class="dMessage'+ hasNewMessages +'" oid="' + msg.conversationId +'">' +
		'<div>' +
		'	<div class="dMessageFrom">' +
		'		<a href="#user/profile/'+ msg.username +'"'+ isOnlineClass +'>'+ msg.firstName + ' ' + msg.lastName +'</a>' +
		(msg.title!=null ? ' - <b>' + msg.title + '</b>' : '') + 
		'	</div>' +
		'	<div class="dMessageDB">' + discussionBoardInfo + '</div>' +
		'	<div class="dMessageDate">'+ msg.sessionStart +'</div>' +
		'	<div class="clear"></div>' +
		'</div>' +
		'<div class="dMessageBody">' + 
			(msg.sender!=msg.username ? '<a href="#user/profile/'+ msg.sender +'"'+ isLMSOnlineClass +'>' + msg.senderName + '</a>: ' : '') +
		msg.body +'</div></div>';
	}
	
	var reqDoMessages = new Request.JSON({
		url: 'do_messages?action=getThreads',
		/*update : $('contentDiv'),*/
		onRequest : function() {
			//$('loading').show();
		},
		onSuccess : function(requestJSON, requestTxt) {
			//console.log(requestJSON.message);
			if (requestJSON.error!="no") return;
			if (requestJSON.message.length>0) {
				lastLoadedThreadID = requestJSON.message[0].conversationId;
			}
			$('dThreadList').set('html', '');
			for (var i=requestJSON.message.length-1; i>=0; i--) {
				var el = new Element('div', {'html': printMessage(requestJSON.message[i])});
				$('dThreadList').grab(el, 'top');
				el.addEvent('click', function(event) {
					clearInterval( loadMessagesInterval );
					var url = "views/message/messages.jsp?tid=" + this.getElement('div.dMessage').get('oid');
					loadURL(url);			
				});
			}
			//$('loading').hide();
		}
	});
	function loadMessages() {
		reqDoMessages.post({ 'conversationId': lastLoadedThreadID });			
		//console.log("test - done");
	}
	loadMessages();
	loadMessagesInterval = setInterval(loadMessages,5000);
	intervals.push(loadMessagesInterval);

	
	$('loading').hide();
	
	var req = new Request.HTML({
		method : 'get',
		update : $('contentDiv'),
		onRequest : function() {
			$('loading').show();
		},
		onSuccess : function() {
			$('loading').hide();
		}
	});
	function loadURL(url) {
		req.cancel();
		req.options.url = url;
		req.send();
	}
	
	$('msg_new_btn').addEvent('click', function(event) {
		event.stop();
		clearInterval(loadMessagesInterval);
		var url = "views/message/messages.jsp?tid=0";
		loadURL(url);			
	});
	
	$('toThreadList').addEvent('click', function(event) {
		event.stop();
		loadURL("views/message/threads.jsp");
	});

});
</script>
<h2>View Threads</h2>
<p><a href="#" id="msg_new_btn">New message</a></p>

<div id="dThreadList" style="min-height: 500px;">
<!-- 
	<div class="dMessage" oid="1">
		<div>
			<div class="dMessageFrom">
				<a href="#">Miha Strumbelj</a>, <a href="#">Nestor Milyaev</a>
			</div>
			<div class="dMessageDate">Date: 4th of July 2012</div>
			<div class="clear"></div>
		</div>
		<div class="dMessageBody">Lorem ipsum dolor sit amet,
			consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt
			ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim...</div>
	</div>
	
	<div class="dMessage" oid="2">
		<div>
			<div class="dMessageFrom">
				<a href="#">Miha Strumbelj</a>, <a href="#">Nestor Milyaev</a>
			</div>
			<div class="dMessageDate">Date: 3rd of July 2012</div>
			<div class="clear"></div>
		</div>
		<div class="dMessageBody">Lorem ipsum dolor sit amet,
			consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt
			ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim...</div>
	</div>
	
	<div class="dMessage" oid="3">
		<div>
			<div class="dMessageFrom">
				<a href="#">Miha Strumbelj</a>, <a href="#">Nestor Milyaev</a>
			</div>
			<div class="dMessageDate">Date: 3rd of July 2012</div>
			<div class="clear"></div>
		</div>
		<div class="dMessageBody">Lorem ipsum dolor sit amet,
			consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt
			ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim...</div>
	</div>
	
	<div class="dMessage" oid="4">
		<div>
			<div class="dMessageFrom">
				<a href="#">Miha Strumbelj</a>, <a href="#">Nestor Milyaev</a>
			</div>
			<div class="dMessageDate">Date: 3rd of July 2012</div>
			<div class="clear"></div>
		</div>
		<div class="dMessageBody">Lorem ipsum dolor sit amet,
			consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt
			ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim...</div>
	</div>
	-->
</div>
