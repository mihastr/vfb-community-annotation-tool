<%
String nodeId = request.getParameter("nodeId");
%>

<script type="text/javascript">
	var d;
	var nodesInfo;
	var geneId = "CaMKII";
	var loadAnnotations;
	var mostFavorableAnnotations = [];
	var reqGetMostFavorableAnnotations;
	var dNodeId = <%=nodeId %>;

	window.addEvent('domready', function() {
		d = new dTree('d');

		var reqDoAnnotation = new Request.JSON({
			url: 'do_annotations?action=store',
			method : 'post',
			update : $('contentDiv'),
			onRequest : function() {
				$('loading').show();
			},
			onSuccess : function(requestJSON, requestTxt) {
				loadAnnotations(requestJSON.message.nodeId);
				//console.log(this.options);
				$('loading').hide();
			}
		});
				
		Tips2.attach();
		
		var reqGetMostFavorableAnnotations = new Request.JSON({
			url: 'do_annotations?action=getMostFavorableAnnotations',
			method : 'post',
			onRequest : function() {
				$('loading').show();
			},
			onSuccess : function(requestJSON, requestTxt) {
				if (requestJSON.error!="no") return;
				// list of: {"nodeId":0,"annotationId":0,"strength":"possible","pattern":"homogeneous","isSubmission":false,"sumOfVotes":1,"nUp":1,"nDown":0,"nSubmissions":0,"vote":0,"isFlagged":false,"pathToNode":"0/11/189/190/192"}
				//console.log(mostFavorableAnnotations);
				if (mostFavorableAnnotations.length>0)
				mostFavorableAnnotations.each(function (element, index) {
					var nid = element.pathToNode.split("/");
					//console.log(nid[nid.length-1]);
					$$('a[name="node' + nid[nid.length-1] + '"]').removeClass(element.strength);
				});
				mostFavorableAnnotations = requestJSON.message.annotations;
				if (mostFavorableAnnotations.length>0)
				mostFavorableAnnotations.each(function (element, index) {
					var nid = element.pathToNode.split("/");
					//console.log(nid[nid.length-1]);
					$$('a[name="node' + nid[nid.length-1] + '"]').addClass(element.strength);
				});
				
				requestJSON.message.entry_count.each(function(element, index) {
					var nid = element.pathToNode.split("/");
					$$('a[name="node' + nid[nid.length-1] + '"] span.entry_count').set('html', element.nSubmissions);
					var nLink = $$('a[name="node' + nid[nid.length-1] + '"]');
					var flagText = (element.nFlags!=null && element.nFlags!=undefined && element.nFlags>0 ? "<img src='images/icon_red_flag.gif' />" : "");
					$$('a[name="node' + nid[nid.length-1] + '"] span.for_flag').set('html', flagText);
					
					var f = 58;
					var lengths = [7+Math.round(f*(element.nPossible/element.nSubmissions)), 
					               7+Math.round(f*(element.nWeak/element.nSubmissions)),
					               7+Math.round(f*(element.nModerate/element.nSubmissions)),
					               7+Math.round(f*(element.nStrong/element.nSubmissions)),
					               ];
					
					var txt = ("<div style='width: " + 140 + "px;'>" +
					" <div><div style='width: 50px; float:left;'>possible</div><div class='possible' style='float:left; width: {0}px; '>{4}</div><div class='clear'></div></div>" +
					" <div><div style='width: 50px; float:left;'>weak</div><div class='weak' style='float:left; width: {1}px; '>{5}</div><div class='clear'></div></div>" +
					" <div><div style='width: 50px; float:left;'>moderate</div><div class='moderate' style='float:left; width: {2}px; '>{6}</div><div class='clear'></div></div>" +
					" <div><div style='width: 50px; float:left;'>strong</div><div class='strong' style='float:left; width: {3}px; '>{7}</div><div class='clear'></div></div>" +					
					" <div><div style='width: 50px; float:left;'>flags</div><div style='float:left; width: 58px; '>{8}</div><div class='clear'></div></div>" +
					"</div>").format(lengths[0], lengths[1], lengths[2], lengths[3],
					element.nPossible, element.nWeak, element.nModerate, element.nStrong, element.nFlags);
					//console.log(txt);
					//nLink.set('title', txt);
					nLink.store('tip:text', txt);
					//Tips2.attach(nLink);
				});
				
				//console.log(mostFavorableAnnotations);
				$('loading').hide();
			}
		});
		var refreshFavorableAnnotations = function() { reqGetMostFavorableAnnotations.get(); }
		refreshNodesInterval = setInterval(refreshFavorableAnnotations, 3000);
		intervals.push(refreshNodesInterval);

		function submitAnnotation(nodeId, strength, pattern, structureId, geneId, pathToNode) {
			reqDoAnnotation.cancel();
			reqDoAnnotation.post({'nodeId': nodeId, 'strength': strength, 'pattern': pattern, 'geneId': geneId, 'structureId': structureId, 'pathToNode': pathToNode});		
		};
		
		function storeAnnotationAction(nodeId, strength, pattern) {
			var structureId = nodesInfo[nodeId]['extId'][0];
			var pathToNode = nodesInfo[nodeId]['path'].join("/");
			submitAnnotation(nodeId, strength, pattern, structureId, geneId, pathToNode);
			d.openTo(nodeId, true);
		}
		
		
		
		function buildTree(structure, data) {
			nodesInfo = data;
			//d.add(0, -1, "stack");
			d.add(0, -1, data[0].name);
			function traverseRecursively(structure) {
				//console.log(structure);
				//console.log("parent nodeId: " + structure.node.nodeId);
				if (structure.node.children != undefined) {
					//console.log(structure.node.children.length);
					for ( var i = 0; i < structure.node.children.length; i++) {
						var child = structure.node.children[i];
						//console.log(parseInt(child.node.nodeId) + " " + parseInt(structure.node.nodeId) + " " + data[parseInt(child.node.nodeId)].name);
						var childNodeId = parseInt(child.node.nodeId);
						d.add(childNodeId, parseInt(structure.node.nodeId),
								data[childNodeId].name, childNodeId);
						traverseRecursively(child);
						//break;
					}
				}
				//traverseRecursively(structure.node.children)
			}
	
			traverseRecursively(structure);

			//d.closeAll();
			$('mytree').innerHTML = d;
			$$('#mytree a.node, #mytree a.nodeSel').addEvent('click',
					function(event) {
						event.stop();
						var nodeId = parseInt(this.get('href'));
						loadAnnotations(nodeId);
						//console.log(this);
					});
			
			if (dNodeId != undefined && dNodeId != null) {
				//alert(dNodeId);
				d.openTo(parseInt(dNodeId), true);
			} else {
				var ptn = window.location.hash;
				if (ptn) {
					ptn = ptn.split("/");
					if (ptn.length==3) {
						d.openTo(parseInt(ptn[2]), true);
					}
				} 
				//if (window.location.href.hash) {
				
				//}
			}
			
			//console.log("build  context menu start");
			var context = new ContextMenu({
				targets: '#mytree a',
				menu: 'contextmenu',
				actions: {
					// possible  weak moderate strong  
					// homogeneous graded regional spotted singlecell nopattern
					homogeneous_possible: function(element,ref) {
						storeAnnotationAction(parseInt(element.get('href')), "possible", "homogeneous");
					},
					homogeneous_weak: function(element,ref) {
						storeAnnotationAction(parseInt(element.get('href')), "weak", "homogeneous");
					},
					homogeneous_moderate: function(element,ref) {
						storeAnnotationAction(parseInt(element.get('href')), "moderate", "homogeneous"); 
					},
					homogeneous_strong: function(element,ref) {
						storeAnnotationAction(parseInt(element.get('href')), "strong", "homogeneous"); 
					},
					// ******
					graded_possible: function(element,ref) {
						storeAnnotationAction(parseInt(element.get('href')), "possible", "graded");  
					},
					graded_weak: function(element,ref) {
						storeAnnotationAction(parseInt(element.get('href')), "weak", "graded");   
					},
					graded_moderate: function(element,ref) {
						storeAnnotationAction(parseInt(element.get('href')), "moderate", "graded");    
					},
					graded_strong: function(element,ref) {
						storeAnnotationAction(parseInt(element.get('href')), "strong", "graded");     
					},
					// ******
					regional_possible: function(element,ref) {
						storeAnnotationAction(parseInt(element.get('href')), "possible", "regional");      
					},
					regional_weak: function(element,ref) {
						storeAnnotationAction(parseInt(element.get('href')), "weak", "regional");       
						submitAnnotation("weak", "regional", structureId, geneId);
					},
					regional_moderate: function(element,ref) {
						storeAnnotationAction(parseInt(element.get('href')), "moderate", "regional");     
					},
					regional_strong: function(element,ref) {
						storeAnnotationAction(parseInt(element.get('href')), "strong", "regional");     
					},
					// ******
					spotted_possible: function(element,ref) {
						storeAnnotationAction(parseInt(element.get('href')), "possible", "spotted"); 
					},
					spotted_weak: function(element,ref) {
						storeAnnotationAction(parseInt(element.get('href')), "weak", "spotted"); 
					},
					spotted_moderate: function(element,ref) {
						storeAnnotationAction(parseInt(element.get('href')), "moderate", "spotted");  
					},
					spotted_strong: function(element,ref) {
						storeAnnotationAction(parseInt(element.get('href')), "strong", "spotted"); 
					},
					// ******
					singlecell_possible: function(element,ref) {
						storeAnnotationAction(parseInt(element.get('href')), "possible", "singlecell"); 
 					},
					singlecell_weak: function(element,ref) {
						storeAnnotationAction(parseInt(element.get('href')), "weak", "singlecell");
					},
					singlecell_moderate: function(element,ref) {
						storeAnnotationAction(parseInt(element.get('href')), "moderate", "singlecell"); 
					},
					singlecell_strong: function(element,ref) {
						storeAnnotationAction(parseInt(element.get('href')), "strong", "singlecell");
					},
					// ******
					nopattern: function(element,ref) {
						storeAnnotationAction(parseInt(element.get('href')), "none", "nopattern");
					}
				}
			});
			//console.log("build menu complete");
			
			// load most favorable annotations
			reqGetMostFavorableAnnotations.get();

		}

		var contentRequest = new Request.JSON({
			url : 'js/treeContentParsed.jso',
			onSuccess : function(content) {
				console.log("content loaded");
				var structureRequest = new Request.JSON({
					url : 'js/treeStructure.jso',
					onSuccess : function(structure) {
						console.log("structure loaded");
						buildTree(structure, content);
						console.log("selected node: " + d.getSelected());
						loadAnnotations(d.getSelected());
						initAutoComplete(content);
					}
				}).get({});

			}
		}).get({});



		function initAutoComplete(data) {
			var ac = new Meio.Autocomplete($('ontoSearchBox'), data, {
				selectOnTab : false,
				onNoItemToList : function(elements) {
					elements.field.node.highlight('#ff0000');
				},
				onSelect : function(elements, value) {
					//console.log(value);
					// expand to node
					d.openTo(parseInt(value.nodeId), true);
					// focus node
					window.location.href = "#node" + parseInt(value.nodeId);
					// load annotations for that node
					loadAnnotations(value.nodeId);
				},
				filter : {
					type : 'contains',
					path : 'name'
				}
			});
			//console.log("ac done");			
		}

		// functions to load the content
		$('loading').hide();
		var req = new Request.HTML({
			method : 'get',
			update : $('annotations'),
			onRequest : function() {
				$('loading').show();
			},
			onSuccess : function() {
				$('loading').hide();
			}
		});
		function loadURL(url, data) {
			req.cancel();
			req.options.url = url;
			req.get(data);
		}

		// functions which loads all the content
		loadAnnotations = function(nodeId) {
			if (nodeId==null) return;
			//$('annotations').innerHTML = JSON.stringify(nodesInfo[nodeId]);
			var node = nodesInfo[nodeId];
			var data = {
					"structureId": node["extId"][0],
					"geneId": "CaMKII",
					"pathToNode": node['path'].join("/"),
					"name": node['name']
			}
			loadURL("views/annotation/annotationAnatomyGene.jsp", data);
			window.location.href = "#annotation/annotations/" + nodeId;
			console.log("load annotatons " + nodeId);
			console.log(data);
		}
		

	});
</script>

<div class="float_left" style="width: 320px;" id="mytreeholder">
<h2>Annotations</h2>
	<p>
		<a href="javascript: d.openAll();">open all</a> | <a
			href="javascript: d.closeAll();">close all</a>
	</p>

	<div id="ontoSearch">
		<input type="text" id="ontoSearchBox" />
	</div>
	<div id="mytree"></div>
</div>
<div class="float_left" id="annotations" style="width: 665px; margin-left: 10px;"></div>
<!-- div class="float_right" style="width: 260px;">right</div -->
<div class="clear"></div>





<script language="javascript">
var submitAnnotation=null;
/*
window.addEvent('domready', function() {

	
	var req = new Request.JSON({
		url: 'do_annotations?action=store',
		method : 'post',
		update : $('contentDiv'),
		onRequest : function() {
			$('loading').show();
		},
		onSuccess : function(requestJSON, requestTxt) {
			console.log(requestTxt)
			$('loading').hide();
		}
	});
	submitAnnotation = function(strength, pattern) {
		req.cancel();
		req.post({'strength': strength, 'pattern': pattern});		
	}
	
	$$('#contextmenu div div').each(function(element, index) {
		element.addEvent('click', function(event) {
			var annotation = element.getElement('a').get('href').split("#")[1].split("_");
			//submitAnnotation(annotation[0], annotation[1]);
			//console.log(event);
		});

	});
	
});
*/
</script>
<!-- context menu -->
	<ul id="contextmenu">
		<!-- 
		<li><a href="#edit" class="edit">Edit</a></li>
		<li class="separator"><a href="#cut" class="cut">Cut</a></li>
		<li><a href="#copy" class="copy">Copy</a></li>
		<li><a href="#paste" class="paste">Paste</a></li>
		<li><a href="#delete" class="delete">Delete</a></li>
		<li class="separator"><a href="#quit" class="quit">Quit</a></li>
		 -->
		 <li><span class="pattern">homogeneous</span>
		 	<div> 
			 	<div class="possible"><a href="#homogeneous_possible">possible</a></div>
			 	<div class="weak"><a href="#homogeneous_weak">weak</a></div>
			 	<div class="moderate"><a href="#homogeneous_moderate">moderate</a></div>
			 	<div class="strong"><a href="#homogeneous_strong">strong</a></div>
		 	</div>
		 </li>
		 <li><span class="pattern">graded</span>
		 	<div>
			 	<div class="possible"><a href="#graded_possible">possible</a></div>
			 	<div class="weak"><a href="#graded_weak">weak</a></div>
			 	<div class="moderate"><a href="#graded_moderate">moderate</a></div>
			 	<div class="strong"><a href="#graded_strong">strong</a></div>
		 	</div>
		 </li>
 		 <li><span class="pattern">regional</span>
		 	<div>
			 	<div class="possible"><a href="#regional_possible">possible</a></div>
			 	<div class="weak"><a href="#regional_weak">weak</a></div>
			 	<div class="moderate"><a href="#regional_moderate">moderate</a></div>
			 	<div class="strong"><a href="#regional_strong">strong</a></div>
		 	</div>
		 </li>
		 <li><span class="pattern">spotted</span>
		 	<div>
			 	<div class="possible"><a href="#spotted_possible">possible</a></div>
			 	<div class="weak"><a href="#spotted_weak">weak</a></div>
			 	<div class="moderate"><a href="#spotted_moderate">moderate</a></div>
			 	<div class="strong"><a href="#spotted_strong">strong</a></div>
		 	</div>
		 </li>		 
		 <li><span class="pattern">single cell</span>
		 	<div>
			 	<div class="possible"><a href="#singlecell_possible">possible</a></div>
			 	<div class="weak"><a href="#singlecell_weak">weak</a></div>
			 	<div class="moderate"><a href="#singlecell_moderate">moderate</a></div>
			 	<div class="strong"><a href="#singlecell_strong">strong</a></div>
		 	</div>
		 </li>		 
		 <li><span class="pattern"><a href="#nopattern">no pattern</a></span> </li>		 
	</ul>