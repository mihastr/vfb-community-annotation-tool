<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%
	String jsPathVFB = "http://www.virtualflybrain.org";
	String jsPath = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<script type="text/javascript">
var _debug = true;
</script>

<link rel="stylesheet" media="all" type="text/css" href="<%=jsPathVFB %>/css/vfb/layout/header.css" />
      <link rel="stylesheet" media="all" type="text/css" href="<%=jsPathVFB %>/css/vfb/layout/layout.css" />
      <link rel="stylesheet" media="all" type="text/css" href=<%=jsPathVFB %>"/css/vfb/utils/help.css" />
      <link rel="stylesheet" media="all" type="text/css" href=<%=jsPathVFB %>"/css/vfb/utils/utils.css" />      
      <link rel="stylesheet" type="text/css" href="<%=jsPathVFB %>/css/vfb/utils/p7menu.css" />  
      
      <link rel="stylesheet" media="all" type="text/css" href="<%=jsPathVFB %>/css/utils/contextMenu.css" />  
	  <link rel="stylesheet" media="all" type="text/css" href="<%=jsPathVFB %>/css/tree/autocomplete.css" />
   	  <link rel="stylesheet" media="all" type="text/css" href="<%=jsPathVFB %>/css/vfb/layout/resize0.css" />      

      <link rel="stylesheet" type="text/css" media="all" href="<%=jsPathVFB %>/css/tools/tiledImage.css" />   
      <link rel="stylesheet" type="text/css" media="all" href="<%=jsPathVFB %>/css/tools/draggableWindow.css" />
      <link rel="stylesheet" type="text/css" media="all" href="<%=jsPathVFB %>/css/tools/rotation.css" />
      <link rel="stylesheet" type="text/css" media="all" href="<%=jsPathVFB %>/css/tools/fixedPoint.css" />
      <link rel="stylesheet" type="text/css" media="all" href="<%=jsPathVFB %>/css/tools/scale.css" />
      <link rel="stylesheet" type="text/css" media="all" href="<%=jsPathVFB %>/css/tools/refresh.css" />
      <link rel="stylesheet" type="text/css" media="all" href="<%=jsPathVFB %>/css/tools/measuring.css" />
      <link rel="stylesheet" type="text/css" media="all" href="<%=jsPathVFB %>/css/tools/selector.css" />
      <link rel="stylesheet" type="text/css" media="all" href="<%=jsPathVFB %>/css/tools/locator.css" />
      <link rel="stylesheet" type="text/css" media="all" href="<%=jsPathVFB %>/css/tools/slider.css" />
 
      <link rel="stylesheet" type="text/css" media="all" href="<%=jsPathVFB %>/css/utils/busyIndicator.css" />
      <link rel="stylesheet" type="text/css" media="all" href="<%=jsPathVFB %>/css/utils/marker.css" />
      <link rel="stylesheet" media="all" type="text/css" href="<%=jsPathVFB %>/css/utils/emapMenu.css" />

      <link rel="stylesheet" media="all" type="text/css" href="<%=jsPathVFB %>/css/tree/tree.css" />
      <link rel="stylesheet" media="all" type="text/css" href="<%=jsPathVFB %>/css/tree/colourPick.css" />
      <link rel="stylesheet" href="<%=jsPathVFB %>/thirdParty/smoothbox/smoothbox.css" type="text/css" media="screen" />	   

<script type="text/javascript"
	src="<%=jsPath%>/javascript/thirdParty/json2.js"></script>
<script type="text/javascript"
	src="<%=jsPath%>/javascript/thirdParty/mootools-core-1.3.2.js"></script>
<script type="text/javascript"
	src="<%=jsPath%>/javascript/thirdParty/mootools-more-1.3.2.1.js"></script>
<script type="text/javascript"
	src="<%=jsPath%>/javascript/thirdParty/mifTree.js"></script>

<script type="text/javascript"
	src="<%=jsPath%>/javascript/utils/busyIndicator.js"></script>
<script type="text/javascript"
	src="<%=jsPath%>/javascript/utils/utilities.js"></script>
<script type="text/javascript"
	src="<%=jsPath%>/javascript/utils/ajaxContentLoader.js"></script>
<script type="text/javascript"
	src="<%=jsPath%>/javascript/utils/emapMenu.js"></script>

<script type="text/javascript"
	src="<%=jsPath%>/javascript/tiledImageModel.js"></script>
<script type="text/javascript"
	src="<%=jsPath%>/javascript/tiledImageView.js"></script>

<script type="text/javascript"
	src="<%=jsPath%>/javascript/tools/draggableWindow.js"></script>
<script type="text/javascript"
	src="<%=jsPath%>/javascript/tools/tiledImageTool.js"></script>
<script type="text/javascript"
	src="<%=jsPath%>/javascript/tools/expressionLevelKey.js"></script>
<script type="text/javascript"
	src="<%=jsPath%>/javascript/tools/sliderComponent.js"></script>
<script type="text/javascript"
	src="<%=jsPath%>/javascript/tools/tiledImageLocatorTool.js"></script>
<script type="text/javascript"
	src="<%=jsPath%>/javascript/tools/tiledImageDistanceTool.js"></script>
<script type="text/javascript"
	src="<%=jsPath%>/javascript/tools/tiledImageLayerTool.js"></script>
<script type="text/javascript"
	src="<%=jsPath%>/javascript/tools/tiledImagePropertiesTool.js"></script>
<script type="text/javascript"
	src="<%=jsPath%>/javascript/tools/tiledImageRotationTool.js"></script>
<script type="text/javascript"
	src="<%=jsPath%>/javascript/tools/tiledImageRefreshTool.js"></script>
<script type="text/javascript"
	src="<%=jsPath%>/javascript/tools/tiledImageMeasuringTool.js"></script>
<script type="text/javascript"
	src="<%=jsPath%>/javascript/tree/treeImplementVFB.js"></script>
<script type="text/javascript"
	src="<%=jsPath%>/javascript/tree/tiledImageTreeTool.js"></script>
<script type="text/javascript"
	src="<%=jsPath%>/javascript/tools/tiledImageScaleTool.js"></script>
<script type="text/javascript"
	src="<%=jsPath%>/javascript/tools/tiledImageFixedPointTool.js"></script>
<script type="text/javascript"
	src="<%=jsPath%>/javascript/tree/colorPicker.js"></script>
<script type="text/javascript"
	src="<%=jsPath%>/javascript/tree/autocomplete.js"></script>
<script type="text/javascript"
	src="<%=jsPath%>/javascript/tree/contextMenu.js"></script>
<script type="text/javascript"
	src="<%=jsPath%>/javascript/tree/contextMenuVFB.js"></script>
<script type="text/javascript" src="<%=jsPath%>/javascript/vfb/utils.js"></script>
<script type="text/javascript"
	src="<%=jsPath%>/thirdParty/smoothbox/smoothbox.js"></script>
<script type="text/javascript"
	src="<%=jsPath%>/javascript/vfb/mailEncoder.js"></script>
<title>3D Browser</title>
<script type="text/javascript">
console.log("before before event");

	var selectedNodeId = "";
	var addNodeId = '';
	var domainList = readCookieAsArray('domainList');
	//alert("Domain List: " + domainList);
	if (addNodeId !== undefined && addNodeId != null && addNodeId != '') {
		//alert("including " + addNodeId);
		domainList.include(addNodeId);
	}
	//alert("Updated cookie: " + domainList);
	saveArrayAsCookie(domainList, 'domainList');
	//	   Cookie.dispose('domainList');
	function doOnload() {
		autocompleteList = [
				{
					"id" : "FBbt:00045003",
					"text" : "accessory medulla"
				},
				{
					"id" : "FBbt:00045003",
					"text" : "aMe (synonym)"
				},
				{
					"id" : "FBbt:00045003",
					"text" : "AME (synonym)"
				},
				{
					"id" : "FBbt:00007401",
					"text" : "adult antennal lobe"
				},
				{
					"id" : "FBbt:00007401",
					"text" : "adult olfactory lobe (synonym)"
				},
				{
					"id" : "FBbt:00007401",
					"text" : "AL (synonym)"
				},
				{
					"id" : "FBbt:00067500",
					"text" : "adult antennal lobe glomerulus"
				},
				{
					"id" : "FBbt:00003624",
					"text" : "adult brain"
				},
				{
					"id" : "FBbt:00003632",
					"text" : "adult central complex"
				},
				{
					"id" : "FBbt:00003632",
					"text" : "adult central body complex (synonym)"
				},
				{
					"id" : "FBbt:00003632",
					"text" : "central body (synonym)"
				},
				{
					"id" : "FBbt:00003632",
					"text" : "central body complex (synonym)"
				},
				{
					"id" : "FBbt:00003684",
					"text" : "adult mushroom body"
				},
				{
					"id" : "FBbt:00003684",
					"text" : "corpora pedunculata (synonym)"
				},
				{
					"id" : "FBbt:00007453",
					"text" : "adult pedunculus"
				},
				{
					"id" : "FBbt:00007453",
					"text" : "adult mushroom body pedunculus (synonym)"
				},
				{
					"id" : "FBbt:00014013",
					"text" : "adult subesophageal ganglion"
				},
				{
					"id" : "FBbt:00014013",
					"text" : "SOG (synonym)"
				},
				{
					"id" : "FBbt:00014013",
					"text" : "suboesophageal ganglion (synonym)"
				},
				{
					"id" : "FBbt:00007093",
					"text" : "antennal lobe glomerulus 2"
				},
				{
					"id" : "FBbt:00007093",
					"text" : "antennal glomerulus 2 (synonym)"
				},
				{
					"id" : "FBbt:00003960",
					"text" : "antennal lobe glomerulus D"
				},
				{
					"id" : "FBbt:00003960",
					"text" : "antennal glomerulus D (synonym)"
				},
				{
					"id" : "FBbt:00003932",
					"text" : "antennal lobe glomerulus DA1"
				},
				{
					"id" : "FBbt:00003932",
					"text" : "antennal glomerulus DA1 (synonym)"
				},
				{
					"id" : "FBbt:00003933",
					"text" : "antennal lobe glomerulus DA2"
				},
				{
					"id" : "FBbt:00003933",
					"text" : "antennal glomerulus DA2 (synonym)"
				},
				{
					"id" : "FBbt:00003934",
					"text" : "antennal lobe glomerulus DA3"
				},
				{
					"id" : "FBbt:00003934",
					"text" : "antennal glomerulus DA3 (synonym)"
				},
				{
					"id" : "FBbt:00003935",
					"text" : "antennal lobe glomerulus DA4"
				},
				{
					"id" : "FBbt:00003935",
					"text" : "antennal glomerulus DA4 (synonym)"
				},
				{
					"id" : "FBbt:00007360",
					"text" : "antennal lobe glomerulus DA4 lateral compartment"
				},
				{
					"id" : "FBbt:00007360",
					"text" : "DA4l (synonym)"
				},
				{
					"id" : "FBbt:00007361",
					"text" : "antennal lobe glomerulus DA4 medial compartment"
				},
				{
					"id" : "FBbt:00007361",
					"text" : "DA4m (synonym)"
				},
				{
					"id" : "FBbt:00003961",
					"text" : "antennal lobe glomerulus DC1"
				},
				{
					"id" : "FBbt:00003961",
					"text" : "antennal glomerulus DC1 (synonym)"
				},
				{
					"id" : "FBbt:00003962",
					"text" : "antennal lobe glomerulus DC2"
				},
				{
					"id" : "FBbt:00003962",
					"text" : "antennal glomerulus DC2 (synonym)"
				},
				{
					"id" : "FBbt:00003963",
					"text" : "antennal lobe glomerulus DC3"
				},
				{
					"id" : "FBbt:00003963",
					"text" : "antennal glomerulus DC3 (synonym)"
				},
				{
					"id" : "FBbt:00100531",
					"text" : "antennal lobe glomerulus DC4"
				},
				{
					"id" : "FBbt:00100531",
					"text" : "antennal glomerulus DC4 (synonym)"
				},
				{
					"id" : "FBbt:00003968",
					"text" : "antennal lobe glomerulus DL1"
				},
				{
					"id" : "FBbt:00003968",
					"text" : "antennal glomerulus DL1 (synonym)"
				},
				{
					"id" : "FBbt:00100375",
					"text" : "antennal lobe glomerulus DL1 lateral compartment"
				},
				{
					"id" : "FBbt:00100375",
					"text" : "DL1l (synonym)"
				},
				{
					"id" : "FBbt:00003969",
					"text" : "antennal lobe glomerulus DL2"
				},
				{
					"id" : "FBbt:00003969",
					"text" : "antennal glomerulus DL2 (synonym)"
				},
				{
					"id" : "FBbt:00100377",
					"text" : "antennal lobe glomerulus DL2 dorsal compartment"
				},
				{
					"id" : "FBbt:00100377",
					"text" : "DL2d (synonym)"
				},
				{
					"id" : "FBbt:00100376",
					"text" : "antennal lobe glomerulus DL2 ventral compartment"
				},
				{
					"id" : "FBbt:00100376",
					"text" : "DL2v (synonym)"
				},
				{
					"id" : "FBbt:00003964",
					"text" : "antennal lobe glomerulus DL3"
				},
				{
					"id" : "FBbt:00003964",
					"text" : "antennal glomerulus DL3 (synonym)"
				},
				{
					"id" : "FBbt:00003965",
					"text" : "antennal lobe glomerulus DL4"
				},
				{
					"id" : "FBbt:00003965",
					"text" : "antennal glomerulus DL4 (synonym)"
				},
				{
					"id" : "FBbt:00003970",
					"text" : "antennal lobe glomerulus DL5"
				},
				{
					"id" : "FBbt:00003970",
					"text" : "antennal glomerulus DL5 (synonym)"
				},
				{
					"id" : "FBbt:00007442",
					"text" : "antennal lobe glomerulus DL6"
				},
				{
					"id" : "FBbt:00007442",
					"text" : "antennal glomerulus DL6 (synonym)"
				},
				{
					"id" : "FBbt:00003975",
					"text" : "antennal lobe glomerulus DM1"
				},
				{
					"id" : "FBbt:00003975",
					"text" : "antennal glomerulus DM1 (synonym)"
				},
				{
					"id" : "FBbt:00003971",
					"text" : "antennal lobe glomerulus DM2"
				},
				{
					"id" : "FBbt:00003971",
					"text" : "antennal glomerulus DM2 (synonym)"
				},
				{
					"id" : "FBbt:00003972",
					"text" : "antennal lobe glomerulus DM3"
				},
				{
					"id" : "FBbt:00003972",
					"text" : "antennal glomerulus DM3 (synonym)"
				},
				{
					"id" : "FBbt:00003976",
					"text" : "antennal lobe glomerulus DM4"
				},
				{
					"id" : "FBbt:00003976",
					"text" : "antennal glomerulus DM4 (synonym)"
				},
				{
					"id" : "FBbt:00003940",
					"text" : "antennal lobe glomerulus DM5"
				},
				{
					"id" : "FBbt:00003940",
					"text" : "antennal glomerulus DM5 (synonym)"
				},
				{
					"id" : "FBbt:00003941",
					"text" : "antennal lobe glomerulus DM6"
				},
				{
					"id" : "FBbt:00003941",
					"text" : "antennal glomerulus DM6 (synonym)"
				},
				{
					"id" : "FBbt:00003977",
					"text" : "antennal lobe glomerulus DP1"
				},
				{
					"id" : "FBbt:00003977",
					"text" : "antennal glomerulus DP1 (synonym)"
				},
				{
					"id" : "FBbt:00007098",
					"text" : "antennal lobe glomerulus DP1 lateral compartment"
				},
				{
					"id" : "FBbt:00007098",
					"text" : "DP1l (synonym)"
				},
				{
					"id" : "FBbt:00007097",
					"text" : "antennal lobe glomerulus DP1 medial compartment"
				},
				{
					"id" : "FBbt:00007097",
					"text" : "DP1m (synonym)"
				},
				{
					"id" : "FBbt:00003951",
					"text" : "antennal lobe glomerulus V"
				},
				{
					"id" : "FBbt:00003951",
					"text" : "antennal glomerulus V (synonym)"
				},
				{
					"id" : "FBbt:00003936",
					"text" : "antennal lobe glomerulus VA1"
				},
				{
					"id" : "FBbt:00003936",
					"text" : "antennal glomerulus VA1 (synonym)"
				},
				{
					"id" : "FBbt:00007101",
					"text" : "antennal lobe glomerulus VA1 dorsal compartment"
				},
				{
					"id" : "FBbt:00007101",
					"text" : "VA1d (synonym)"
				},
				{
					"id" : "FBbt:00007099",
					"text" : "antennal lobe glomerulus VA1 lateral compartment"
				},
				{
					"id" : "FBbt:00007099",
					"text" : "VA1l (synonym)"
				},
				{
					"id" : "FBbt:00007100",
					"text" : "antennal lobe glomerulus VA1 medial compartment"
				},
				{
					"id" : "FBbt:00007100",
					"text" : "VA1m (synonym)"
				},
				{
					"id" : "FBbt:00007363",
					"text" : "antennal lobe glomerulus VA1 ventral compartment"
				},
				{
					"id" : "FBbt:00007363",
					"text" : "VA1v (synonym)"
				},
				{
					"id" : "FBbt:00003942",
					"text" : "antennal lobe glomerulus VA2"
				},
				{
					"id" : "FBbt:00003942",
					"text" : "antennal glomerulus VA2 (synonym)"
				},
				{
					"id" : "FBbt:00003943",
					"text" : "antennal lobe glomerulus VA3"
				},
				{
					"id" : "FBbt:00003943",
					"text" : "antennal glomerulus VA3 (synonym)"
				},
				{
					"id" : "FBbt:00003944",
					"text" : "antennal lobe glomerulus VA4"
				},
				{
					"id" : "FBbt:00003944",
					"text" : "antennal glomerulus VA4 (synonym)"
				},
				{
					"id" : "FBbt:00003937",
					"text" : "antennal lobe glomerulus VA5"
				},
				{
					"id" : "FBbt:00003937",
					"text" : "antennal glomerulus VA5 (synonym)"
				},
				{
					"id" : "FBbt:00003938",
					"text" : "antennal lobe glomerulus VA6"
				},
				{
					"id" : "FBbt:00003938",
					"text" : "antennal glomerulus VA6 (synonym)"
				},
				{
					"id" : "FBbt:00003945",
					"text" : "antennal lobe glomerulus VA7"
				},
				{
					"id" : "FBbt:00003945",
					"text" : "antennal glomerulus VA7 (synonym)"
				},
				{
					"id" : "FBbt:00007103",
					"text" : "antennal lobe glomerulus VA7 lateral compartment"
				},
				{
					"id" : "FBbt:00007103",
					"text" : "VA7l (synonym)"
				},
				{
					"id" : "FBbt:00007102",
					"text" : "antennal lobe glomerulus VA7 medial compartment"
				},
				{
					"id" : "FBbt:00007102",
					"text" : "VA7m (synonym)"
				},
				{
					"id" : "FBbt:00003952",
					"text" : "antennal lobe glomerulus VC1"
				},
				{
					"id" : "FBbt:00003952",
					"text" : "antennal glomerulus VC1 (synonym)"
				},
				{
					"id" : "FBbt:00003946",
					"text" : "antennal lobe glomerulus VC2"
				},
				{
					"id" : "FBbt:00003946",
					"text" : "antennal glomerulus VC2 (synonym)"
				},
				{
					"id" : "FBbt:00003953",
					"text" : "antennal lobe glomerulus VC3"
				},
				{
					"id" : "FBbt:00003953",
					"text" : "antennal glomerulus VC3 (synonym)"
				},
				{
					"id" : "FBbt:00007105",
					"text" : "antennal lobe glomerulus VC3 lateral compartment"
				},
				{
					"id" : "FBbt:00007105",
					"text" : "VC3l (synonym)"
				},
				{
					"id" : "FBbt:00007104",
					"text" : "antennal lobe glomerulus VC3 medial compartment"
				},
				{
					"id" : "FBbt:00007104",
					"text" : "VC3m (synonym)"
				},
				{
					"id" : "FBbt:00007366",
					"text" : "antennal lobe glomerulus VC4"
				},
				{
					"id" : "FBbt:00007366",
					"text" : "antennal glomerulus VC4 (synonym)"
				},
				{
					"id" : "FBbt:00110029",
					"text" : "antennal lobe glomerulus VC5"
				},
				{
					"id" : "FBbt:00110029",
					"text" : "antennal glomerulus VC5 (synonym)"
				},
				{
					"id" : "FBbt:00003954",
					"text" : "antennal lobe glomerulus VL1"
				},
				{
					"id" : "FBbt:00003954",
					"text" : "antennal glomerulus VL1 (synonym)"
				},
				{
					"id" : "FBbt:00003955",
					"text" : "antennal lobe glomerulus VL2"
				},
				{
					"id" : "FBbt:00003955",
					"text" : "antennal glomerulus VL2 (synonym)"
				},
				{
					"id" : "FBbt:00007106",
					"text" : "antennal lobe glomerulus VL2 anterior compartment"
				},
				{
					"id" : "FBbt:00007106",
					"text" : "VL2a (synonym)"
				},
				{
					"id" : "FBbt:00007107",
					"text" : "antennal lobe glomerulus VL2 posterior compartment"
				},
				{
					"id" : "FBbt:00007107",
					"text" : "VL2p (synonym)"
				},
				{
					"id" : "FBbt:00003956",
					"text" : "antennal lobe glomerulus VM1"
				},
				{
					"id" : "FBbt:00003956",
					"text" : "antennal glomerulus VM1 (synonym)"
				},
				{
					"id" : "FBbt:00003947",
					"text" : "antennal lobe glomerulus VM2"
				},
				{
					"id" : "FBbt:00003947",
					"text" : "antennal glomerulus VM2 (synonym)"
				},
				{
					"id" : "FBbt:00003948",
					"text" : "antennal lobe glomerulus VM3"
				},
				{
					"id" : "FBbt:00003948",
					"text" : "antennal glomerulus VM3 (synonym)"
				},
				{
					"id" : "FBbt:00003957",
					"text" : "antennal lobe glomerulus VM4"
				},
				{
					"id" : "FBbt:00003957",
					"text" : "antennal glomerulus VM4 (synonym)"
				},
				{
					"id" : "FBbt:00003949",
					"text" : "antennal lobe glomerulus VM5"
				},
				{
					"id" : "FBbt:00003949",
					"text" : "antennal glomerulus VM5 (synonym)"
				},
				{
					"id" : "FBbt:00007391",
					"text" : "antennal lobe glomerulus VM5 dorsal compartment"
				},
				{
					"id" : "FBbt:00007391",
					"text" : "antennal lobe glomerulus VM5d (synonym)"
				},
				{
					"id" : "FBbt:00007365",
					"text" : "antennal lobe glomerulus VM5 ventral compartment"
				},
				{
					"id" : "FBbt:00007365",
					"text" : "VM5v (synonym)"
				},
				{
					"id" : "FBbt:00003958",
					"text" : "antennal lobe glomerulus VM6"
				},
				{
					"id" : "FBbt:00003958",
					"text" : "antennal glomerulus VM6 (synonym)"
				},
				{
					"id" : "FBbt:00003973",
					"text" : "antennal lobe glomerulus VM7"
				},
				{
					"id" : "FBbt:00003973",
					"text" : "antennal glomerulus VM7 (synonym)"
				},
				{
					"id" : "FBbt:00110028",
					"text" : "antennal lobe glomerulus VM7 dorsal compartment"
				},
				{
					"id" : "FBbt:00110028",
					"text" : "antennal lobe glomerulus VM7d (synonym)"
				},
				{
					"id" : "FBbt:00007092",
					"text" : "antennal lobe glomerulus VM7 ventral compartment"
				},
				{
					"id" : "FBbt:00007092",
					"text" : "antennal lobe glomerulus 1 (synonym)"
				},
				{
					"id" : "FBbt:00007092",
					"text" : "antennal lobe glomerulus VM7v (synonym)"
				},
				{
					"id" : "FBbt:00003926",
					"text" : "antennal lobe glomerulus VP"
				},
				{
					"id" : "FBbt:00003926",
					"text" : "antennal glomerulus VP (synonym)"
				},
				{
					"id" : "FBbt:00003927",
					"text" : "antennal lobe glomerulus VP1"
				},
				{
					"id" : "FBbt:00003927",
					"text" : "antennal glomerulus VP1 (synonym)"
				},
				{
					"id" : "FBbt:00003928",
					"text" : "antennal lobe glomerulus VP2"
				},
				{
					"id" : "FBbt:00003928",
					"text" : "antennal glomerulus VP2 (synonym)"
				},
				{
					"id" : "FBbt:00003928",
					"text" : "medial PAP (synonym)"
				},
				{
					"id" : "FBbt:00003928",
					"text" : "medial proximal antennal protocerebrum (synonym)"
				},
				{
					"id" : "FBbt:00003928",
					"text" : "VP2 (synonym)"
				},
				{
					"id" : "FBbt:00003929",
					"text" : "antennal lobe glomerulus VP3"
				},
				{
					"id" : "FBbt:00003929",
					"text" : "antennal glomerulus VP3 (synonym)"
				},
				{
					"id" : "FBbt:00003929",
					"text" : "lateral PAP (synonym)"
				},
				{
					"id" : "FBbt:00003929",
					"text" : "lateral proximal antennal protocerebrum (synonym)"
				},
				{
					"id" : "FBbt:00003929",
					"text" : "VP3 (synonym)"
				},
				{
					"id" : "FBbt:00100362",
					"text" : "antennal lobe hub"
				},
				{
					"id" : "FBbt:00100362",
					"text" : "ALH (synonym)"
				},
				{
					"id" : "FBbt:00003982",
					"text" : "antennal mechanosensory and motor center"
				},
				{
					"id" : "FBbt:00003982",
					"text" : "AMC (synonym)"
				},
				{
					"id" : "FBbt:00003982",
					"text" : "AMMC (synonym)"
				},
				{
					"id" : "FBbt:00003982",
					"text" : "antennal mechanosensory region (synonym)"
				},
				{
					"id" : "FBbt:00003982",
					"text" : "antenno-mechanosensory center (synonym)"
				},
				{
					"id" : "FBbt:00003982",
					"text" : "dorsal lobe (synonym)"
				},
				{
					"id" : "FBbt:00100015",
					"text" : "antennal mechanosensory and motor center zone A"
				},
				{
					"id" : "FBbt:00100015",
					"text" : "AMMC zone A (synonym)"
				},
				{
					"id" : "FBbt:00100015",
					"text" : "antennal mechanosensory and motor center zone A (synonym)"
				},
				{
					"id" : "FBbt:00100015",
					"text" : "zone A (synonym)"
				},
				{
					"id" : "FBbt:00100016",
					"text" : "antennal mechanosensory and motor center zone B"
				},
				{
					"id" : "FBbt:00100016",
					"text" : "AMMC zone B (synonym)"
				},
				{
					"id" : "FBbt:00100016",
					"text" : "antennal mechanosensory and motor center zone B (synonym)"
				},
				{
					"id" : "FBbt:00100016",
					"text" : "zone B (synonym)"
				},
				{
					"id" : "FBbt:00100017",
					"text" : "antennal mechanosensory and motor center zone C"
				},
				{
					"id" : "FBbt:00100017",
					"text" : "AMMC zone C (synonym)"
				},
				{
					"id" : "FBbt:00100017",
					"text" : "antennal mechanosensory and motor center zone C (synonym)"
				},
				{
					"id" : "FBbt:00100017",
					"text" : "zone C (synonym)"
				},
				{
					"id" : "FBbt:00100018",
					"text" : "antennal mechanosensory and motor center zone D"
				},
				{
					"id" : "FBbt:00100018",
					"text" : "AMMC zone D (synonym)"
				},
				{
					"id" : "FBbt:00100018",
					"text" : "antennal mechanosensory and motor center zone D (synonym)"
				},
				{
					"id" : "FBbt:00100018",
					"text" : "zone D (synonym)"
				},
				{
					"id" : "FBbt:00100019",
					"text" : "antennal mechanosensory and motor center zone E"
				},
				{
					"id" : "FBbt:00100019",
					"text" : "ammc zone E (synonym)"
				},
				{
					"id" : "FBbt:00100019",
					"text" : "AMMC zone E (synonym)"
				},
				{
					"id" : "FBbt:00100019",
					"text" : "antennal mechanosensory and motor center zone E (synonym)"
				}, {
					"id" : "FBbt:00100019",
					"text" : "zone E (synonym)"
				}, {
					"id" : "FBbt:00100365",
					"text" : "anterior bulb"
				}, {
					"id" : "FBbt:00040057",
					"text" : "anterior maxillary sensory center"
				}, {
					"id" : "FBbt:00040057",
					"text" : "AMS (synonym)"
				}, {
					"id" : "FBbt:00040045",
					"text" : "anterior superior lateral protocerebrum"
				}, {
					"id" : "FBbt:00040045",
					"text" : "ASLP (synonym)"
				}, {
					"id" : "FBbt:00040045",
					"text" : "middle slpr (synonym)"
				}, {
					"id" : "FBbt:00040045",
					"text" : "mslpr (synonym)"
				}, {
					"id" : "FBbt:00040062",
					"text" : "anterior superior medial protocerebrum"
				}, {
					"id" : "FBbt:00040062",
					"text" : "anterior SLP (synonym)"
				}, {
					"id" : "FBbt:00040062",
					"text" : "ASMP (synonym)"
				}, {
					"id" : "FBbt:00040043",
					"text" : "anterior ventrolateral protocerebrum"
				}, {
					"id" : "FBbt:00040043",
					"text" : "anterior VLP (synonym)"
				}, {
					"id" : "FBbt:00040043",
					"text" : "AVLP (synonym)"
				}, {
					"id" : "FBbt:00045039",
					"text" : "antler"
				}, {
					"id" : "FBbt:00045039",
					"text" : "ATL (synonym)"
				}, {
					"id" : "FBbt:00003682",
					"text" : "bulb"
				}, {
					"id" : "FBbt:00003682",
					"text" : "BU (synonym)"
				}, {
					"id" : "FBbt:00003682",
					"text" : "isthmus (synonym)"
				}, {
					"id" : "FBbt:00003682",
					"text" : "lateral triangle (synonym)"
				}, {
					"id" : "FBbt:00003682",
					"text" : "ltr (synonym)"
				}, {
					"id" : "FBbt:00007385",
					"text" : "calyx of adult mushroom body"
				}, {
					"id" : "FBbt:00045051",
					"text" : "cantle"
				}, {
					"id" : "FBbt:00045051",
					"text" : "CAN (synonym)"
				}, {
					"id" : "FBbt:00045051",
					"text" : "PONPp (synonym)"
				}, {
					"id" : "FBbt:00045051",
					"text" : "posterior perioesophageal neuropil (synonym)"
				}, {
					"id" : "FBbt:00045020",
					"text" : "central body"
				}, {
					"id" : "FBbt:00045020",
					"text" : "CB (synonym)"
				}, {
					"id" : "FBbt:00040047",
					"text" : "clamp"
				}, {
					"id" : "FBbt:00040047",
					"text" : "CL (synonym)"
				}, {
					"id" : "FBbt:00045037",
					"text" : "crepine"
				}, {
					"id" : "FBbt:00045037",
					"text" : "CRE (synonym)"
				}, {
					"id" : "FBbt:00045042",
					"text" : "delta"
				}, {
					"id" : "FBbt:00045002",
					"text" : "dorsal rim area of medulla"
				}, {
					"id" : "FBbt:00045002",
					"text" : "MEDRA (synonym)"
				}, {
					"id" : "FBbt:00003678",
					"text" : "ellipsoid body"
				}, {
					"id" : "FBbt:00003678",
					"text" : "eb (synonym)"
				}, {
					"id" : "FBbt:00003678",
					"text" : "EB (synonym)"
				}, {
					"id" : "FBbt:00007555",
					"text" : "ellipsoid body inner ring"
				}, {
					"id" : "FBbt:00040034",
					"text" : "ellipsoid body layer"
				}, {
					"id" : "FBbt:00040034",
					"text" : "ellipsoid body ring (synonym)"
				}, {
					"id" : "FBbt:00040034",
					"text" : "layer of ellipsoid body (synonym)"
				}, {
					"id" : "FBbt:00007556",
					"text" : "ellipsoid body outer ring"
				}, {
					"id" : "FBbt:00040070",
					"text" : "ellipsoid body segment"
				}, {
					"id" : "FBbt:00040070",
					"text" : "ellipsoid body glomerulus (synonym)"
				}, {
					"id" : "FBbt:00040070",
					"text" : "ellipsoid body sector (synonym)"
				}, {
					"id" : "FBbt:00040070",
					"text" : "segment of ellipsoid body (synonym)"
				}, {
					"id" : "FBbt:00007584",
					"text" : "ellipsoid body segment 1"
				}, {
					"id" : "FBbt:00007546",
					"text" : "ellipsoid body segment 2"
				}, {
					"id" : "FBbt:00007547",
					"text" : "ellipsoid body segment 3"
				}, {
					"id" : "FBbt:00007548",
					"text" : "ellipsoid body segment 4"
				}, {
					"id" : "FBbt:00007549",
					"text" : "ellipsoid body segment 5"
				}, {
					"id" : "FBbt:00007550",
					"text" : "ellipsoid body segment 6"
				}, {
					"id" : "FBbt:00007551",
					"text" : "ellipsoid body segment 7"
				}, {
					"id" : "FBbt:00007552",
					"text" : "ellipsoid body segment 8"
				}, {
					"id" : "FBbt:00040040",
					"text" : "epaulette"
				}, {
					"id" : "FBbt:00040040",
					"text" : "EPA (synonym)"
				}, {
					"id" : "FBbt:00003679",
					"text" : "fan-shaped body"
				}, {
					"id" : "FBbt:00003679",
					"text" : "fan shaped body (synonym)"
				}, {
					"id" : "FBbt:00003679",
					"text" : "fb (synonym)"
				}, {
					"id" : "FBbt:00003679",
					"text" : "FB (synonym)"
				}, {
					"id" : "FBbt:00040035",
					"text" : "fan-shaped body layer"
				}, {
					"id" : "FBbt:00040035",
					"text" : "layer of fan-shaped body (synonym)"
				}, {
					"id" : "FBbt:00007487",
					"text" : "fan-shaped body layer 1"
				}, {
					"id" : "FBbt:00007487",
					"text" : "fan shaped body layer 1 (synonym)"
				}, {
					"id" : "FBbt:00007488",
					"text" : "fan-shaped body layer 2"
				}, {
					"id" : "FBbt:00007488",
					"text" : "fan shaped body layer 2 (synonym)"
				}, {
					"id" : "FBbt:00007490",
					"text" : "fan-shaped body layer 3"
				}, {
					"id" : "FBbt:00007490",
					"text" : "fan shaped body layer 3 (synonym)"
				}, {
					"id" : "FBbt:00007491",
					"text" : "fan-shaped body layer 4"
				}, {
					"id" : "FBbt:00007491",
					"text" : "fan shaped body layer 4 (synonym)"
				}, {
					"id" : "FBbt:00007492",
					"text" : "fan-shaped body layer 5"
				}, {
					"id" : "FBbt:00007492",
					"text" : "fan shaped body layer 5 (synonym)"
				}, {
					"id" : "FBbt:00007493",
					"text" : "fan-shaped body layer 6"
				}, {
					"id" : "FBbt:00007493",
					"text" : "fan shaped body layer 6 (synonym)"
				}, {
					"id" : "FBbt:00007493",
					"text" : "superior arch (synonym)"
				}, {
					"id" : "FBbt:00007494",
					"text" : "fan-shaped body segment pair W"
				}, {
					"id" : "FBbt:00007494",
					"text" : "fan shaped body segment pair W (synonym)"
				}, {
					"id" : "FBbt:00007494",
					"text" : "segment A of fan-shaped body (synonym)"
				}, {
					"id" : "FBbt:00007494",
					"text" : "segment H of fan-shaped body (synonym)"
				}, {
					"id" : "FBbt:00007495",
					"text" : "fan-shaped body segment pair X"
				}, {
					"id" : "FBbt:00007495",
					"text" : "fan shaped body segment pair X (synonym)"
				}, {
					"id" : "FBbt:00007495",
					"text" : "segment B of fan-shaped body (synonym)"
				}, {
					"id" : "FBbt:00007495",
					"text" : "segment G of fan-shaped body (synonym)"
				}, {
					"id" : "FBbt:00007496",
					"text" : "fan-shaped body segment pair Y"
				}, {
					"id" : "FBbt:00007496",
					"text" : "fan shaped body segment pair Y (synonym)"
				}, {
					"id" : "FBbt:00007496",
					"text" : "segment C of fan-shaped body (synonym)"
				}, {
					"id" : "FBbt:00007496",
					"text" : "segment F of fan-shaped body (synonym)"
				}, {
					"id" : "FBbt:00007497",
					"text" : "fan-shaped body segment pair Z"
				}, {
					"id" : "FBbt:00007497",
					"text" : "fan shaped body segment pair Z (synonym)"
				}, {
					"id" : "FBbt:00007497",
					"text" : "segment D of fan-shaped body (synonym)"
				}, {
					"id" : "FBbt:00007497",
					"text" : "segment E of fan-shaped body (synonym)"
				}, {
					"id" : "FBbt:00045050",
					"text" : "flange"
				}, {
					"id" : "FBbt:00045050",
					"text" : "FLA (synonym)"
				}, {
					"id" : "FBbt:00040060",
					"text" : "gall"
				}, {
					"id" : "FBbt:00040060",
					"text" : "GA (synonym)"
				}, {
					"id" : "FBbt:00040060",
					"text" : "Gall of lateral accessory lobe (synonym)"
				}, {
					"id" : "FBbt:00040039",
					"text" : "gorget"
				}, {
					"id" : "FBbt:00040039",
					"text" : "GOR (synonym)"
				}, {
					"id" : "FBbt:00040050",
					"text" : "inferior bridge"
				}, {
					"id" : "FBbt:00040050",
					"text" : "IB (synonym)"
				}, {
					"id" : "FBbt:00100364",
					"text" : "inferior bulb"
				}, {
					"id" : "FBbt:00040049",
					"text" : "inferior clamp"
				}, {
					"id" : "FBbt:00040049",
					"text" : "ICL (synonym)"
				}, {
					"id" : "FBbt:00040037",
					"text" : "inferior neuropils"
				}, {
					"id" : "FBbt:00040037",
					"text" : "inferior protocerebrum (synonym)"
				}, {
					"id" : "FBbt:00040037",
					"text" : "INP (synonym)"
				}, {
					"id" : "FBbt:00040054",
					"text" : "inferior pharyngeal sensory center"
				}, {
					"id" : "FBbt:00040054",
					"text" : "IPhS (synonym)"
				}, {
					"id" : "FBbt:00040054",
					"text" : "ventral pharyngeal sensory center (synonym)"
				}, {
					"id" : "FBbt:00045046",
					"text" : "inferior posterior slope"
				}, {
					"id" : "FBbt:00045046",
					"text" : "IPS (synonym)"
				}, {
					"id" : "FBbt:00045027",
					"text" : "inferior ventrolateral protocerebrum"
				}, {
					"id" : "FBbt:00045027",
					"text" : "anterior VLP (synonym)"
				}, {
					"id" : "FBbt:00045027",
					"text" : "IVLP (synonym)"
				}, {
					"id" : "FBbt:00003757",
					"text" : "inner medulla"
				}, {
					"id" : "FBbt:00003757",
					"text" : "MEI (synonym)"
				}, {
					"id" : "FBbt:00040055",
					"text" : "labial sensory center"
				}, {
					"id" : "FBbt:00040055",
					"text" : "labellar sensory center (synonym)"
				}, {
					"id" : "FBbt:00040055",
					"text" : "LS (synonym)"
				}, {
					"id" : "FBbt:00003708",
					"text" : "lamina"
				}, {
					"id" : "FBbt:00003708",
					"text" : "La (synonym)"
				}, {
					"id" : "FBbt:00003708",
					"text" : "LA (synonym)"
				}, {
					"id" : "FBbt:00045000",
					"text" : "lamina dorsal rim area"
				}, {
					"id" : "FBbt:00045000",
					"text" : "LDRA (synonym)"
				}, {
					"id" : "FBbt:00001943",
					"text" : "lamina plexus"
				}, {
					"id" : "FBbt:00001943",
					"text" : "lamina plexiform layer (synonym)"
				}, {
					"id" : "FBbt:00003681",
					"text" : "lateral accessory lobe"
				}, {
					"id" : "FBbt:00003681",
					"text" : "vbo (synonym)"
				}, {
					"id" : "FBbt:00003681",
					"text" : "ventral body (synonym)"
				}, {
					"id" : "FBbt:00040001",
					"text" : "lateral complex"
				}, {
					"id" : "FBbt:00040001",
					"text" : "LX (synonym)"
				}, {
					"id" : "FBbt:00007053",
					"text" : "lateral horn"
				}, {
					"id" : "FBbt:00007053",
					"text" : "LH (synonym)"
				}, {
					"id" : "FBbt:00003688",
					"text" : "lobe system of mushroom body"
				}, {
					"id" : "FBbt:00003688",
					"text" : "lobe of corpora pedunculata (synonym)"
				}, {
					"id" : "FBbt:00003688",
					"text" : "lobe of mushroom body (synonym)"
				}, {
					"id" : "FBbt:00003852",
					"text" : "lobula"
				}, {
					"id" : "FBbt:00003852",
					"text" : "Lo (synonym)"
				}, {
					"id" : "FBbt:00003852",
					"text" : "LO (synonym)"
				}, {
					"id" : "FBbt:00045004",
					"text" : "lobula complex"
				}, {
					"id" : "FBbt:00045004",
					"text" : "LOX (synonym)"
				}, {
					"id" : "FBbt:00003853",
					"text" : "lobula layer 1"
				}, {
					"id" : "FBbt:00003853",
					"text" : "lobula layer Lo1 (synonym)"
				}, {
					"id" : "FBbt:00003853",
					"text" : "T5 layer (synonym)"
				}, {
					"id" : "FBbt:00003854",
					"text" : "lobula layer 2"
				}, {
					"id" : "FBbt:00003854",
					"text" : "lobula layer Lo2 (synonym)"
				}, {
					"id" : "FBbt:00003855",
					"text" : "lobula layer 3"
				}, {
					"id" : "FBbt:00003855",
					"text" : "lobula layer Lo3 (synonym)"
				}, {
					"id" : "FBbt:00003856",
					"text" : "lobula layer 4"
				}, {
					"id" : "FBbt:00003856",
					"text" : "lobula layer Lo4 (synonym)"
				}, {
					"id" : "FBbt:00040016",
					"text" : "lobula layer 5"
				}, {
					"id" : "FBbt:00040017",
					"text" : "lobula layer 6"
				}, {
					"id" : "FBbt:00003885",
					"text" : "lobula plate"
				}, {
					"id" : "FBbt:00003885",
					"text" : "lobula plate neuropil (synonym)"
				}, {
					"id" : "FBbt:00003885",
					"text" : "Lop (synonym)"
				}, {
					"id" : "FBbt:00003885",
					"text" : "LOP (synonym)"
				}, {
					"id" : "FBbt:00003886",
					"text" : "lobula plate layer 1"
				}, {
					"id" : "FBbt:00003886",
					"text" : "HS layer (synonym)"
				}, {
					"id" : "FBbt:00003886",
					"text" : "layer Lop1 (synonym)"
				}, {
					"id" : "FBbt:00003887",
					"text" : "lobula plate layer 2"
				}, {
					"id" : "FBbt:00003887",
					"text" : "layer Lop2 (synonym)"
				}, {
					"id" : "FBbt:00003888",
					"text" : "lobula plate layer 3"
				}, {
					"id" : "FBbt:00003888",
					"text" : "layer Lop3 (synonym)"
				}, {
					"id" : "FBbt:00003889",
					"text" : "lobula plate layer 4"
				}, {
					"id" : "FBbt:00003889",
					"text" : "layer Lop4 (synonym)"
				}, {
					"id" : "FBbt:00040061",
					"text" : "lower lateral accessory lobe"
				}, {
					"id" : "FBbt:00040061",
					"text" : "LLAL (synonym)"
				}, {
					"id" : "FBbt:00003748",
					"text" : "medulla"
				}, {
					"id" : "FBbt:00003748",
					"text" : "m (synonym)"
				}, {
					"id" : "FBbt:00003748",
					"text" : "ME (synonym)"
				}, {
					"id" : "FBbt:00003748",
					"text" : "optic medulla (synonym)"
				}, {
					"id" : "FBbt:00003750",
					"text" : "medulla layer M1"
				}, {
					"id" : "FBbt:00003750",
					"text" : "stratum M1 (synonym)"
				}, {
					"id" : "FBbt:00003760",
					"text" : "medulla layer M10"
				}, {
					"id" : "FBbt:00003760",
					"text" : "T4 layer (synonym)"
				}, {
					"id" : "FBbt:00003751",
					"text" : "medulla layer M2"
				}, {
					"id" : "FBbt:00003751",
					"text" : "L2 layer (synonym)"
				}, {
					"id" : "FBbt:00003751",
					"text" : "layer M2 (synonym)"
				}, {
					"id" : "FBbt:00003751",
					"text" : "medulla layer 2 (synonym)"
				}, {
					"id" : "FBbt:00003751",
					"text" : "stratum M2 (synonym)"
				}, {
					"id" : "FBbt:00003752",
					"text" : "medulla layer M3"
				}, {
					"id" : "FBbt:00003752",
					"text" : "L3 layer (synonym)"
				}, {
					"id" : "FBbt:00003752",
					"text" : "medulla layer 3 (synonym)"
				}, {
					"id" : "FBbt:00003752",
					"text" : "stratum M3 (synonym)"
				}, {
					"id" : "FBbt:00003753",
					"text" : "medulla layer M4"
				}, {
					"id" : "FBbt:00003753",
					"text" : "L4 layer (synonym)"
				}, {
					"id" : "FBbt:00003753",
					"text" : "medulla layer 4 (synonym)"
				}, {
					"id" : "FBbt:00003753",
					"text" : "stratum M4 (synonym)"
				}, {
					"id" : "FBbt:00003754",
					"text" : "medulla layer M5"
				}, {
					"id" : "FBbt:00003754",
					"text" : "medulla layer 5 (synonym)"
				}, {
					"id" : "FBbt:00003754",
					"text" : "proximal L1 layer (synonym)"
				}, {
					"id" : "FBbt:00003754",
					"text" : "stratum M5 (synonym)"
				}, {
					"id" : "FBbt:00003755",
					"text" : "medulla layer M6"
				}, {
					"id" : "FBbt:00003755",
					"text" : "medulla layer 6 (synonym)"
				}, {
					"id" : "FBbt:00003755",
					"text" : "R7 layer (synonym)"
				}, {
					"id" : "FBbt:00003755",
					"text" : "R7 recipient layer (synonym)"
				}, {
					"id" : "FBbt:00003755",
					"text" : "stratum M6 (synonym)"
				}, {
					"id" : "FBbt:00003758",
					"text" : "medulla layer M8"
				}, {
					"id" : "FBbt:00003758",
					"text" : "medulla layer 8 (synonym)"
				}, {
					"id" : "FBbt:00003759",
					"text" : "medulla layer M9"
				}, {
					"id" : "FBbt:00003759",
					"text" : "medulla layer 9 (synonym)"
				}, {
					"id" : "FBbt:00045014",
					"text" : "mushroom body alpha accessory calyx fibers"
				}, {
					"id" : "FBbt:00045014",
					"text" : "vertical lobelet (synonym)"
				}, {
					"id" : "FBbt:00100258",
					"text" : "mushroom body alpha lobe core stratum"
				}, {
					"id" : "FBbt:00100258",
					"text" : "alphac (synonym)"
				}, {
					"id" : "FBbt:00100258",
					"text" : "alpha-c (synonym)"
				}, {
					"id" : "FBbt:00100259",
					"text" : "mushroom body alpha lobe inner core stratum"
				}, {
					"id" : "FBbt:00100259",
					"text" : "alpha-c(i) (synonym)"
				}, {
					"id" : "FBbt:00100259",
					"text" : "alphac(i) (synonym)"
				}, {
					"id" : "FBbt:00100260",
					"text" : "mushroom body alpha lobe outer core stratum"
				}, {
					"id" : "FBbt:00100260",
					"text" : "alpha-c(o) (synonym)"
				}, {
					"id" : "FBbt:00100260",
					"text" : "alphac(o) (synonym)"
				}, {
					"id" : "FBbt:00100256",
					"text" : "mushroom body alpha lobe posterior stratum"
				}, {
					"id" : "FBbt:00100256",
					"text" : "alphap (synonym)"
				}, {
					"id" : "FBbt:00100256",
					"text" : "alpha-p (synonym)"
				}, {
					"id" : "FBbt:00110000",
					"text" : "mushroom body alpha lobe segment"
				}, {
					"id" : "FBbt:00100285",
					"text" : "mushroom body alpha lobe segment 1"
				}, {
					"id" : "FBbt:00100285",
					"text" : "alpha1 (synonym)"
				}, {
					"id" : "FBbt:00100285",
					"text" : "alpha-1 (synonym)"
				}, {
					"id" : "FBbt:00100285",
					"text" : "alpha lobe base (synonym)"
				}, {
					"id" : "FBbt:00100286",
					"text" : "mushroom body alpha lobe segment 2"
				}, {
					"id" : "FBbt:00100286",
					"text" : "alpha2 (synonym)"
				}, {
					"id" : "FBbt:00100286",
					"text" : "alpha-2 (synonym)"
				}, {
					"id" : "FBbt:00100286",
					"text" : "alpha lobe shaft (synonym)"
				}, {
					"id" : "FBbt:00100287",
					"text" : "mushroom body alpha lobe segment 3"
				}, {
					"id" : "FBbt:00100287",
					"text" : "alpha3 (synonym)"
				}, {
					"id" : "FBbt:00100287",
					"text" : "alpha-3 (synonym)"
				}, {
					"id" : "FBbt:00100287",
					"text" : "alpha lobe tip (synonym)"
				}, {
					"id" : "FBbt:00110004",
					"text" : "mushroom body alpha lobe stratum"
				}, {
					"id" : "FBbt:00100257",
					"text" : "mushroom body alpha lobe surface stratum"
				}, {
					"id" : "FBbt:00100257",
					"text" : "alphas (synonym)"
				}, {
					"id" : "FBbt:00100257",
					"text" : "alpha-s (synonym)"
				}, {
					"id" : "FBbt:00100266",
					"text" : "mushroom body alpha' anterior stratum"
				}, {
					"id" : "FBbt:00100266",
					"text" : "alpha'a (synonym)"
				}, {
					"id" : "FBbt:00100266",
					"text" : "alpha'-a (synonym)"
				}, {
					"id" : "FBbt:00110001",
					"text" : "mushroom body alpha' lobe segment"
				}, {
					"id" : "FBbt:00100282",
					"text" : "mushroom body alpha' lobe segment 1"
				}, {
					"id" : "FBbt:00100282",
					"text" : "alpha'1 (synonym)"
				}, {
					"id" : "FBbt:00100282",
					"text" : "alpha'-1 (synonym)"
				}, {
					"id" : "FBbt:00100282",
					"text" : "alpha lobe base (synonym)"
				}, {
					"id" : "FBbt:00100283",
					"text" : "mushroom body alpha' lobe segment 2"
				}, {
					"id" : "FBbt:00100283",
					"text" : "alpha'2 (synonym)"
				}, {
					"id" : "FBbt:00100283",
					"text" : "alpha'-2 (synonym)"
				}, {
					"id" : "FBbt:00100283",
					"text" : "alpha' lobe shaft (synonym)"
				}, {
					"id" : "FBbt:00100284",
					"text" : "mushroom body alpha' lobe segment 3"
				}, {
					"id" : "FBbt:00100284",
					"text" : "alpha'3 (synonym)"
				}, {
					"id" : "FBbt:00100284",
					"text" : "alpha'-3 (synonym)"
				}, {
					"id" : "FBbt:00100284",
					"text" : "alpha' lobe tip (synonym)"
				}, {
					"id" : "FBbt:00110006",
					"text" : "mushroom body alpha' lobe stratum"
				}, {
					"id" : "FBbt:00100267",
					"text" : "mushroom body alpha' middle stratum"
				}, {
					"id" : "FBbt:00100267",
					"text" : "alpha'm (synonym)"
				}, {
					"id" : "FBbt:00100267",
					"text" : "alpha'-m (synonym)"
				}, {
					"id" : "FBbt:00100268",
					"text" : "mushroom body alpha' posterior stratum"
				}, {
					"id" : "FBbt:00100268",
					"text" : "alpha'p (synonym)"
				}, {
					"id" : "FBbt:00100268",
					"text" : "alpha'-p (synonym)"
				}, {
					"id" : "FBbt:00100252",
					"text" : "mushroom body alpha'' lobe"
				}, {
					"id" : "FBbt:00100252",
					"text" : "alpha'' (synonym)"
				}, {
					"id" : "FBbt:00003691",
					"text" : "mushroom body alpha'-lobe"
				}, {
					"id" : "FBbt:00003691",
					"text" : "alpha'-lobe (synonym)"
				}, {
					"id" : "FBbt:00003691",
					"text" : "alpha'-lobe of mushroom body (synonym)"
				}, {
					"id" : "FBbt:00003691",
					"text" : "delta lobe (synonym)"
				}, {
					"id" : "FBbt:00003690",
					"text" : "mushroom body alpha-lobe"
				}, {
					"id" : "FBbt:00003690",
					"text" : "alpha L (synonym)"
				}, {
					"id" : "FBbt:00003690",
					"text" : "alpha-lobe (synonym)"
				}, {
					"id" : "FBbt:00003690",
					"text" : "alpha-lobe of mushroom body (synonym)"
				}, {
					"id" : "FBbt:00045018",
					"text" : "mushroom body beta accessory calyx fibers"
				}, {
					"id" : "FBbt:00045018",
					"text" : "medial lobelet (synonym)"
				}, {
					"id" : "FBbt:00100263",
					"text" : "mushroom body beta lobe core stratum"
				}, {
					"id" : "FBbt:00100263",
					"text" : "betac (synonym)"
				}, {
					"id" : "FBbt:00100263",
					"text" : "beta-c (synonym)"
				}, {
					"id" : "FBbt:00100264",
					"text" : "mushroom body beta lobe inner core stratum"
				}, {
					"id" : "FBbt:00100264",
					"text" : "beta-c(i) (synonym)"
				}, {
					"id" : "FBbt:00100264",
					"text" : "betac(i) (synonym)"
				}, {
					"id" : "FBbt:00100265",
					"text" : "mushroom body beta lobe outer core stratum"
				}, {
					"id" : "FBbt:00100265",
					"text" : "beta-c(o) (synonym)"
				}, {
					"id" : "FBbt:00100265",
					"text" : "betac(o) (synonym)"
				}, {
					"id" : "FBbt:00100261",
					"text" : "mushroom body beta lobe posterior stratum"
				}, {
					"id" : "FBbt:00100261",
					"text" : "betap (synonym)"
				}, {
					"id" : "FBbt:00100261",
					"text" : "beta-p (synonym)"
				}, {
					"id" : "FBbt:00110003",
					"text" : "mushroom body beta lobe segment"
				}, {
					"id" : "FBbt:00100280",
					"text" : "mushroom body beta lobe segment 1"
				}, {
					"id" : "FBbt:00100280",
					"text" : "beta1 (synonym)"
				}, {
					"id" : "FBbt:00100280",
					"text" : "mushroom body beta lobe shaft (synonym)"
				}, {
					"id" : "FBbt:00100281",
					"text" : "mushroom body beta lobe segment 2"
				}, {
					"id" : "FBbt:00100281",
					"text" : "beta2 (synonym)"
				}, {
					"id" : "FBbt:00100281",
					"text" : "mushroom body beta lobe tip (synonym)"
				}, {
					"id" : "FBbt:00110005",
					"text" : "mushroom body beta lobe stratum"
				}, {
					"id" : "FBbt:00100262",
					"text" : "mushroom body beta lobe surface stratum"
				}, {
					"id" : "FBbt:00100262",
					"text" : "betas (synonym)"
				}, {
					"id" : "FBbt:00100262",
					"text" : "beta-s (synonym)"
				}, {
					"id" : "FBbt:00100269",
					"text" : "mushroom body beta' anterior stratum"
				}, {
					"id" : "FBbt:00100269",
					"text" : "beta'a (synonym)"
				}, {
					"id" : "FBbt:00100269",
					"text" : "beta'-a (synonym)"
				}, {
					"id" : "FBbt:00110009",
					"text" : "mushroom body beta' lobe segment"
				}, {
					"id" : "FBbt:00100278",
					"text" : "mushroom body beta' lobe segment 1"
				}, {
					"id" : "FBbt:00100278",
					"text" : "beta'1 (synonym)"
				}, {
					"id" : "FBbt:00100278",
					"text" : "mushroom body beta' lobe shaft (synonym)"
				}, {
					"id" : "FBbt:00100279",
					"text" : "mushroom body beta' lobe segment 2"
				}, {
					"id" : "FBbt:00100279",
					"text" : "beta'2 (synonym)"
				}, {
					"id" : "FBbt:00100279",
					"text" : "mushroom body beta' lobe tip (synonym)"
				}, {
					"id" : "FBbt:00110008",
					"text" : "mushroom body beta' lobe stratum"
				}, {
					"id" : "FBbt:00100270",
					"text" : "mushroom body beta' middle stratum"
				}, {
					"id" : "FBbt:00100270",
					"text" : "beta'm (synonym)"
				}, {
					"id" : "FBbt:00100270",
					"text" : "beta'-m (synonym)"
				}, {
					"id" : "FBbt:00100271",
					"text" : "mushroom body beta' posterior stratum"
				}, {
					"id" : "FBbt:00100271",
					"text" : "beta'p (synonym)"
				}, {
					"id" : "FBbt:00100271",
					"text" : "beta'-p (synonym)"
				}, {
					"id" : "FBbt:00100251",
					"text" : "mushroom body beta'' lobe"
				}, {
					"id" : "FBbt:00003694",
					"text" : "mushroom body beta'-lobe"
				}, {
					"id" : "FBbt:00003694",
					"text" : "beta'-lobe (synonym)"
				}, {
					"id" : "FBbt:00003694",
					"text" : "beta'-lobe of mushroom body (synonym)"
				}, {
					"id" : "FBbt:00003693",
					"text" : "mushroom body beta-lobe"
				}, {
					"id" : "FBbt:00003693",
					"text" : "beta-lobe (synonym)"
				}, {
					"id" : "FBbt:00003693",
					"text" : "beta-lobe of mushroom body (synonym)"
				}, {
					"id" : "FBbt:00110002",
					"text" : "mushroom body gamma lobe segment"
				}, {
					"id" : "FBbt:00100273",
					"text" : "mushroom body gamma lobe segment 1"
				}, {
					"id" : "FBbt:00100273",
					"text" : "gamma1 (synonym)"
				}, {
					"id" : "FBbt:00100273",
					"text" : "gamma-1 (synonym)"
				}, {
					"id" : "FBbt:00100274",
					"text" : "mushroom body gamma lobe segment 2"
				}, {
					"id" : "FBbt:00100274",
					"text" : "gamma2 (synonym)"
				}, {
					"id" : "FBbt:00100274",
					"text" : "gamma-2 (synonym)"
				}, {
					"id" : "FBbt:00100275",
					"text" : "mushroom body gamma lobe segment 3"
				}, {
					"id" : "FBbt:00100275",
					"text" : "gamma3 (synonym)"
				}, {
					"id" : "FBbt:00100275",
					"text" : "gamma-3 (synonym)"
				}, {
					"id" : "FBbt:00100276",
					"text" : "mushroom body gamma lobe segment 4"
				}, {
					"id" : "FBbt:00100276",
					"text" : "gamma4 (synonym)"
				}, {
					"id" : "FBbt:00100276",
					"text" : "gamma-4 (synonym)"
				}, {
					"id" : "FBbt:00100277",
					"text" : "mushroom body gamma lobe segment 5"
				}, {
					"id" : "FBbt:00100277",
					"text" : "gamma5 (synonym)"
				}, {
					"id" : "FBbt:00100277",
					"text" : "gamma-5 (synonym)"
				}, {
					"id" : "FBbt:00003695",
					"text" : "mushroom body gamma-lobe"
				}, {
					"id" : "FBbt:00003695",
					"text" : "gamma L (synonym)"
				}, {
					"id" : "FBbt:00003695",
					"text" : "gamma-lobe (synonym)"
				}, {
					"id" : "FBbt:00003695",
					"text" : "gamma-lobe of mushroom body (synonym)"
				}, {
					"id" : "FBbt:00005408",
					"text" : "mushroom body medial lobe"
				}, {
					"id" : "FBbt:00005408",
					"text" : "horizontal lobe of mushroom body (synonym)"
				}, {
					"id" : "FBbt:00005408",
					"text" : "medial branch of mushroom body (synonym)"
				}, {
					"id" : "FBbt:00005408",
					"text" : "medial lobe (synonym)"
				}, {
					"id" : "FBbt:00005408",
					"text" : "medial lobe of mushroom body (synonym)"
				}, {
					"id" : "FBbt:00045010",
					"text" : "mushroom body spur"
				}, {
					"id" : "FBbt:00045010",
					"text" : "heel of mushroom body (synonym)"
				}, {
					"id" : "FBbt:00045010",
					"text" : "knee of mushroom body (synonym)"
				}, {
					"id" : "FBbt:00045010",
					"text" : "mushroom body heel (synonym)"
				}, {
					"id" : "FBbt:00045010",
					"text" : "SPU (synonym)"
				}, {
					"id" : "FBbt:00045010",
					"text" : "spur (synonym)"
				}, {
					"id" : "FBbt:00005407",
					"text" : "mushroom body vertical lobe"
				}, {
					"id" : "FBbt:00005407",
					"text" : "dorsal lobe (synonym)"
				}, {
					"id" : "FBbt:00005407",
					"text" : "dorsal lobe of mushroom body (synonym)"
				}, {
					"id" : "FBbt:00005407",
					"text" : "vertical branch of mushroom body (synonym)"
				}, {
					"id" : "FBbt:00005407",
					"text" : "vertical lobe of mushroom body (synonym)"
				}, {
					"id" : "FBbt:00040033",
					"text" : "nodule"
				}, {
					"id" : "FBbt:00003680",
					"text" : "nodulus"
				}, {
					"id" : "FBbt:00003680",
					"text" : "no (synonym)"
				}, {
					"id" : "FBbt:00003680",
					"text" : "NO (synonym)"
				}, {
					"id" : "FBbt:00003680",
					"text" : "noduli (synonym)"
				}, {
					"id" : "FBbt:00003680",
					"text" : "ventral tubercle (synonym)"
				}, {
					"id" : "FBbt:00003747",
					"text" : "optic cartridge"
				}, {
					"id" : "FBbt:00003747",
					"text" : "cartridge (synonym)"
				}, {
					"id" : "FBbt:00003747",
					"text" : "lamina cartridge (synonym)"
				}, {
					"id" : "FBbt:00003698",
					"text" : "optic glomerulus"
				}, {
					"id" : "FBbt:00003698",
					"text" : "optic focus (synonym)"
				}, {
					"id" : "FBbt:00007581",
					"text" : "optic glomerulus of the PLP"
				}, {
					"id" : "FBbt:00007582",
					"text" : "optic glomerulus of the PVLP"
				}, {
					"id" : "FBbt:00003701",
					"text" : "optic lobe"
				}, {
					"id" : "FBbt:00003701",
					"text" : "ol (synonym)"
				}, {
					"id" : "FBbt:00007059",
					"text" : "optic tubercle"
				}, {
					"id" : "FBbt:00007059",
					"text" : "AOTU (synonym)"
				}, {
					"id" : "FBbt:00007059",
					"text" : "optic tubercle (synonym)"
				}, {
					"id" : "FBbt:00007059",
					"text" : "OTU (synonym)"
				}, {
					"id" : "FBbt:00003749",
					"text" : "outer medulla"
				}, {
					"id" : "FBbt:00045009",
					"text" : "pedunculus divide"
				}, {
					"id" : "FBbt:00045009",
					"text" : "heel (synonym)"
				}, {
					"id" : "FBbt:00045009",
					"text" : "knee (synonym)"
				}, {
					"id" : "FBbt:00045009",
					"text" : "lobe junction (synonym)"
				}, {
					"id" : "FBbt:00045009",
					"text" : "PEDD (synonym)"
				}, {
					"id" : "FBbt:00045008",
					"text" : "pedunculus neck"
				}, {
					"id" : "FBbt:00045008",
					"text" : "PEDN (synonym)"
				}, {
					"id" : "FBbt:00045047",
					"text" : "periesophageal neuropils"
				}, {
					"id" : "FBbt:00045047",
					"text" : "PONP (synonym)"
				}, {
					"id" : "FBbt:00100366",
					"text" : "plexiform lamina"
				}, {
					"id" : "FBbt:00100366",
					"text" : "PLLA (synonym)"
				}, {
					"id" : "FBbt:00045001",
					"text" : "plexiform medulla"
				}, {
					"id" : "FBbt:00045001",
					"text" : "medulla proper (synonym)"
				}, {
					"id" : "FBbt:00045001",
					"text" : "MEP (synonym)"
				}, {
					"id" : "FBbt:00045001",
					"text" : "PLME (synonym)"
				}, {
					"id" : "FBbt:00040044",
					"text" : "posterior lateral protocerebrum"
				}, {
					"id" : "FBbt:00040044",
					"text" : "PLP (synonym)"
				}, {
					"id" : "FBbt:00040044",
					"text" : "posteriolateral protocerebrum (synonym)"
				}, {
					"id" : "FBbt:00040044",
					"text" : "posterior-lateral protocerebrum (synonym)"
				}, {
					"id" : "FBbt:00040044",
					"text" : "posteriorlateral protocerebrum (synonym)"
				}, {
					"id" : "FBbt:00040056",
					"text" : "posterior maxillary sensory center"
				}, {
					"id" : "FBbt:00040056",
					"text" : "PMS (synonym)"
				}, {
					"id" : "FBbt:00040072",
					"text" : "posterior slope"
				}, {
					"id" : "FBbt:00040072",
					"text" : "PS (synonym)"
				}, {
					"id" : "FBbt:00040046",
					"text" : "posterior superior lateral protocerebrum"
				}, {
					"id" : "FBbt:00040046",
					"text" : "posterior SLP (synonym)"
				}, {
					"id" : "FBbt:00040046",
					"text" : "posterior splr (synonym)"
				}, {
					"id" : "FBbt:00040046",
					"text" : "PSLP (synonym)"
				}, {
					"id" : "FBbt:00040046",
					"text" : "pslpr (synonym)"
				}, {
					"id" : "FBbt:00040063",
					"text" : "posterior superior medial protocerebrum"
				}, {
					"id" : "FBbt:00040063",
					"text" : "posterior SMP (synonym)"
				}, {
					"id" : "FBbt:00040063",
					"text" : "PSMP (synonym)"
				}, {
					"id" : "FBbt:00040042",
					"text" : "posterior ventrolateral protocerebrum"
				}, {
					"id" : "FBbt:00040042",
					"text" : "posterior VLP (synonym)"
				}, {
					"id" : "FBbt:00040042",
					"text" : "PVLP (synonym)"
				}, {
					"id" : "FBbt:00003668",
					"text" : "protocerebral bridge"
				}, {
					"id" : "FBbt:00003668",
					"text" : "pb (synonym)"
				}, {
					"id" : "FBbt:00003668",
					"text" : "PB (synonym)"
				}, {
					"id" : "FBbt:00003669",
					"text" : "protocerebral bridge glomerulus"
				}, {
					"id" : "FBbt:00003670",
					"text" : "protocerebral bridge glomerulus 1"
				}, {
					"id" : "FBbt:00003671",
					"text" : "protocerebral bridge glomerulus 2"
				}, {
					"id" : "FBbt:00003672",
					"text" : "protocerebral bridge glomerulus 3"
				}, {
					"id" : "FBbt:00003673",
					"text" : "protocerebral bridge glomerulus 4"
				}, {
					"id" : "FBbt:00003674",
					"text" : "protocerebral bridge glomerulus 5"
				}, {
					"id" : "FBbt:00003675",
					"text" : "protocerebral bridge glomerulus 6"
				}, {
					"id" : "FBbt:00003676",
					"text" : "protocerebral bridge glomerulus 7"
				}, {
					"id" : "FBbt:00003677",
					"text" : "protocerebral bridge glomerulus 8"
				}, {
					"id" : "FBbt:00040051",
					"text" : "prow"
				}, {
					"id" : "FBbt:00040051",
					"text" : "PRW (synonym)"
				}, {
					"id" : "FBbt:00040038",
					"text" : "rubus"
				}, {
					"id" : "FBbt:00040038",
					"text" : "RUB (synonym)"
				}, {
					"id" : "FBbt:00045048",
					"text" : "saddle"
				}, {
					"id" : "FBbt:00045048",
					"text" : "SAD (synonym)"
				}, {
					"id" : "FBbt:00040036",
					"text" : "segment of fan-shaped body"
				}, {
					"id" : "FBbt:00040036",
					"text" : "stave of fan-shaped body (synonym)"
				}, {
					"id" : "FBbt:00040036",
					"text" : "vertical column of fan-shaped body (synonym)"
				}, {
					"id" : "FBbt:00003756",
					"text" : "serpentine layer"
				}, {
					"id" : "FBbt:00003756",
					"text" : "medulla layer 7 (synonym)"
				}, {
					"id" : "FBbt:00003756",
					"text" : "medulla layer M7 (synonym)"
				}, {
					"id" : "FBbt:00003756",
					"text" : "SPL (synonym)"
				}, {
					"id" : "FBbt:00100363",
					"text" : "superior bulb"
				}, {
					"id" : "FBbt:00040048",
					"text" : "superior clamp"
				}, {
					"id" : "FBbt:00040048",
					"text" : "SCL (synonym)"
				}, {
					"id" : "FBbt:00045032",
					"text" : "superior intermediate protocerebrum"
				}, {
					"id" : "FBbt:00045032",
					"text" : "SIP (synonym)"
				}, {
					"id" : "FBbt:00007054",
					"text" : "superior lateral protocerebrum"
				}, {
					"id" : "FBbt:00007054",
					"text" : "adult superior lateral protocerebrum (synonym)"
				}, {
					"id" : "FBbt:00007054",
					"text" : "SLP (synonym)"
				}, {
					"id" : "FBbt:00007054",
					"text" : "slpr (synonym)"
				}, {
					"id" : "FBbt:00007055",
					"text" : "superior medial protocerebrum"
				}, {
					"id" : "FBbt:00007055",
					"text" : "SMP (synonym)"
				}, {
					"id" : "FBbt:00007055",
					"text" : "smpr (synonym)"
				}, {
					"id" : "FBbt:00045030",
					"text" : "superior neuropils"
				}, {
					"id" : "FBbt:00045030",
					"text" : "SNP (synonym)"
				}, {
					"id" : "FBbt:00040052",
					"text" : "superior pharyngeal sensory center"
				}, {
					"id" : "FBbt:00040052",
					"text" : "dorsal pharyngeal sensory center (synonym)"
				}, {
					"id" : "FBbt:00040052",
					"text" : "SPhS (synonym)"
				}, {
					"id" : "FBbt:00045040",
					"text" : "superior posterior slope"
				}, {
					"id" : "FBbt:00045040",
					"text" : "SPS (synonym)"
				}, {
					"id" : "FBbt:00003626",
					"text" : "supraesophageal ganglion"
				}, {
					"id" : "FBbt:00004015",
					"text" : "taste-sensory glomerulus DA"
				}, {
					"id" : "FBbt:00004016",
					"text" : "taste-sensory glomerulus M"
				}, {
					"id" : "FBbt:00004017",
					"text" : "taste-sensory glomerulus VL"
				}, {
					"id" : "FBbt:00004018",
					"text" : "taste-sensory glomerulus VM"
				}, {
					"id" : "FBbt:00004014",
					"text" : "taste-sensory region glomerulus"
				}, {
					"id" : "FBbt:00004014",
					"text" : "labellar gustatory association centre (synonym)"
				}, {
					"id" : "FBbt:00040059",
					"text" : "upper lateral accessory lobe"
				}, {
					"id" : "FBbt:00040059",
					"text" : "ULAL (synonym)"
				}, {
					"id" : "FBbt:00040071",
					"text" : "ventral complex"
				}, {
					"id" : "FBbt:00040071",
					"text" : "VX (synonym)"
				}, {
					"id" : "FBbt:00045021",
					"text" : "ventrolateral neuropils"
				}, {
					"id" : "FBbt:00045021",
					"text" : "VLNP (synonym)"
				}, {
					"id" : "FBbt:00007058",
					"text" : "ventrolateral protocerebrum"
				}, {
					"id" : "FBbt:00007058",
					"text" : "ventro-lateral adult protocerebrum (synonym)"
				}, {
					"id" : "FBbt:00007058",
					"text" : "ventro-lateral protocerebrum (synonym)"
				}, {
					"id" : "FBbt:00007058",
					"text" : "VLP (synonym)"
				}, {
					"id" : "FBbt:00007058",
					"text" : "vlpr (synonym)"
				}, {
					"id" : "FBbt:00040002",
					"text" : "ventromedial neuropils"
				}, {
					"id" : "FBbt:00040002",
					"text" : "VMNPS (synonym)"
				}, {
					"id" : "FBbt:00040041",
					"text" : "vest"
				}, {
					"id" : "FBbt:00040041",
					"text" : "VES (synonym)"
				} ];
		console.log("before before jso");
		jso = {
			"modelDataUrl" : "<%=request.getContextPath()%>/js/tiledImageModelData.jso"
		};
		console.log("before init");
		emouseatlas.emap.tiledImageModel.initialise(jso);
		console.log("after init");
	}

	window.addEvent('domready', function() {
		console.log("dom ready");
		doOnload();
	});

</script>

</head>

<body>

	<div id="wrapper">

  

<script type="text/javascript">
	//Show/hide the recent query link depending on the cooke value
	window.addEvent('domready', function() {
		toggleResultLink();
	});
</script>



 	<div id="head_wrapper">
 		
 		<table style="width:100%">
 		<tr>
 		<td width="180" height="90">
 			<div id="header">
 			
  			<a href="<%=jsPathVFB %>/site/vfb_site/overview.htm" title="Home">
  				<img id="logo" src= "<%=jsPathVFB %>/images/vfb/flyBrain.gif" alt="Brain" width="180" height="90" border="0"<%=jsPathVFB %>/>
  			</a>
  		</td>
  		<td>
  			
  			
  				<a class="help smoothbox" href="./help.htm?height=600&width=800&TB_iframe=true" style="top:-18px" title="Quick help">&nbsp;Help</a>
  				<a class="help youtube" href="http://www.youtube.com/playlist?list=PL8E3BDD1BA565B4FD" target="_new" style="top:-18px" title="Watch VFB tutorial videos">Tutorial Videos</a>
			
			   		
  			<h1 id="page_header"><a href="<%=jsPathVFB %>/site/vfb_site/overview.htm">Virtual Fly Brain</a>:&nbsp;Adult Brain Stack</h1>  			
  			<div id="breadcrumb" >
			
				<b>Your trail: </b>
				
					
					
					
						
							<a href="<%=jsPathVFB %>/site/vfb_site/overview.htm">The VFB Site</a>
						
						
					
											
				
					
					>
					
						
						
							Stacks
						
					
											
				
					
					>
					
					
						<b>Adult Brain Stack</b>
											
				
			
  			</div>
  		</div><!-- header -->

		<div id="menuwrapper"
			style="z-index: 100; position: relative; top: 32px;">
		<ul id="p7menubar">

			<li><a class="trigger" href="#">The VFB Site</a>
			<ul>
				<li><a href="<%=jsPathVFB %>/site/vfb_site/overview.htm">Overview</a></li>
				<li><a href="<%=jsPathVFB %>/site/vfb_site/features.htm">Features</a></li>
				<li><a href="<%=jsPathVFB %>/site/vfb_site/tutorial.htm">Tutorials</a></li>
				<li><a href="<%=jsPathVFB %>/site/vfb_site/releases.htm">Releases</a></li>
				<li><a href="<%=jsPathVFB %>/site/vfb_site/sitemap.htm">Sitemap</a></li>
				<li><a href="<%=jsPathVFB %>/site/vfb_site/privacy_cookies.htm">Privacy and Cookies</a></li>
				<li><a href="<%=jsPathVFB %>/site/vfb_site/about_us.htm">About Us</a></li>
			</ul>
			</li>
			<li><a class="trigger" href="#">Tools</a>
			<ul>
				<li><a href="<%=jsPathVFB %>/site/vfb_site/browserCheck.htm?url=/site/tools/query_builder/">Query Builder</a></li>
				<!-- li><a href="<%=jsPathVFB %>/site/tools/neuron_finder/">Neuron Finder</a></li-->
				<li><a href="<%=jsPathVFB %>/site/tools/anatomy_finder/">Anatomy/Neuron Finder</a></li>
				<li><a href="<%=jsPathVFB %>/site/vfb_site/inProgress.htm" onclick="alert('We are sorry. This feature development is in progress'); return false">Upload Your Stack</a></li>
				<li><a href="<%=jsPathVFB %>/site/vfb_site/inProgress.htm" onclick="alert('We are sorry. This feature development is in progress'); return false">Annotation Tool</a></li>
				<li><a href="<%=jsPathVFB %>/site/vfb_site/inProgress.htm" onclick="alert('We are sorry. This feature development is in progress'); return false">Software Downloads</a></li>
				<li><a href="<%=jsPathVFB %>/site/tools/protected/index.htm">Protected Area</a></li>
			</ul>
			</li>
			<li><a class="trigger" href="#">Stacks</a>
			<ul>
				<li><a href="<%=jsPathVFB %>/site/stacks/index.htm">Adult Brain Stack</a></li>
			</ul>
			</li>
			
				<li><a class="trigger" href="#">Screen Resolution</a>
				<ul>
					<li><a href="<%=jsPathVFB %>/site/stacks/index.htm?res=0">Best Fit</a></li>
					<li><a href="<%=jsPathVFB %>/site/stacks/index.htm?res=1">Low: 960 x 600</a></li>
					<li><a href="<%=jsPathVFB %>/site/stacks/index.htm?res=2">High: 1960 x 1200</a></li>
				</ul>
				</li>
			
		</ul>
		
			<a href="<%=jsPathVFB %>/site/stacks/recentQuery.jsp" id="recent_query" title="Click to bring back your recent query results window"
				onClick='fireLightBox("<%=jsPathVFB %>/site/stacks/recentQuery.jsp?popup=true", 600, 800); return false;' class="recent_query_link">Recent query</a>
		
		<br class="clearit">
		</div>
		</td>  			
			</div><!-- header -->
			
		</td>
		</tr>				
		</table>				  			
	  	</div> <!-- head_wrapper -->  	
    <div style="clear:both;"></div>	  	
 	 
  	
	<div id="body_wrapper">
		<div id="left_panel">
			<div id="toolContainerDiv">
				
  


<div style="position:absolute; bottom: 0px; height:auto; width:96%; font-size:.9em; padding:0 4px;" >
	<a class="help smoothbox" style="margin-top:0px" href="<%=jsPathVFB %>/site/credits.htm?height=600&width=600&stackInfo=Template data painted with BrainName standard |Arnim Jenett (Janelia Farm Research Campus), Kazunori Shinomiya and Kei Ito(Tokyo University)| |http://www.flybase.net/reports/FBgn0259246.html|Nc82&TB_iframe=true" >&nbsp;full info</a>
	<b>Stack Info:</b><br/>Template data by Arnim Jenett (Janelia Farm Research Campus), Kazunori Shinomiya and Kei Ito (Tokyo University) <br/>
</div>


			</div>
		</div>
	    
	    <div id="center_panel">
			<div id="emapIIPViewerDiv">
			</div>	 
		</div>
		
		<div id="right_panel"> 
		</div> <!--right_panel -->			
		
		<div id="annotation">
			<span class="mif-tree-name" style="z-index:100; float:right; width:240px;">
				<a class="help triangle-down" href="#" title="Right click here for term-specific options">&nbsp;Right/ctrl click for queries</a>
			</span>
			<!-- Need this to get the context menu on the term info box working, see span above -->
			<div id="annotation_content">
				<h2 class="panel_header">Annotation for Selected Node</h2>
			</div>
		</div>
 	
 	</div> <!--body_wrapper -->
		<br clear="all"<%=jsPathVFB %>/>
	<div id="cellar">
	  | <a href="<%=jsPathVFB %>/site/vfb_site/sitemap.htm">Sitemap</a> |
	  <a href="<%=jsPathVFB %>/">Virtual Fly Brain</a> |
	  <script>mail2("support","virtualflybrain",1,"","Contact us")</script> |
	  <a href="<%=jsPathVFB %>/site/vfb_site/privacy_cookies.htm">Privacy and Cookies</a> |
	</div>

 </div> <!-- wrapper -->
 
  	<!--script type="text/javascript">
		var vertical=new Splitter($("splitter_wrapper"),{orientation:1});
		vertical.addWidget($("search"),{minimumSize:50});
		vertical.addWidget($("tree_container"),{minimumSize:50});
		vertical.setSizes([90,570]);
	</script-->
			
	<!-- Context menu for tree nodes --> 	
	<ul id="contextmenu">
		<!-- onClick="openInfoWindow('query.php?action=innervate');return false;" -->
		<li style="padding: 5px 10px 5px 15px;">Neurons with:</li>
		<li><a name="found" class="cut"  target="_blank">. some part here</a></li>
		<li><a name="synaptic" class="cut">. . synaptic terminals here</a></li>
		<li><a name="presynaptic" class="cut">. . . presynaptic terminals here</a></li>
		<li><a name="postsynaptic" class="cut">. . . postsynaptic terminals here </a></li>
		<li class="separator"><a name="tract" class="cut">Tracts/nerves innervating here</a></li>
		<li class="separator"><a name="transgene" class="cut">Transgenes expressed here</a></li>
		<li><a name="geneex" class="paste">Genes expressed here</a></li>
		<li><a name="phenotype" class="paste">Phenotypes here</a></li>
		<li class="separator"><a name="quit" class="quit">Cancel</a></li>
	</ul>

   </body> 

</html>
