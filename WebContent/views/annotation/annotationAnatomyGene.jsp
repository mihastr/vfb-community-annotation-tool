
<%@page import="com.vfb.socl.entity.Person"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="com.vfb.socl.entity.Annotation"%>
<%@page import="java.util.Arrays"%>
<%@page import="com.vfb.socl.mappers.AnnotationMapper"%>
<%@page import="com.vfb.socl.util.DB"%>
<%@page import="java.util.List"%>
<%@page import="java.util.Random"%>
<%@page import="com.google.gson.Gson"%>
<%@page import="java.util.Map"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%
	//Gson g = new Gson();
	//response.getWriter().write(g.toJson(request.getParameterMap()));
	String structureId = request.getParameter("structureId");
	Person user = (Person)session.getAttribute("user");
	String pathToNode = request.getParameter("pathToNode");
	System.out.println("structureId: " + structureId);
	String geneId = request.getParameter("geneId");
	if (geneId==null)  {
		geneId = "CaMKII";
	}
			
	
%>
<h3 style="text-transform: capitalize"><%=request.getParameter("name")%>
	(<%=structureId%>)
</h3>
<h4>For gene: <%=geneId %></h4>

<script type="text/javascript">
	var pathToNode = <%=pathToNode %>;
	window.addEvent('domready', function() {
		
		/*
		$('cbList').hide();
		$('cbBtn').addEvent('click', function(event) {
			event.stop();
			openedIds.push('cbList');
			console.log(openedIds);
			$('cbList').show();
		});
		
		$$('#cbList input[name="expLevels"]').addEvent('click',
						function(event) {
							console.log("**** start **********");
							var lst = [];
							$$('#cbList input[name="expLevels"]').each(
									function(element, index) {
										console.log(element.value + " "
												+ element.checked);
										if (element.checked) {
											lst.push(element.value);
										}
									});
							$('expLevelsSelected').innerHTML = lst.length
									+ " selected"; //lst.join(', ');
							console.log("**** done ***********");
						});
		*/
		/*
		$('newAnnotationForm').hide();
		$$('#newAnnotationBtn a').addEvent('click', function(event) {
			event.stop();
			$('newAnnotationForm').show();
			new Fx.Slide($('newAnnotationForm'), {
				duration : 500,
				onComplete : function() {
					//$(this).getParent()
				}
			}).slideIn();
		});

		$$('div.close_btn').addEvent('click', function(event) {
			//$(this).getParent().hide();
			event.stop();
			new Fx.Slide($(this).getParent(), {
				duration : 500,
				onComplete : function() {
					$('newAnnotationForm').hide();
				}
			}).slideOut(); // Slide it!
		});
		new Picker.Date($('anDate'), {
			timePicker : false,
			positionOffset : {
				x : 5,
				y : 0
			},
			pickerClass : 'datepicker_dashboard',
			useFadeInOut : !Browser.ie
		});
		*/
		
		
		// activate images mouseover effect
		function makeRollable() {
			$$("img.mo").each(function(element, index) {
				var prev_src = null;
				var himage = null;
				element.addEvent('mouseenter', function(event) {
					//var is_selected = element.hasClass("is_selected");
					//console.log(is_selected);
					//if (!is_selected) {
						prev_src = element.getProperty('src');
						himage = element.getProperty('himage');
						element.setProperty('src', himage);
						element.setProperty('prev_image', prev_src);
					//}
				});
				element.addEvent('mouseleave', function(event) {
					//var is_selected = element.hasClass("is_selected");
					//console.log(is_selected);
					//if (!is_selected) {
						prev_src = element.getProperty('prev_image');
						element.setProperty('src', prev_src);
					//}
				});
			});
		}
		//makeRollable();

		function toggleSelection(obj) {
			curr_image = obj.get('src');
			prev_image = obj.get('prev_image');
			obj.setProperty('prev_image', curr_image);
			obj.setProperty('src', prev_image);
		}
		
		modal = new FbModal({
			'parentEl' : 'mainDiv',
			'title' : 'Experiment',
			'subTitle' : 'Details which explain annotation',
			'content' : '',
			'height' : 600,
			'width' : 800,
		});

		$('loading').hide();
		var req = new Request.HTML({
			method : 'get',
			update : $('ModalContent'),
			onRequest : function() {
				$('loading').show();
			},
			onSuccess : function(responseTree, responseElements, responseHTML,
					responseJavaScript) {
				$('loading').hide();
				modal.options.content = responseHTML;
				modal.drawModal();

			}
		});
		function loadDetailData(url, data) {
			req.cancel();
			req.options.url = url;
			if (!(data == undefined))
				req.get(data);
			else
				req.get();
		}
		
		$$('a.listDetailBtn').addEvent('click', function(event) {
			event.stop();
			//console.log($(this).getParent());
			var rid = $(this).getParent().getParent().getAttribute("rid");
			loadDetailData("views/annotation/experiment_detail.jsp", {
				'id' : rid
			});
		});

		$$('a.listDiscussBtn').addEvent('click', function(event) {
			event.stop();
			//console.log($(this).getParent());
			var rid = $(this).getParent().getParent().getAttribute("rid");
			loadDetailData("views/message/newMessage.jsp", {
				'tid' : parseInt(rid)+1,
				'taWidth' : 725
			});
		});
		
		
		
		var reqDoAnnotation = new Request.JSON({
			url: 'do_annotations?action=vote',
			update : $('contentDiv'),
			onRequest : function() {
				$('loading').show();
			},
			onSuccess : function(requestJSON, requestTxt) {
				console.log(requestJSON);
				//loadAnnotations(requestJSON.message);
				//loadAnnotations(pathToNode[pathToNode.length-1]);
				//console.log("pathToNode " + pathToNode[pathToNode.length-1]);
				$('loading').hide();
			}
		});

		$$('div.annRowExpanded').addEvent('click', function(event) {
			this.addClass('annRow');
			this.removeClass('annRowExpanded');
			var entryDetail = this.getElement('div.entry_detail');
			entryDetail.addClass('entry_detail_hidden');
			entryDetail.removeClass('entry_detail');
		});

		//http://localhost:8080/VFBAnnotation/do_annotations?action=getStats&structureId=FBbt:00100365&geneId=CaMKII&strength=weak
		var reqDoAnnotationStats = new Request.JSON({
			url: 'do_annotations?action=getStats',
			onRequest : function() {
				$('loading').show();
			},
			onSuccess : function(requestJSON, requestTxt) {
				$('loading').hide();
				if (requestJSON.error!="no") return;
				if (requestJSON.message.length==0) return;
				//loadAnnotations(requestJSON.message.nodeId);
				console.log(requestJSON);
				//{"nodeId":0,"annotationId":62,"isSubmission":true,"sumOfVotes":0,"nUp":0,"nDown":0,"vote":1,"isFlagged":false,"username":"miha","isOnline":false,"isOnXMPP":false,"email4newsletter":false,"isAFriend":false}

				var submissions = [];
				var pros = [];
				var cons = [];

				for (var i=requestJSON.message.length-1; i>=0; i--) {
					var o = requestJSON.message[i];
					if (o.isSubmission) {
						submissions.push("<a href='#user/profile/" + o.username + "' class='userProfileBtn'>" + o.username + "</a>");	
					} else {
						if (o.vote>0) {
							pros.push("<a href='#user/profile/" + o.username + "' class='userProfileBtn'>" + o.username + "</a>");
						} else if (o.vote<0) {
							cons.push("<a href='#user/profile/" + o.username + "' class='userProfileBtn'>" + o.username + "</a>");
						}
					}
				}
				
				var text = "<div><div style='float: left; width: 200px;'>submitted: <br />" + submissions.join(", ") + "</div>" + 
				"<div style='float: left; width: 200px;'>voted for:<br />" + pros.join(", ") + "</div>" +
				"<div style='float: left; width: 200px;'>voded against: <br />" + cons.join(", ") + "</div><div class='clear'> </div></div>";
				
				$$('div.annRow[rid='+requestJSON.message[0].annotationId+'] div.entry_detail').set('html', text);
				$$('div.annRow[rid='+requestJSON.message[0].annotationId+'] div.entry_detail').set('style', 'height: ' + (requestJSON.message.length * 12 + 40) + "px");
				
				$$("a.userProfileBtn").addEvent(
						'click',
						function(event) {
							event.stop();
							var link = $(this).getAttribute("href").replace("#", "").split("/");
							var path = "views/" + link[0] + "/" + link[1] + ".jsp?id=" + link[2];
							loadURL(path);
						});
				
			}
		});
		$$('div.annRow').addEvent('click', function(event) {
			if (this.hasClass("is_expanded")) {
				this.removeClass('annRowExpanded');
				var entryDetail = this.getElement('div.entry_detail');
				entryDetail.addClass('entry_detail_hidden');
				this.removeClass("is_expanded");
			} else {
				var annotationId = this.get("rid");
				reqDoAnnotationStats.get({
					'annotationId': annotationId
				});
				//structureId=FBbt:00100365&geneId=CaMKII&strength=weak
				
				this.addClass("is_expanded");
				this.addClass('annRowExpanded');
				var entryDetail = this.getElement('div.entry_detail_hidden');
				entryDetail.addClass('entry_detail');
				entryDetail.removeClass('entry_detail_hidden');
				
				var nid = this.get("nid").split("/");
				console.log(nodesInfo[nid[nid.length-1]].name);				
			}
		});

		
		function storeVote(annotationId, vote, nodeId) {
			reqDoAnnotation.cancel();
			reqDoAnnotation.get({'annotationId': annotationId, 'vote': vote, 'nodeId': nodeId});
		}

		$$('a.listThumbDownBtn').addEvent(
				'click',
				function(event) {
					event.stop();
					
					var rid = $(this).getParent().getParent().get("rid");
					var nid = $(this).getParent().getParent().get("nid");
					if (nid!=undefined && nid!=null) {
						nid = nid.split("/");
						nid = nid[nid.length-1];
					}
					//console.log(rid);
					
					var thmbsDownBtn = $(this).getParent().getElement("a.listThumbDownBtn img");
					var thmbsUpBtn = $(this).getParent().getElement("a.listThumbUpBtn img");
					
					if (thmbsDownBtn.hasClass("is_selected")) {
						storeVote(rid, 0, nid);	
						thmbsDownBtn.removeClass("is_selected");
					} else {
						storeVote(rid, -1, nid);
						thmbsDownBtn.addClass("is_selected");
					}
					
					if (thmbsUpBtn.hasClass("is_selected")) {
						toggleSelection(thmbsUpBtn);
						thmbsUpBtn.removeClass("is_selected");
					}
					toggleSelection(thmbsDownBtn);
					
				});


		$$('a.listThumbUpBtn').addEvent('click',
				function(event) {

					event.stop();
					var rid = $(this).getParent().getParent().get("rid");
					var nid = $(this).getParent().getParent().get("nid");
					if (nid!=undefined && nid!=null) {
						nid = nid.split("/");
						nid = nid[nid.length-1];
					}

					var thmbsUpBtn = $(this).getParent().getElement("a.listThumbUpBtn img");
					var thmbsDownBtn = $(this).getParent().getElement("a.listThumbDownBtn img");
					
					if (thmbsUpBtn.hasClass("is_selected")) {
						storeVote(rid, 0, nid);	
						thmbsUpBtn.removeClass("is_selected");
					} else {
						storeVote(rid, 1, nid);
						thmbsUpBtn.addClass("is_selected");
					}
					
					if (thmbsDownBtn.hasClass("is_selected")) {
						toggleSelection(thmbsDownBtn);
						thmbsDownBtn.removeClass("is_selected");
					}
					toggleSelection(thmbsUpBtn);
				});

		var reqDoAnnotatioFlag = new Request.JSON({
			url: 'do_annotations?action=flag',
			onRequest : function() {
				$('loading').show();
			},
			onSuccess : function(requestJSON, requestTxt) {
				$('loading').hide();
			}
		});
		function storeFlag(annotationId, isFlagged) {
			reqDoAnnotatioFlag.cancel();
			reqDoAnnotatioFlag.get({'annotationId': annotationId, 'isFlagged': isFlagged});
		}
		
		
		$$('a.listFlagBtn').addEvent('click', function(event) {
			event.stop();
			var rid = $(this).getParent().getParent().getAttribute("rid");
			var listFlagBtn = $(this).getElement("img");
			
			if (listFlagBtn.hasClass("is_selected")) {
				storeFlag(rid, false);	
				listFlagBtn.removeClass("is_selected");
			} else {
				storeFlag(rid, true);
				listFlagBtn.addClass("is_selected");
			}
			toggleSelection(listFlagBtn);
		});

		
		$$("a.userProfileBtn").addEvent(
				'click',
				function(event) {
					event.stop();
					var link = $(this).getAttribute("href").replace("#", "").split("/");
					var path = "views/" + link[0] + "/" + link[1] + ".jsp?id=" + link[2];
					loadURL(path);
				});

		$('loading').hide();
		var reqContent = new Request.HTML({
			method : 'get',
			update : $('contentDiv'),
			onRequest : function() {
				$('loading').show();
			},
			onSuccess : function() {
				$('loading').hide();
			}
		});
		function loadURL(url) {
			reqContent.cancel();
			reqContent.options.url = url;
			reqContent.send();
		}

		$$('a.geneListStructureBtn').addEvent('click', function(event) {
			var pathToNode = this.get('href').split("#")[1].split("/");
			var nodeId = parseInt(pathToNode[pathToNode.length-1])
			d.openTo(nodeId, true);
			loadAnnotations(nodeId);
			event.stop();
		});

		$$('div.annRow').each(function(element, index) {
			var nid = element.get('nid');
			if (nid!=null) {
				pathToNode = nid.split("/");
				var nodeId = parseInt(pathToNode[pathToNode.length-1]);
				element.getElement("div.structure").set("title", nodesInfo[nodeId]["name"]);
			}			
		});
		// tips to add information 
		/* Tips 2 */
		//var Tips2 = new Tips($$('.tips'));
		Tips2.attach($$('.tips'));
		
	});
</script>
<div id="submittedAnnotations">
	<h4>List of Annotations</h4>
	
	<div id="anaotmy_annotationHeader"></div>
	
	
	<div id="anaotmy_annotation">
		<div class="annRow">
			<div class="expLvlSquare">&nbsp;</div>
			<div class="name">User</div>
			<div class="date">Date</div>
			<div class="expLvl">Strength</div>
			<div class="pattern">Pattern</div>
			<div class="gene">Gene</div>
			<div class="structure">Structure</div>
			<div class="modified">&nbsp;</div>
			<div class="actions">&nbsp;</div>
			<div style="clear"></div>
		</div>		
		<%
		DB db = new DB();
		AnnotationMapper am = db.getAnnotationMapper();
		// update annotations count
		
		if (am.updateViewedAnnotations(user.username)==0) {
			am.createViewedAnnotationsField(user.username);
		}
		
		
		//List<Annotation> annotations = am.getAnnotationByStructure(geneId, structureId);
		List<Annotation> annotations = am.getAnnotationByPath(geneId, pathToNode, user.username);
		Long discussionBoard = am.getAnnotationDiscussionBoard(geneId, pathToNode);
		if (discussionBoard==null) {
			discussionBoard = Long.valueOf(0);
		}
		SimpleDateFormat dateFormater = new SimpleDateFormat("dd/MM/yy");
				
		Annotation prevAnnotation=new Annotation();
		if (annotations!=null)
		for (Annotation a: annotations) {
			String dateSubmitted = dateFormater.format(a.getDateSubmited());
			if (a.similarTo(prevAnnotation)) continue;
			int percentUp = (int)((((float)a.getnUp())/(a.getnUp()+a.getnDown()))*30);
			int percentDown = 30-percentUp;
			System.out.println(a.getnUp() + " " + a.getnDown() + " " + percentUp + " " + percentDown);
		%>
		<div nid=<%=a.getPathToNode() %> rid="<%=a.getAnnotationId()%>" class="annRow <% if (!a.getStructureId().equals(prevAnnotation.getStructureId())) { %>separator<% } %>">
			<div class="expLvlSquare <%=a.getStrength()%>">&nbsp;</div>
			<div class="name">
				submitted by: <%=a.getnSubmissions()%>
			</div>
			<div class="date"><%=dateSubmitted %></div>
			<div class="expLvl"><%=a.getStrength() %></div>
			<div class="pattern"><%=a.getPattern() %></div>
			<div class="gene"><a href="#<%=a.getPathToNode() %>" class="geneListStructureBtn"><%=a.getGeneId() %></a></div>
			<div class="structure tips"><a href="#<%=a.getPathToNode() %>" class="geneListStructureBtn"><%=a.getStructureId() %></a></div>
			<div class="modified">
				<div style="background-color: green; float:left; height:14px; width: <%=percentUp %>px">&nbsp;</div>
				<div style="background-color: red; float:left; height:14px; width: <%=percentDown %>px">&nbsp;</div>
				<div class="clear"></div>
			</div>
			<div class="actions">
			<% if (!a.isSubmission()) { %>
				<a href="#" class="listThumbUpBtn">
					<% if (a.hasThumbsUp()) { %>
					<img src="images/thumbs_up_light_green.png" prev_image="images/thumbs_up_dark_grey.png" class="is_selected" />
					<% } else { %>
					<img src="images/thumbs_up_dark_grey.png" prev_image="images/thumbs_up_light_green.png"  />
					<% } %>
				</a> <a href="#" class="listThumbDownBtn">
				 <% if (a.hasThumbsDown()) { %>
					<img src="images/thumbs_down_light_red.png" prev_image="images/thumbs_down_dark_grey.png" class="is_selected" />
					<% } else { %>
					<img src="images/thumbs_down_dark_grey.png" prev_image="images/thumbs_down_light_red.png" />
					<% } %>
				</a>
				<% }  %>
				<!-- 
				 <a href="#" class="listDiscussBtn">
					<img src="images/widget-forum.png" class="mo" himage="images/widget-forum_active.png" />
				</a>
				 -->
				<% if (a.isFlagged()) { %>
					 <a href="#" class="listFlagBtn"><img src="images/icon_red_flag.gif" prev_image="images/icon_gray_flag.gif" class="is_selected" /></a>
				<% } else { %>
					 <a href="#" class="listFlagBtn"><img src="images/icon_gray_flag.gif" prev_image="images/icon_red_flag.gif" /></a>
				<% } %>
					<!-- <a href="#"
					class="listDetailBtn"><img src="images/magnifying-glass.gif" /></a>-->
			</div>
			<div class='entry_detail_hidden'>&nbsp;</div>
			<!-- 
			<%=a.getComments() %> votes: <span class='nVotes'><%=a.getNumberOfVotes() %></span> (up: <span class='nVotesUp'><%=a.getnUp() %></span> down: <span class='nVotesDown'><%=a.getnDown() %></span>)
			 -->
			<div style="clear"></div>
		</div>
		<%
			prevAnnotation = a;
			}
		if (annotations==null || annotations.size()==0) {
			%><p>no submitted annotations</p><%
		}
		%>
	</div>
</div>

<div>
	<h4>Discussion</h4>

	<jsp:include page="../message/messages.jsp">
		<jsp:param value="500" name="taWidth" />
		<jsp:param value="<%=discussionBoard %>" name="tid" />
		<jsp:param value="annotation" name="owner" />
		<jsp:param value="<%=(geneId + '#' + pathToNode) %>" name="entityId" />
		<jsp:param value="public" name="visibility" />
	</jsp:include>
</div>

 