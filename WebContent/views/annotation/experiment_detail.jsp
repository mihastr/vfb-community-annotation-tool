<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<input type="hidden" value="<%=request.getParameter("id") %>" />
<div class="exDetailBox">
<h3>Experimental design</h3>
<textarea rows="3" cols="30"></textarea>
</div>


<div class="exDetailBox">
<h3>Biomaterials and treatments</h3>
<textarea rows="3" cols="30"></textarea>
</div>

<div class="exDetailBox">
<h3>Reporter (probe or antibody) information</h3>
<textarea rows="3" cols="30"></textarea>
</div>

<div class="exDetailBox">
<h3>Staining protocols and parameters</h3>
<textarea rows="3" cols="30"></textarea>
</div>

<div class="exDetailBox">
<h3>Imaging data and parameters</h3>
<textarea rows="3" cols="30"></textarea>
</div>

<div class="exDetailBox">
<h3>Image characterisations</h3>
<textarea rows="3" cols="30"></textarea>
</div>
