<%@page import="com.vfb.socl.entity.MessageThread"%>
<%@page import="com.vfb.socl.servlets.ProcessMessage"%>
<%@page import="com.vfb.socl.servlets.ResponseObject"%>
<%@page import="com.vfb.socl.entity.Annotation"%>
<%@page import="com.vfb.socl.servlets.ProcessAnnotation"%>
<%@page import="java.util.List"%>
<%@page import="com.vfb.socl.util.XMPPUtil"%>
<%@page import="com.vfb.socl.mappers.PersonMapper"%>
<%@page import="com.vfb.socl.entity.Person"%>
<%@page import="com.vfb.socl.util.DB"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" %>
<%
String profileId = request.getParameter("id");
if (profileId==null) profileId = request.getParameter("pid");
String loggedIn = (String)session.getAttribute("loggedIn");
Person user = null;
user = (Person)session.getAttribute("user");
if (user==null) user = new Person();
// load persons' profile
Person profile;
DB db = new DB();
if (profileId==null || profileId.equals(user.username)) {
	profile = user;
} else {
	PersonMapper pm = db.getPersonMapper(); 
	profile = pm.getPerson(profileId, user.username);
}

if (profile==null) {
	profile = user;
}
ProcessAnnotation pa = new ProcessAnnotation(profile);
ResponseObject roAnnotation = pa.getAnnotationsForUser(profile.getUsername());
List<Annotation> listOfSubmissions = (List<Annotation>)roAnnotation.message;

roAnnotation = pa.getAnnotationsVotesForUser(profile.getUsername());
List<Annotation> listOfVotes = (List<Annotation>)roAnnotation.message;

roAnnotation = pa.getAnnotationsFlagsForUser(profile.getUsername()); 
List<Annotation> listOfFlags = (List<Annotation>)roAnnotation.message;


ProcessMessage pm = new ProcessMessage(profile);
roAnnotation = pm.getDiscussionsForUser(profile.username);
List<MessageThread> listOfThreads = (List<MessageThread>)roAnnotation.message;
%> 

<script type="text/javascript">
	window.addEvent('domready', function() {
		
		$('loading').hide();
		var req = new Request.HTML({
			method : 'get',
			update : $('contentDiv'),
			onRequest : function() {
				$('loading').show();
			},
			onSuccess : function() {
				$('loading').hide();
			}
		});
		function loadURL(url) {
			req.cancel();
			req.options.url = url;
			req.send();
		}
		
		
		$$('div.connEntry a').each(function(element, index) {
			element.addEvent('click', function() {
				// GET http://localhost:8080/VFBAnnotation/views/user/profile.jsp?id=bd
				//click in #user/profile/bd
				var link = element.getAttribute("href").replace("#","").split("/");
				console.log(link);
				var path = "views/"+link[0]+"/"+link[1]+".jsp";
				//console.log(path);
				req.options.url = path;
				req.get({'pid': link[2]});
				console.log("click out");
			});
		});
		
		
		function initAutoComplete(data) {
			var ac = new Meio.Autocomplete($('connFilter'), data, {
				selectOnTab : true,
				onNoItemToList : function(elements) {
					//elements.field.node.highlight('#ff0000');
				},
				valueFilter: function(data){
					console.log(data);
			        return data.identifier;
			    },
				onSelect : function(elements, value) {
					console.log("selected: " + JSON.stringify(value));
					var path = "views/user/profile.jsp";
					req.options.url = path;
					req.get({'pid': value.username});
					window.location.href = "#user/profile/" + value.username; 
					console.log(path);
				},
				filter : {
					filter: function(text, data){
						console.log("filter: " + text);
						var txt = (data.firstName!=undefined ? data.firstName : data.username) + " " + (data.lastName!=undefined ? data.lastName : "");
						//console.log(txt);
						return true;
						//return text ? txt.trim().standardize().test(new RegExp(text.standardize().escapeRegExp(), 'i')) : true;
					},
					formatMatch: function(text, data, i){
						var txt = (data.firstName!=undefined ? data.firstName : data.username) + " " + (data.lastName!=undefined ? data.lastName : "");
						return txt.trim();
					},
					formatItem: function(text, data){
						var txt = (data.firstName!=undefined ? data.firstName : data.username) + " " + (data.lastName!=undefined ? data.lastName : "");
						return txt.trim();
					}					
					/*
					type : 'contains',
					path : 'firstName'
					*/
				}
			});
			//console.log("ac done");			
		}
		initAutoComplete('do_person?action=seekPerson');
		
		
		var reqDoPersonConnect = new Request.JSON({
			url: 'do_person?action=connectTo',
			onRequest : function() {
				//$('loading').show();
			},
			onSuccess : function(requestJSON, requestTxt) {
				if (requestJSON.error!="no") return;
				
			}
		});
		
		var reqDoPersonAcceptConnection = new Request.JSON({
			url: 'do_person?action=acceptConnectionRequest',
			onRequest : function() {
				//$('loading').show();
			},
			onSuccess : function(requestJSON, requestTxt) {
				if (requestJSON.error!="no") return;
				
			}
		});
				
		
		var connectWithMeBtn = $('connectWithMeBtn');
		if (connectWithMeBtn!=null && connectWithMeBtn!=undefined)
		connectWithMeBtn.addEvent('click', function(event) {
			reqDoPersonConnect.post({'username': '<%=profile.username%>', 'relationship': 'friend'});
			$('connectWithMeBtn').getParent().set('html', 'request sent');
			event.stop();
		});
		
		
		var acceptConnectionBtn = $$('a.acceptConnectionBtn');
		if (acceptConnectionBtn!=null && acceptConnectionBtn!=undefined)
		acceptConnectionBtn.addEvent('click', function(event) {
			reqDoPersonAcceptConnection.post({'username': this.get("username") });
			this.set('html', 'Ok thanks');
			event.stop();
		});
		
		reqDoPersonAcceptConnection
		
		var msgsMeBtn = $('messageMeBtn');
		if (msgsMeBtn!=null && msgsMeBtn!=undefined)
			msgsMeBtn.addEvent('click', function(event) {
				event.stop();
				var url = "views/message/messages.jsp?tid=0&to=<%=profile.username %>";
				loadURL(url);	
			});
		
		$$('a.viewAnnotationBtn').addEvent('click', function(event) {
			var ptn = this.get("href").split("/");
			var url = "views/annotation/annotations.jsp?nodeId=" + ptn[ptn.length-1];
			loadURL(url);	
		});
		$$('a.viewThreadBtn').addEvent('click', function(event) {
			var ptn = this.get("href").split("/");
			var url = "views/message/messages.jsp?tid=" + ptn[ptn.length-1];
			loadURL(url);		
		});		
		
	});
</script>

<div>
	<div id="personal_details" class="float_left">
		<h2>User Profile</h2>
		<div class="userProfile <%
			System.err.println("current profile: " + profile);
			if (profile.isOnline()!=null && profile.isOnline()) {
				%>on_line<%
			} else {
				%>off_line<%
			}
		%>">
			<img src="images/man.png" align="left" />
			<div class="row" style="width: 350px; height: 118px;">
				<div class="profEmail">
				<%
				if (user!=profile && !profile.isAFriend()) {
					%>
					:: <a href='#' id="connectWithMeBtn">Connect with me</a>
					<%
				} 
				%>
				</div>
				<div class="profName"><%=profile.toString() %> (<%=profile.username %>)</div>
				<div class="profEmail"><%=profile.getEmail() %></div>
				<div class="profInstitution"><%=profile.getInstitute() %></div>
				<div class="profPosition"><%=profile.getPosition() %></div>
				<!-- 
				<div class="profPosition"><%=profile.getLastUpdate() %></div>
				 -->
				<% if (profile.from!=null) { %>
				<div class="profFrom">From: <%=profile.getFrom() %></div>
				<%} %>
				<% if (profile.lives!=null) { %>
				<div  class="profLives">Lives: <%=profile.getLives() %></div>
				<%} %>
			</div>
			<div>
			<% if (profile!=user) { %>
			<a href="#message/threads/<%=profile.username %>" id="messageMeBtn"><img src="images/envelope.png" /></a> <% if (user.getXmpp().isOnline(String.format("%s@%s", profile.username, user.getXmpp().SERVER_URL))) { %>via client<% } %>
			<% } %>
			</div>
		</div>
	</div>
  <div>	
	<div class="float_left" style="width: 500px;">
		<h3>Connections</h3>
		<p><input type="text" id="connFilter"></p>
		<div id="profileConnections">
		<% 	if (profile.getConncections()!=null) {
				for (Person p: profile.getConncections().accepted) { 
					p.isOnXMPP = user.getXmpp().isOnline(String.format("%s@%s", p.username, user.getXmpp().SERVER_URL));					
					if (p.isOnline)	{	%>
				<div class="connEntry connOnline"><a href="#user/profile/<%=p.username%>"><%=p %> <% if (p.isOnXMPP) { %><img src="images/mobile_cue.gif" /><% } %></a></div>
			<% 		} else {  %>
				<div class="connEntry connOffline"><a href="#user/profile/<%=p.username%>"><%=p %> <% if (p.isOnXMPP) { %><img src="images/mobile_cue.gif" /><% } %></a></a></div>
			<%		} 
				}
			}%>

		</div>
	</div>
<% if (profile.equals(user)) { %>
	<div class="float_left" style="width: 500px;">
		<h3>Request to be accepted</h3>
		<div id="profileConnections">
		<% 	if (profile.getConncections()!=null) {
				for (Person p: profile.getConncections().pending) { 
					p.isOnXMPP = user.getXmpp().isOnline(String.format("%s@%s", p.username, user.getXmpp().SERVER_URL));					
					if (p.isOnline)	{	%>
				<div class="connEntry connOnline"><a href="#user/profile/<%=p.username%>"><%=p %> <% if (p.isOnXMPP) { %><img src="images/mobile_cue.gif" /><% } %></a> - <a href='#' class="acceptConnectionBtn" username="<%=p.getUsername() %>">accept</a></div>
			<% 		} else {  %>
				<div class="connEntry connOffline"><a href="#user/profile/<%=p.username%>"><%=p %> <% if (p.isOnXMPP) { %><img src="images/mobile_cue.gif" /><% } %></a> - <a href='#' class="acceptConnectionBtn" username="<%=p.getUsername() %>">accept</a></div>
			<%		} 
				}
				if (profile.getConncections().pending.size()==0) {
					%>
					<p>No requests</p>
					<%
				}
			}%>

		</div>
	</div>
	<div class="float_left" style="width: 500px;">
		<h3>Requests awaiting approval</h3>
		<div id="profileConnections">
		<% 	if (profile.getConncections()!=null) {
				for (Person p: profile.getConncections().requests) { 
					p.isOnXMPP = user.getXmpp().isOnline(String.format("%s@%s", p.username, user.getXmpp().SERVER_URL));					
					if (p.isOnline)	{	%>
				<div class="connEntry connOnline"><a href="#user/profile/<%=p.username%>"><%=p %> <% if (p.isOnXMPP) { %><img src="images/mobile_cue.gif" /><% } %></a></div>
			<% 		} else {  %>
				<div class="connEntry connOffline"><a href="#user/profile/<%=p.username%>"><%=p %> <% if (p.isOnXMPP) { %><img src="images/mobile_cue.gif" /><% } %></a></a></div>
			<%		} 
				}
				if (profile.getConncections().requests.size()==0) {
					%>
					<p>No requests</p>
					<%
				}
			}%>

		</div>
	</div>
<% } %>	
  </div>
	<div  class="float_left" style="width: 500px; margin-top: 20px;">
		<h3>Submitted the following annotations</h3>
		<ul>
		<%
		for (Annotation a: listOfSubmissions) {
			String[] ptn = a.getPathToNode().split("/");
		%>
		<li><a class='viewAnnotationBtn' href='#annotation/annotations/<%=ptn[ptn.length-1] %>'><%=a.getStructureId() %> / <%=a.getGeneId() %> -&gt; <%=a.getPattern() %> - <%=a.getStrength() %></a></li>
		<% } 
		if (listOfSubmissions.size()==0) {
			%><p>No submissions so far.</p><%
		} %>
		</ul>
	</div>
	
	<div  class="float_left" style="width: 500px; margin-top: 20px;">
		<h3>Flagged the following annotations.</h3>
		<ul>
		<%
		for (Annotation a: listOfFlags) {
			String[] ptn = a.getPathToNode().split("/");
		%>
		<li><a class='viewAnnotationBtn' href='#annotation/annotations/<%=ptn[ptn.length-1] %>'><%=a.getStructureId() %> / <%=a.getGeneId() %> -&gt; <%=a.getPattern() %> - <%=a.getStrength() %></a></li>
		<% } 
		if (listOfFlags.size()==0) {
			%><p>User has not flagged anything so far.</p><%
		} 
		%>
		</ul>		
	</div>

	<div  class="float_left" style="width: 500px; margin-top: 20px;">
		<h3>Voted on the following annotations</h3>
		<ul>
		<%
		for (Annotation a: listOfVotes) {
			String[] ptn = a.getPathToNode().split("/");
		%>
		<li><a class='viewAnnotationBtn' href='#annotation/annotations/<%=ptn[ptn.length-1] %>'><%=a.getStructureId() %> / <%=a.getGeneId() %> -&gt; <%=a.getPattern() %> - <%=a.getStrength() %>: <%
		 if (a.getVote()==1) {
			 %><img src='images/thumbs_up_light_green.png' /><%
		 } else {
			 %><img src='images/thumbs_down_light_red.png' /><%
		 }
		%></a></li>
		<% } 
		if (listOfVotes.size()==0) {
			%><p>Not voted on any annotation yet.</p><%
		} 
		%>
		</ul>		
	</div>
	<div  class="float_left" style="width: 900px; margin-top: 20px;">
		<h3>Participant in the following public threads</h3>
		<ul>
		<%
		for (MessageThread a: listOfThreads) {
		%>
		<li><a class='viewThreadBtn' href="#message/threads/<%=a.getConversationId() %>"><%=a.getEntityId() %> - last message from <b><%=a.getSenderName() %></b>: <%=a.getBody() %></a></li>
		<% } 
		if (listOfThreads.size()==0) {
			%><p>Not participated on any annotation yet.</p><%
		} 
		%>

		</ul>		
	</div>

	<div class="clear"></div>
	
</div>