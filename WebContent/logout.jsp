<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%
	session.setAttribute("loggedIn", null);
	session.invalidate();
	response.sendRedirect(request.getContextPath());
%>