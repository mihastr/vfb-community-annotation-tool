 <%@page import="com.vfb.socl.entity.Person"%>
<%@page import="com.vfb.socl.servlets.ProcessPerson"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%
boolean failedLogin = false;
String loggedIn = (String) session.getAttribute("loggedIn");
if (loggedIn != null) {
	//response.sendRedirect(request.getContextPath());
} else {
	//session.setAttribute("loggedIn", "ok");
	String username = request.getParameter("username");
	String password = request.getParameter("password");
	
	ProcessPerson pp = new ProcessPerson(session.getId(), request.getRemoteAddr(), username, password);
	if (pp.isLoggedIn()) {
		request.getSession().setAttribute("loggedIn", "ok");
		request.getSession().setAttribute("user", pp.getUser());
		session.setAttribute("loggedIn", "yes");
		response.sendRedirect(request.getContextPath());
	} 
	failedLogin = pp.isLoginError();
	
}
	
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link rel="stylesheet" href="css/style.css" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>VFB Community Annotation</title>
<script type="text/javascript"
	src="js/mootools-core-1.4.5-full-nocompat-yc.js"></script>
<script type="text/javascript" src="js/mootools-more-1.4.0.1.js"></script>
<style type="text/css">
form input[type='submit'] {
	border: thin solid #123;
	border-radius: 5px;
}

form label {
	font-size: 12px;
}

#loginForm {
	display: block;
	margin-left: auto;
	margin-right: auto;
	width: 380px;
	min-height: 50px;
	padding: 10px;
}
</style>
<script type="text/javascript">
	window.addEvent('domready', function() {
	});
</script>
</head>
<body>
	<div id="mainDiv">
		<h1>VFB Community Annotation Tool</h1>
		<div id="contentDiv">
			<div id="loginForm">
				<form method="post" id="theform">
					<table>
						<caption>Login / register form</caption>
						<tr>
							<%
								if (failedLogin) {
							%>
							<td colspan="2">Error logging in! Wrong login data! Try
								again</td>
						</tr>
						<%
							}
						%>
						<tr>
							<td align="right"><label for="username">Username:</label></td>
							<td><input name="username" type="text" /></td>
						</tr>
						<tr>
							<td align="right"><label for="password">Password:</label></td>
							<td><input name="password" type="password" /></td>
						</tr>
						<tr>
							<td>&nbsp;</td>
							<td><input type="submit" value="login" /></td>
						</tr>
					</table>
				</form>
			</div>
		</div>
	</div>
</body>
</html>