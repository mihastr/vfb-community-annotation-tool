<%@page import="com.vfb.socl.mappers.ActiveSessionsMapper"%>
<%@page import="com.vfb.socl.util.DB"%>
<%@page import="com.vfb.socl.entity.Person"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%
String loggedIn = (String)session.getAttribute("loggedIn");

String tid = "0";
Person user;
if (loggedIn==null) {
	response.sendRedirect("login.jsp");
} else {
	try {
		user = (Person)session.getAttribute("user");
		if (user==null) {
			session.invalidate();
			response.sendRedirect("login.jsp");
		} else {
			// check if the session is valid
			DB db = new DB();
			ActiveSessionsMapper asm = db.getSessionMapper();
			if (!asm.checkSession(user.getUsername())) {
				session.invalidate();
				response.sendRedirect("login.jsp");
			}
		}
	} catch (Exception e) {
		e.printStackTrace();
		//response.sendRedirect("login.jsp");
	}
}
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link rel="stylesheet" href="css/style.css" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>VFB Community Annotation</title>
<script type="text/javascript"	src="js/mootools-core-1.4.5-full-nocompat-yc.js"></script>
<script type="text/javascript"	src="js/mootools-more-1.4.0.1.js"></script>
<script type="text/javascript"	src="js/store.min.js"></script>
<script type="text/javascript"	src="js/ContextMenu.js"></script>
<link rel="StyleSheet" href="css/dtree.css" type="text/css" />
<link rel="StyleSheet" href="css/datepicker_dashboard.css" type="text/css" />

<link rel="StyleSheet" href="css/FbModal.style.css" type="text/css" />


<link rel="StyleSheet" href="css/autocomplete.css" type="text/css" />
<script type="text/javascript" src="js/dtree.js"></script>
<script type="text/javascript" src="js/dtree.js"></script>


	<script src="js/Locale.en-US.DatePicker.js" type="text/javascript"></script>
	<script src="js/Picker.js" type="text/javascript"></script>
	<script src="js/Picker.Attach.js" type="text/javascript"></script>
	<script src="js/Picker.Date.js" type="text/javascript"></script>
	
	<script src="js/FbModal.class.js" type="text/javascript"></script>

<!-- <script type="text/javascript" src="js/autocomplete.js"></script> -->
<script type="text/javascript" src="js/Meio.Autocomplete.js"></script>


<!-- 	
<script type="text/javascript"  src="js/script.js">
</script>
 -->

<script type="text/javascript">
var intervals = [];
var Tips2;
(function() {
    var s = document.createElement('script');
    s.type = 'text/javascript';
    s.async = true;
    s.src = 'js/script.js';
    var x = document.getElementsByTagName('script')[0];
    x.parentNode.insertBefore(s, x);
})();

window.onbeforeunload = function(event) {
	//alert("rly?");
	event.preventDefault();
};

window.addEvent('domready', function() {
	Tips2 = new Tips($$('.tips'));
});
</script>

</head>
<body>
	<div id='tipHolder' class='hidden'>

	</div>
	<div id="mainDiv">
		<div id="loading"><img src="images/ajax-loader.gif" /></div>
		<h1>VFB Community Annotation Tool</h1>
		<p>Hello <span style="text-transform: capitalize">${user}</span>! (<a href="logout.jsp">logout</a>)</p>
		
		<jsp:include page="topMenu.jsp"></jsp:include>
		<div id="contentDiv">

		</div>
	</div>
</body>
</html>