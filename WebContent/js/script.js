var loadURL;

var openedIds = []; // stores list of opened popUps

var map = {
	"user/profile" : "user/profile.jsp",
	//"user/dashboard" : "user/dashboard.jsp",
	"message/threads" : "message/threads.jsp",
	"annotation/annotations" : "annotation/annotations.jsp"
};

if(typeof(String.prototype.trim) === "undefined")
{
    String.prototype.trim = function() 
    {
        return String(this).replace(/^\s+|\s+$/g, '');
    };
}

String.prototype.format = function() {
	  var args = arguments;
	  return this.replace(/{(\d+)}/g, function(match, number) { 
	    return typeof args[number] != 'undefined'
	      ? args[number]
	      : match
	    ;
	  });
	};

	


window.addEvent('domready', function() {
	Element.implement({
		// implement show
		show : function() {
			this.setStyle('display', '');
			this.setStyle('visibility', '');
		},
		// implement hide
		hide : function() {
			this.setStyle('display', 'none');
			this.setStyle('visibility', 'collapse');
		}
	});
	Element.implement({
		fadeAndDestroy: function(duration) {
			duration = duration || 600;
			var el = this;
			this.set('tween', {
				duration: duration
			}).fade('out').get('tween').chain(function() {
				el.dispose();
			});
		}});

	window.addEvent('click', function(event) {
		//console.log("something: " + !(event.target.tagName == 'INPUT'));
		if (!(event.target.tagName == 'INPUT')) {
			if (!$(event.target).hasClass("popup_window")) {
				for ( var i = 0; i < openedIds.length; i++) {
					$(openedIds[i]).hide();
				}
				openedIds = [];
			} else {
				//console.log("has class popup window");
			}
		} else {
			//console.log("an input control");
		}
		//console.log("done");
	});
	window.addEvent('keydown', function(event) {
		//console.log(event.target);
		if (event.key == 'esc') {
			for ( var i = 0; i < openedIds.length; i++) {
				$(openedIds[i]).hide();
			}
			openedIds = [];
		}
		/*
		 * if (this.hasClass("popup") && this.hasClass("visible")) {
		 * this.removeClass("visible"); this.addClass("hidden"); }
		 */
	});

	$('loading').hide();
	var req = new Request.HTML({
		method : 'get',
		update : $('contentDiv'),
		onRequest : function() {
			$('loading').show();
		},
		onSuccess : function() {
			$('loading').hide();
		}
	});
	function loadURL(url) {
		req.cancel();
		req.options.url = url;
		req.send();
	}
	function onLoad() {
		var err = false;

		if (window.location.hash.length > 0) {
			var hash = window.location.hash.split("#")[1].split("/");
			console.log(window.location.hash);
			console.log(hash);

			var path = hash[0] + "/" + hash[1];
			var menuId = hash[0] + "-" + hash[1];
			$$('#navlist li').removeClass("active");
			$(menuId).addClass("active");
			console.log(path);
			if (map.hasOwnProperty(path)) {
				loadURL('views/' + map[path] + "?id=" + hash[2]);
			} else {
				err = true;
			}
		} else {
			err = true;
		}
		if (err) {
			//loadURL('views/user/dashboard.jsp');
			loadURL('views/user/profile.jsp');
		}
	}
	onLoad();

	$$('#navlist a').each(function(element, index) {
		element.addEvent('click', function(event) {
			for (var i=0; i<intervals.length; i++) {
				clearInterval(intervals[i]);
			}
			$$('#navlist li').removeClass("active");
			element.getParent().addClass("active");
			event.stop();
			var path = element.get('href');
			var url = path + ".jsp";
			window.location.href = "#" + path.replace("views/", "");
			loadURL(url);
		});
	});
	
	var lastShownAnnotationNotification = 0;
	var reqNewAnnotations = new Request.JSON({
		url: 'do_annotations?action=getNewAnnotations',
		method : 'get',
		onRequest : function() {
			//$('loading').show();
		},
		onSuccess : function(requestJSON, requestTxt) {
			if (requestJSON.error!="no") return;
			$('annotation_count').set('html', requestJSON.message.annotations.length);
			if (requestJSON.message.annotations.length==0) {
				$('annotation_count').set('class', 'tips notification_count_none');
			} else {
				$('annotation_count').set('class', 'tips notification_count');
				
				
				for (var i=0; i<requestJSON.message.annotations.length; i++) {
					var annRow = requestJSON.message.annotations[i];
					var d = Date.parse(annRow.dateSubmited); 
					if (lastShownAnnotationNotification < d) {
						var msg = "Structure: " + annRow.structureId + "<br />Gene: " + annRow.geneId + 
						    "<br/>pattern: " + annRow.pattern + " - " + annRow.strength; 
						if (annRow.isSubmission) {
							createMessageNotificationTickerMessage("Annotation submitted from: <b>" + annRow.username + "</b>", msg );	
						} else {
							createMessageNotificationTickerMessage("<b>" + annRow.username + "</b> voted on submission", msg );
						}
					}
				}
				lastShownAnnotationNotification = Date.parse(requestJSON.message.annotations[0].dateSubmited); 
			}
			$('annotation_flag_count').set('html', requestJSON.message.flags.length);
			if (requestJSON.message.flags.length==0) {
				$('annotation_flag_count').set('class', 'tips notification_count_none');
			} else {
				$('annotation_flag_count').set('class', 'tips notification_count');
			}
			
		}
	});
	var refreshAnnotationCount = function() { reqNewAnnotations.get(); };
	refreshAnnotationCount();
	refreshAnnotationInterval = setInterval(refreshAnnotationCount, 3000);
	//intervals.push(refreshAnnotationInterval);

	var hideElements = [];
	function createMessageNotificationTickerMessage(title, body) {
		var el = new Element('div', {'html': "<div class='tip'><div class='tip-title'>" + title + "</div><div class='tip-text'>" + body + "</div></div>" });
		$('tipHolder').set('class', 'visible');
		$('tipHolder').grab(el, 'top');
		var d = parseInt(new Date().getTime()) + 10000;
		//console.log(d-parseInt(new Date().getTime()));
		hideElements.push([el, d]);
		return ;
	}
	

	function morfeNotificationOut() {
		
		var curTime = new Date().getTime();
		var newList = [];

		console.log("clear called");
		hideElements.each(function (element, index) {
			if (element[1] < curTime) {
				console.log("clear ok");
				/*
			  element[0].morph({
		        'background-color': '#F9F9EE',
		        'opacity': 0,
		        'duration': 'long'
		      });			
		      */
				element[0].fadeAndDestroy();
			} else {
				newList.push(element);
			}
		});
		hideElements = newList;
	}
	
	var lastShownMessageNotification = 0;
	var reqNewMessages = new Request.JSON({
		url: 'do_messages?action=getNewMessages',
		method : 'post',
		onRequest : function() {
			//$('loading').show();
		},
		onSuccess : function(requestJSON, requestTxt) {
			if (requestJSON.error!="no") return;
			$('message_count').set('html', requestJSON.message.length);
			if (requestJSON.message.length==0) {
				$('message_count').set('class', 'tips notification_count_none');
			} else {
				for (var i=0; i<requestJSON.message.length; i++) {
					if (lastShownMessageNotification<requestJSON.message[i].messageId) {
						createMessageNotificationTickerMessage("Message from: <b>" + requestJSON.message[i].username + "</b>", requestJSON.message[i].body);
					}
				}
				lastShownMessageNotification = requestJSON.message[0].messageId;
				$('message_count').set('class', 'tips notification_count');
			}
		}
	});
	
	var refreshMessageCount = function() { reqNewMessages.get(); };
	refreshMessageCount();
	refreshMessagesInterval = setInterval(refreshMessageCount, 3000);
	
	var clearNotificationsInterval = setInterval(morfeNotificationOut, 7000);
	//intervals.push(clearNotificationsInterval);
	
});
